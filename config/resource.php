<?php 

return [

'parameter_missing' => 'Parameter missing',
'status_400' => 400, 
'signup_success' => 'SignUp Successfully',  
'status_200' => 200, 
'method_mismatch' => 'Rquest Method Wrong',  
'status_420' => 420,
'record_not_found' => 'Record Not Found',
'password_wrong'   => 'Please check your password',
'status_404' => 404, 
'type_missing' => 'Type Missing', 
'status_400' => 201, 
'mobile_number_exists' => 'Mobile number already exists', 
'email_exists' => 'Email already exists', 
'user_doesnt_exists' => "User doesn't Exists",
'login_success' => 'Login successfully', 
'password_updated_successfully' => 'Password Updated Successfully', 
'updated_successfully' => 'Updated Successfully',
'no_record' => 'No Record',
'added_successfully' => "Added Successfully",
'removed_successfully' => 'Removed Successfully',
'status_201' => 201,
'error' => 'Error',
'login_process' => "Login Process",
'success' => 'success',
'invalid_email_or_password' => "Invalid Email or Password",
'user_exists' => 'User Exists',
'user_exist_process' => 'Check user Exists Process',
'signup_process'  => 'Signup Process',
'profile_update_process' => 'Profile Update Process',
'value_missing' => 'Value missing',
'data_added_successfully' => 'Data Added Successfully',
'added_successfully' => 'Added  Successfully',
'id_missing'     => 'Id missing', 
'no_data'        => 'No Data', 
'password_reset'        => 'Password link sent to your registered emaild', 
'email_doesnt_exists'        => "Email doesn't Exist", 
'update_success'        => "Update Successfully", 

];

?>

 