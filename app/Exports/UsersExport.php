<?php

namespace App\Exports;

use App\Models\Users;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;	
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;


class UsersExport implements FromCollection, WithTitle, WithHeadings
{
	private $query;

    public function __construct(array $query)
    {
         $this->query = $query;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {   
      // dd($this->query['start_date']);
       $start = $this->query['start_date'];
        $end = $this->query['end_date'];
    	 $language = array();
    	 $status   = array();
    	 $gender   = array();
    	if($this->query['language'] == 'ar' ||  $this->query['language'] == 'en'){
                   $language[] = $this->query['language'];
               } 
        if($this->query['status'] == 0 ||  $this->query['status'] == 1){
                   $status[] = $this->query['status'];
               }  
        if($this->query['gender'] == 0 ||  $this->query['gender'] == 1){
                   $gender[] = $this->query['gender'];
               }               
         $data = Users::whereIn('language',$language)
                       ->whereIn('status',$status)
                       ->whereIn('gender',$gender)
                       ->whereDate('created_at', '>=', $start) 
                       ->whereDate('created_at', '<=', $end)
                       ->get();

          if($data->count() !=0){
                      $finaldata = array();
                      foreach ($data as $key => $value) {
                      	   if($value->gender == 0){
                              $value->gender = 'Male' ;
                      	   }
                      	   if($value->gender == 1){
                              $value->gender = 'Female' ;
                      	   }
                      }                     
                   return $data;
          }
          return $data ;
              
    }

    public function headings(): array
    {
        return [
            'id',
            'name',
            'lastname',
            'photo',
            'email',
            'password',
            'phone',
            'phone_country_code',
            'language',
            'dob',
            'gender',
            'id_cms_privileges',
            'facebook_id',
            'twitter_id',
            'googleplus_id',
            'created_at',
            'updated_at',
            'status',
            'user_type',
        ];
    }
     /**
     * @return string
     */
    public function title(): string
    {
        return 'User Details';
    }
          
}
