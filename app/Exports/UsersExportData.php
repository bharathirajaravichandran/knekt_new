<?php

namespace App\Exports;

use App\Models\Users;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;	
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;

use Request;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;


class UsersExportData implements FromView
{
	  private $query;

    public function __construct(array $query)
    {
         $this->query = $query;
    }

    public function view(): View
    {       
          
           $start_date = $this->query['start_date']; 
           $end_date   = $this->query['end_date'] ;
           $lang= 0;$coun= 0;$gend= 0;$stat = 0;
           $data = Users::whereDate('created_at', '>=', $start_date) 
                         ->whereDate('created_at', '<=', $end_date)
                         ->get(); 
            $final_array = array(); 
          //get language data  
           if($this->query['type'] == 'language'){            
                    $lang = 1;
                    if(count($data) !=0){
                               foreach ($data as $key => $value) {
                                       $final_array[$value->language][] = $value ;
                               }
                     }
            }  
          //get language data        

           if($this->query['type'] == 'country'){$coun = 1; }
           if($this->query['type'] == 'gender'){
              $gend = 1;
              if(count($data) !=0){
                     foreach ($data as $key => $value) {
                             $final_array[$value->gender][] = $value ;
                     }
               }  
            }
           if($this->query['type'] == 'status'){
              $stat = 1;  
              if(count($data) !=0){
                     foreach ($data as $key => $value) {
                             $final_array[$value->status][] = $value ;
                     }
               }
           }
          libxml_use_internal_errors(true);
        return view('export_user_data_view',compact('data','lang','coun','gend','stat','final_array'));      
    }
          
}
