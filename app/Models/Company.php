<?php

namespace App\Models;

use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use AdminBuilder;

    protected $table = 'tbl_service';

    public function category()
    {
        return $this->belongsTo(Category::class,'parent_category_id');
    } 
    public function subcategory()
    {
        return $this->belongsTo(Category::class,'sub_cat_id');
    }      
}
