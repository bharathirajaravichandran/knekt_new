<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cat_option extends Model
{
    protected $table = 'tbl_catoptions';

     public function getCompanyName(){
        return $this->belongsTo('App\Models\Company','company_id','id');
    }
     public function company()
    {
        return $this->belongsTo(Company::class,'company_id');
    } 

    public function childs() {
        return $this->hasMany('App\Models\Cat_option','parent_option_id','id') ;
    }
}
