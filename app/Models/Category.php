<?php

namespace App\Models;

use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use AdminBuilder;

    protected $table = 'tbl_category';

    public $sortable = ['id', 'category_name'];

    public function parentcategory()
    {
        return $this->belongsTo(Category::class,'parent_category_id');
    }   
}
