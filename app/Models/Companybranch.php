<?php

namespace App\Models;

use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;

class Companybranch extends Model
{
    use AdminBuilder;

    protected $table = 'tbl_location';

    public function company()
    {
        return $this->belongsTo(Company::class,'company_id');
    } 
}
