<?php

namespace App\Admin\Extensions\Tools;

use Encore\Admin\Grid\Tools\BatchAction;

class ReleasePost extends BatchAction
{
    protected $action;

    public function __construct($action = 0)
    {
        $this->action = $action;
    }
    
    public function script()
    {
        return <<<EOT
        
$('{$this->getElementClass()}').on('click', function() {
    var urlsegments = '{$this->resource}';
    var current = urlsegments.substring(urlsegments.lastIndexOf("/") + 1, urlsegments.length);
    swal({
        title: "Are you sure you want to activate these fields ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "confirm",
        showLoaderOnConfirm: true,
        cancelButtonText: "cancel",
        preConfirm: function() {
            return new Promise(function(resolve) {
                $.ajax({
                    method: 'post',
                    url: '{$this->resource}/active',
                    data: {
                        _token:LA.token,
                        ids: selectedRows(),
                        action: {$this->action},
                        url_data : current,
                    },
                    success: function (data) {
                        $.pjax.reload('#pjax-container');
                        if (typeof data === "object") {
                            if (data.status) {
                                swal(data.message, "", "success");
                            } else {
                                swal(data.message, "", "error");
                            }
                        }
                    }
                });
            });
        }
    });
   
});

EOT;

    }
}