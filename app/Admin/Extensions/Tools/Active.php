<?php

namespace App\Admin\Extensions\Tools;

use Encore\Admin\Grid\Tools\BatchAction;
use Encore\Admin\Facades\Admin;
use Illuminate\Contracts\Support\Htmlable;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Support\Collection;

class Active extends BatchAction
{
    protected $action;

    public function __construct($action = 1)
    {
        $this->action = $action;
    }
    
    public function script()
    {
        $ActiveConfirm = trans('admin.active_confirm');
        $confirm = trans('admin.confirm');
        $cancel = trans('admin.cancel');
        return <<<EOT
        
$('{$this->getElementClass()}').on('click', function() {
    var urlsegments = '{$this->resource}';
    var current = urlsegments.substring(urlsegments.lastIndexOf("/") + 1, urlsegments.length);
    swal({
        title: "$ActiveConfirm",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "$confirm",
        showLoaderOnConfirm: true,
        cancelButtonText: "$cancel",
        preConfirm: function() {
            return new Promise(function(resolve) {
                $.ajax({
                    method: 'post',
                    url: 'active',
                    data: {
                        _token:LA.token,
                        ids: selectedRows(),
                        action: {$this->action},
                        url_data : current,
                    },
                    success: function (data) {
                        $.pjax.reload('#pjax-container');
                        if (typeof data === "object") {
                            if (data.status) {
                                swal(data.message, "", "success");
                            } else {
                                swal(data.message, "", "error");
                            }
                        }
                    }
                });
            });
        }
    });
   
});

EOT;

    }
}