<?php

namespace App\Admin\Controllers;

use App\Models\Cat_option;
use App\Models\Company;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Facades\Admin;
use Illuminate\Support\Facades\Input;
use Encore\Admin\Show;
use Illuminate\Http\Request;
// use Request;
use DB;

class AdminSubIvrDetailsController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
       return Admin::content(function (Content $content) {
            $company_id = Input::get('company_id', false);
            $preid = Input::get('id');
            $lang = Input::get('lang');
            // dd($company_id);
            $content->header(trans('admin.form_menu.sub_ivr_details'));
            $content->description('');
            $content->body($this->grid($company_id,$preid,$lang));
        });
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        $ivr_value =Cat_option::find($id);
        $company = Company::find($ivr_value->company_id);
        $company_id = Input::get('company_id', false);
        $preid = Input::get('id', false);
         $hour_time = explode(':', $ivr_value->time_delay);
        $time_delay = $hour_time[1].':'.$hour_time[2];
        return $content
            ->header(trans('admin.form_menu.sub_ivr_details'))
            ->description('')
             ->body(view('subIvrView',['company'=>$company,'ivr_value'=>$ivr_value,'company_id'=>$company_id,'preid'=>$preid,'time_delay'=>$time_delay]));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return Admin::content(function (Content $content) use ($id) {
            
            $content->header(trans('admin.form_menu.sub_ivr_details'));
            $content->description('');
            $old_values = Cat_option::find($id);           
            $times = $old_values->time_delay;
            $time_value=explode(':', $times);
            $content->body(view('subIvr',['old_values'=>$old_values,'time_value'=>$time_value,'pid'=>$id]));
                         //$content->body($this->form());
        });
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return Admin::content(function (Content $content) {
            
            $content->header(trans('admin.form_menu.sub_ivr_details'));
            $content->description('');          
            $content->body(view('subIvr',['pid'=>'']));
                         //$content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid($segments,$preid,$lang)
    {
        $grid = new Grid(new Cat_option);

       
        // dd($segments);
        $grid->model()->where('company_id',$segments)->where('parent_option_id','!=',0)->where('language',$lang)->orderBy('id', 'desc');
       
        $grid->tools(function ($tools) use ($segments,$preid, $lang) {
            $tools->append("<a href='add_subivrs/".$preid."/create?preid=".$preid."&lang=". $lang."&company=". $segments."' class='btn btn-success'>".trans('admin.form_menu.create')."</a>");
            $tools->append("<span id='mainoption' class='btn btn-sm' style='color:white;margin-left:20px;font-size:15px;background:#14A8A6;'>".Cat_option::where('company_id',$segments)->where('language',$lang)->where('parent_option_id',0)->first()->option_name." : ".Company::find($segments)->service_name."</span>");
        });

        // $grid->company_id('Company id');
        $grid->option_name(trans('admin.form_menu.ivr_name'));
         $grid->option_value(trans('admin.form_menu.ivr_value'));
          $grid->column(trans('admin.form_menu.parent_option'))->display(function () {
            $parent=Cat_option::find($this->parent_option_id)->option_name;

           
             return $parent;
        });

        // $grid->time_delay('Time Delay');
        $grid->column(trans('admin.form_menu.time_delay'))->display(function () {
             $hour_time = explode(':', $this->time_delay);
            $time_delay = $hour_time[1].':'.$hour_time[2];
            return $time_delay;
        });
         $grid->column(trans('admin.form_menu.add_sub_ivr'))->display(function () use ($segments,$preid, $lang) {
            $add_button = '    
            <a href="add_subivr/'.$this->id.'/create?preid='.$preid.'&lang='. $lang.'&company='. $segments.'" id="createsub" class="btn btn-primary btn-sm">'.trans('admin.form_menu.add').'</a>
            ';
            return $add_button;
        });

         $grid->column(trans('admin.form_menu.sub_ivr_list'))->display(function () use ($segments,$preid, $lang) {
            $add_button = '    
            <a href="add_subivr/'.$this->id.'/view?preid='.$preid.'&lang='. $lang.'&company='. $segments.'" id="createsub" class="btn btn-primary btn-sm">'.trans('admin.form_menu.list').'</a>
            ';
            return $add_button;
        });

           $grid->disableCreateButton();
          $grid->filter(function($filter){

            // Remove the default id filter
            $filter->disableIdFilter();
            $filter->like('option_name', trans('admin.form_menu.ivr_name'));          

            });
         $grid->actions(function ($actions) use ($segments,$preid,$lang) {
            // $actions->disableDelete();
            $key = $actions->getKey();
            // dd($key);
            $actions->disableEdit();
            // $actions->disableCreate();
            $actions->disableView();
            $actions->prepend('<a href="add_subivr/'.$key.'/edit?preid='.$preid.'&lang='. $lang.'&company='. $segments.'"><i class="fa fa-edit"></i></a>');
            $actions->prepend('<a href="add_subivr/detail/'.$key.'?preid='.$preid.'&lang='. $lang.'&company='. $segments.'"><i class="fa fa-eye"></i></a>');
        });
          Admin::script("
            $('a#createsub').on('click',function(){
                var parent_id =$('#parent_id').val();
                    $('#parent_value').val(parent_id);
                });
            ");

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Cat_option::findOrFail($id));

        // $show->id('Id');
         $show->company_id('Company')->as(function ($company_id) {
                $company_name=Company::find($this->company_id)->service_name;
                return $company_name;
            });
        $show->option_name('Option name');
        $show->option_value('Option value');
        $show->time_delay('Time delay');       
        $show->panel()
        ->tools(function ($tools) {
            $tools->disableEdit();
            $tools->disableList();
            $tools->disableDelete();
        });
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {

        $form = new Form(new Cat_option);
        // dd($form);
        // exit;
        $cat = DB::table('tbl_catoptions')->where('parent_option_id',Request::segment(3))->get();  
        $subcat =array();
        foreach ($cat as $key => $value) {
            $subcat[] = $value->option_name.' = '.$value->option_value;
        }

        if(count($subcat)!=0){
            $subcat = $subcat;
        }
        $form->html('<span id="ivrdetails">'.implode(',',$subcat).'</span>');
        $form->text('option_name', 'Service Name');
        $form->text('option_value', 'IVR Value');
        $form->hidden('company_id', 'company id');
        $form->hidden('parent_option_id', 'Parend id');
        $form->time('time_delay', 'Time delay')->default(date('H:i:s'));
        $form->saving(function(Form $form){
            $company=Cat_option::find(Request::segment(3));
            $form->parent_option_id = Request::segment(3);
            $form->company_id = $company->company_id;
          
        });

        return $form;
    }
     public function get_sub_ivr_details(Request $request)
     {   
        // dd($request->all());
        $cat = DB::table('tbl_catoptions')->where('parent_option_id',$request->input('parent_id'))->get();  
        $subcat =array();
        foreach ($cat as $key => $value) {
            $subcat[] = $value->option_name.' = '.$value->option_value;
        }

        if(count($subcat)!=0){
            $subcat = $subcat;
        }else{
            $subcat ='No Data';
        }
        return response()->json($subcat);
                        
     }
       // IVR already exists check
     public function get_ivr_value(Request $request)
     {   
        $data=0;
        $parent_id = $request->input('parent_id');
        // dd($request->parent_id);
        if($request->uri_segment!='edit'){
           $ivr_option = DB::table('tbl_catoptions')
                       ->where('parent_option_id',$request->input('parent_id'))
                       ->where('option_value',$request->input('option_value'))
                       ->get();
        }else{
          $ivr_option = DB::table('tbl_catoptions')
                      ->where('parent_option_id',$request->input('parent_id'))
                      ->where('option_value',$request->input('option_value'))
                      ->where('id','!=',$request->old_id)
                      ->get();
        }
        if($ivr_option->count()!=0){
            $data =1;
        }else{
            $data =0;
        }
        
        return $data;   
     } 
     // get_service_name already exists check

     public function get_service_name(Request $request)
     {   

        $data=0;
        $parent_id = $request->input('parent_id');
        if($request->uri_segment!='edit'){
            $ivr_option = DB::table('tbl_catoptions')
                        ->where('parent_option_id',$request->input('parent_id'))
                        ->where('option_name',$request->input('option_name'))
                        ->get();
        }else{
             $ivr_option = DB::table('tbl_catoptions')
                         ->where('parent_option_id',$request->input('parent_id'))
                         ->where('option_name',$request->input('option_name'))
                         ->where('id','!=',$request->old_id)
                         ->get();
        }
            
        if($ivr_option->count()!=0){
            $data =1;
        }else{
            $data =0;
        }
        
        return $data;   
     }


     // Save Sub IVR
     public function subivrsave(Request $request){
        // dd($request->all());

            $company = Cat_option::find($request->input('parent_option_id'));
            if($request->minutes !='' && $request->seconds !=''){
            if(strlen($request->minutes)==1){
                $minutes ='0'.$request->minutes;
            }else{
                $minutes =$request->minutes;
            }
            if(strlen($request->seconds)==1){
                 $seconds ='0'.$request->seconds;
            }else{
                 $seconds =$request->seconds;
            }

             $time_delay = '00:'.$minutes.':'.$seconds;
            }


              // dd($time_delay);
            if($request->oid!='' || $request->oid!=null)
            $ivr_value=Cat_option::find($request->oid);
            else
            $ivr_value = new Cat_option;

            $ivr_value->company_id = $company->company_id;
            $ivr_value->parent_option_id = $request->input('parent_option_id');
            $ivr_value->option_value = $request->input('option_value');
            $ivr_value->option_name =$request->input('option_name');
            $ivr_value->time_delay =$time_delay;
            $ivr_value->language = $company->language;
            $ivr_value->save();

            $company_value = $company->company_id;
            $preid = $request->input('preid');
            $lang = $company->language;

       if($request->input('submit_type') == 'addmore')      
           return redirect()->back();
       else
           return redirect('admin/add_subivr?company_id='.$company_value.'&id='.$preid.'&lang='.$lang); 
       
       
     }

     
     public function view(Request $request,Content $content){
            $datas = array();
            $id = $request->segment(3);
            $value = Cat_option::find($id)->company_id;
            $companyname = Company::find($value)->service_name;
            do {
                  $value = Cat_option::find($id);
                if($value->parent_option_id != 0){
                    $id = $value->parent_option_id;
                    $datas[]=$value->option_name;
                    $values[]=$value->option_value;
                }else{
                     $id = '';
                     $language = $value->option_name;
                     $language_value = $value->option_value;
                }
            } while ($id != '');
            $data = array_reverse($datas);
            $option_value = array_reverse($values);
            return $content
            ->header('Sub IVR List')
            ->description('')
             ->body(view('subIvrList',['language'=>$language,'language_value'=>$language_value,'companyname'=>$companyname,'data'=>$data,'option_value'=>$option_value]));

     }
}
