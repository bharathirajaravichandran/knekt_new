<?php

namespace App\Admin\Controllers;

use App\Models\Category;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Contracts\Filesystem\Filesystem;
use Carbon\Carbon;

class CategoryController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Category Management')
            ->description('Listing')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Category Management')
            ->description('View')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
            $pid=$id;
            $image_array = array();
            $category=Category::find($id);
            if($category->category_image!="")
            $image_array[] = $category->category_image;
        return $content
            ->header('Category Management')
            ->description('Edit')
            //->body($this->form()->edit($id))
            ->body(view('category',['pid'=>$pid,'category'=>$category,'image_array'=>$image_array]));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Category Management')
            ->description(' ')
           // ->body($this->form());
             ->body(view('category',['pid'=>'','image_array'=>array()]));
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Category);

        $grid->model()->orderby('id','DESC');
        $grid->category_name('Category Name');
        $grid->category_description('Category Description')->display(function ($category_description) 
        {
            return str_limit( $category_description, 80, '...');
        });  

        $grid->column('category_image')->display(function () { 
            if($this->category_image!="")
            $img = '<img src="'.url('/').'/uploads/Category/'.$this->category_image.'" width="75px"/>';
            else
            $img = "No Image Found";
            return $img;
        });

        //$grid->status()->switch();

        // set the `text`、`color`、and `value`
        $states = [
            'on'  => ['value' => 1, 'text' => 'Active', 'color' => 'success'],
            'off' => ['value' => 0, 'text' => 'Inactive', 'color' => 'danger'],
        ];
        $grid->status()->switch($states);

         $grid->filter(function($filter){
                $filter->disableIdFilter();
                $filter->like('category_name', 'Category Name');
                $filter->equal('status','status')->select(['0' => 'In Active','1'=>'Active']);
            });  

        /*$grid->column('status','Status')->display(function () {
            return ($this->status==1)?'Active':'In Active';
        }); */
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Category::findOrFail($id));

        $show->category_name('Category name');
        $show->category_description('Category description');
        $show->category_image('Category image');
        $show->created_at('Created at');
        $show->updated_at('Updated at');
        $show->status('Status');
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Category);

        $form->text('category_name', 'Category name');
        $form->switch('status');
        $form->textarea('category_description', 'Category description');
        $form->text('category_image', 'Category image');
        $form->number('parent_category_id', 'Parent category id');
        $form->text('language', 'Language')->default('en');
       // $form->number('status', 'Status')->default(1);
        $form->number('user_id', 'User id');

        return $form;
    }

    public function savecategory(Request $request)
    {
        $catname = $request->input('category_name');
        $catdesc = $request->input('category_description');
        $status = $request->input('status');
        $pid = $request->input('pid');
        $deletecheck = $request->input('deletecheck');
        $picture_single ="";
        $imagecheck =0;

        if($request->has('category_image'))
        {
            $image_single = $request->file('category_image');
            $extension = $image_single->getClientOriginalExtension();
            $picture_single = date('YmdHis').".".$extension;
            $destinationPath = base_path() . '/public/uploads/Category/';
            $image_single->move($destinationPath, $picture_single);
            $imagecheck =1;
        }

        if($imagecheck=="0" && $deletecheck==0)
        $imagecheck=0;
        if($imagecheck=="0" && $deletecheck==1)
        $imagecheck=1;

        if($pid=="" || $pid==null)
        {
            $category = new Category;
            $category->created_at=  Carbon::now()->toDateTimeString();
        }
        else
        {
            $category=Category::find($pid);
            $category->updated_at=  Carbon::now()->toDateTimeString();
        }

        $category->category_name=$catname;
        $category->category_description=$catdesc;

        if($imagecheck==1)
        $category->category_image=$picture_single;

        $category->status=$status;    
        $category->save();

        if($request->input('pid')!="")
        admin_toastr('Category Updated successfully');
        else
        admin_toastr('Category Created successfully');

        return redirect('admin/category');
    }


    public function categoryexist(Request $request)
    {
        $catname = $request->post('category_name');
        $id = ($request->has('pid')) ? $request->post('pid') : 0;
        
        if(is_numeric($id))
            $catcheck=Category::where("category_name",'=',trim($catname))->where('parent_category_id',0)->whereNOTIn('id',[$id])->first();
        else
            $catcheck=Category::where("category_name",'=',trim($catname))->where('parent_category_id',0)->first();
                
        if($catcheck)
        {
            echo "failed";
            exit;
        }
        else
        {
            echo "success";
            exit;
        }
    }
}
