<?php

namespace App\Admin\Controllers;

use App\Models\Companybranch;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

use Encore\Admin\Facades\Admin;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Contracts\Filesystem\Filesystem;
use Carbon\Carbon;
use DB;
use App\Admin\Extensions\Tools\Active;
use App\Admin\Extensions\Tools\Inactive;

class CompanybranchController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content,Request $request)
    {
        return $content
            ->header(trans('admin.form_menu.company_branch_details'))
            ->description('Listing')
            ->body($this->grid($request->all()));
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        $companybranch = Companybranch::with('company')->where('id',$id)->first();
        return $content
            ->header(trans('admin.form_menu.company_branch_details'))
            ->description(' ')
            ->body(view('companybranchview',['companybranch'=>$companybranch]));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        $company = DB::table('tbl_service')->where('status', 1)->pluck('service_name','id');
        $pid=$id;
        $companybranch=Companybranch::find($id);
        return $content
            ->header(trans('admin.form_menu.company_branch_details'))
            ->description(' ')
            ->body(view('companybranch',['company'=>$company,'pid'=>$pid,'companybranch'=>$companybranch]));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        $company = DB::table('tbl_service')->where('status', 1)->pluck('service_name','id');
        return $content
            ->header(trans('admin.form_menu.company_branch_details'))
            ->description(' ')
            //->body($this->form());
            ->body(view('companybranch',['company'=>$company,'pid'=>'']));
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid($request)
    {
        $grid = new Grid(new Companybranch);

        $grid->disableExport();
        // $grid->model()->orderby('id','DESC');
            if(isset($request['_sort']['type']) && ($request['_sort']['type'] == "asc")){           
              $grid->model()->orderby($request['_sort']['column'],'asc');
             
            }elseif(isset($request['_sort']['type']) && ($request['_sort']['type'] == "desc")){  
                     
              $grid->model()->orderby($request['_sort']['column'],'desc');
            }else{
               
                $grid->model()->orderby('id','DESC');
            }



        $grid->company_id(trans('admin.form_menu.company'))->display(function () {
            $companyname = ($this->company_id!=0)? $this->company['service_name']:'No Company';
            return str_limit( $companyname, 30, '...');
        })->sortable();

        $grid->phone(trans('admin.form_menu.phone'))->display(function () {
            return ($this->phone!=0 && $this->phone!="")? $this->phone:'--------';
        });
        $grid->latitude(trans('admin.form_menu.latitude'));
        $grid->langitude(trans('admin.form_menu.longitude'));

        $grid->address(trans('admin.form_menu.address'));
        $states = [
            'on'  => ['value' => 1, 'text' => 'Active', 'color' => 'success'],
            'off' => ['value' => 0, 'text' => 'Inactive', 'color' => 'danger'],
        ];
        $grid->status(trans('admin.form_menu.status'))->switch($states);
        $grid->tools(function ($tools) {
            $tools->batch(function ($batch) {
                $batch->add(trans('admin.set_inactive'), new Inactive(0));
                $batch->add(trans('admin.set_active'), new Active(1));      
            });
        });

        $grid->filter(function($filter){
            $filter->disableIdFilter();
            $filter->like('company.service_name', 'Company');
            $filter->like('address', 'Address');
            $filter->equal('status','status')->select(['0' => 'In Active','1'=>'Active']);
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Companybranch::findOrFail($id));

        //$show->id('Id');
        //$show->company_id('Company');
    /*    $show->company('Company', function ($company) {

            //$company->resource('/admin/models/company');
            $company->service_name();

        });*/
        $show->latitude('Latitude');
        $show->langitude('Longitude');
        $show->address('Address');
        $show->created_at()->as(function ($created_at) {
            return date_format($created_at,"M d, Y H:m:s");
        });
        $show->updated_at()->as(function ($updated_at) {
            return date_format($updated_at,"M d, Y H:m:s");
        });
       // $show->user_id('Created By')->using(['0' => 'Others', '1' => 'Admin']);
        $show->status()->using(['0' => 'Inactive', '1' => 'Active']);

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Companybranch);

        $form->number('company_id', 'Company id');
        $form->textarea('address', 'Address');
        $form->switch('status');

        return $form;
    }

    public function savecompanybranch(Request $request)
    {
        $company_id = $request->input('company_id');
        $address = $request->input('address');
        $status = $request->input('status');
        $pid = $request->input('pid');

        if($pid=="" || $pid==null)
        {
            $companybranch = new Companybranch;
            $companybranch->created_at=  Carbon::now()->toDateTimeString();
        }
        else
        {
            $companybranch=Companybranch::find($pid);
            $companybranch->updated_at=  Carbon::now()->toDateTimeString();
        }

        $companybranch->company_id = $company_id;
        $companybranch->address = $address;
        $companybranch->status = $status;
        $companybranch->phone=$request->input('phone');
        $companybranch->phone_country_code=$request->input('phone_country_code');
        $companybranch->user_id = Admin::user()->id;

        $formattedAddr = str_replace(' ','+',trim($address));
        $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$formattedAddr.'&sensor=false&key=AIzaSyBdkLaZtAdO8mLytloT8OUZbYwLK-cOSxM');
        $output = json_decode($geocodeFromAddr);
        $data['latitude']  = (isset($output->results[0]->geometry->location->lat)) ? $output->results[0]->geometry->location->lat : 0; 
        $data['longitude'] = (isset($output->results[0]->geometry->location->lng)) ? $output->results[0]->geometry->location->lng : 0;

        if(!empty($data)){
             $companybranch->latitude =  ($data['latitude']!="" && $data['latitude']!=null) ? $data['latitude'] : 0;
             $companybranch->langitude = ($data['longitude']!="" && $data['longitude']!=null) ? $data['longitude'] : 0;
        }
        else
        {
            $companybranch->latitude =0;
            $companybranch->langitude =0;
        }
        $companybranch->save();

        if($request->input('pid')!="")
        admin_toastr('Companybranch Updated successfully');
        else
        admin_toastr('Companybranch Created successfully');

        return redirect('admin/companybranch');

    }

    public function companybranchcheck(Request $request)
    {
        $company_id = $request->post('company_id');
        $id = ($request->has('pid')) ? $request->post('pid') : 0;
        $address = $request->post('address');
        
        if(is_numeric($id))
            $companybranchcheck=Companybranch::where("address",'=',trim($address))->where('company_id',$company_id)->whereNOTIn('id',[$id])->first();
        else
            $companybranchcheck=Companybranch::where("address",'=',trim($address))->where('company_id',$company_id)->first();
                
        if($companybranchcheck)
        {
            echo "failed";
            exit;
        }
        else
        {
            echo "success";
            exit;
        }
    }
}
