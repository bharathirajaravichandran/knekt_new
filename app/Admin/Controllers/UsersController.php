<?php

namespace App\Admin\Controllers;

use App\Models\Users;
use App\Models\Country;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

use Encore\Admin\Facades\Admin;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Contracts\Filesystem\Filesystem;
use Carbon\Carbon;
use DB;
use App\Admin\Extensions\Tools\Active;
use App\Admin\Extensions\Tools\Inactive;

class UsersController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content,Request $request)
    {
        return $content
            ->header(trans('admin.form_menu.users_management'))
            ->description('Listing')
            ->body($this->grid($request->all()));
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        $user = Users::where('id',$id)->first();
        return $content
            ->header(trans('admin.form_menu.users_management'))
            ->description(' ')
           // ->body($this->detail($id));
            ->body(view('userview',['user'=>$user])); 
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        $pid=$id;
        $image_array = array();
        $users=Users::find($id);
        if($users->photo!="")
        $image_array[] = $users->photo;

        return $content
            ->header(trans('admin.form_menu.users_management'))
            ->description(' ')
            //->body($this->form()->edit($id));
            ->body(view('users',['pid'=>$pid,'image_array'=>$image_array,'user'=>$users])); 
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header(trans('admin.form_menu.create'))
            ->description('description')
            //->body($this->form());
            ->body(view('users',['pid'=>'0','image_array'=>array()])); 
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid($request)
    {
        $grid = new Grid(new Users);
        $grid->disableExport();
        // $grid->model()->orderby('id','DESC');
        
        if(isset($request['_sort']['type']) && ($request['_sort']['type'] == "asc")){           
          $grid->model()->orderby($request['_sort']['column'],'asc');
         
        }elseif(isset($request['_sort']['type']) && ($request['_sort']['type'] == "desc")){  
                 
          $grid->model()->orderby($request['_sort']['column'],'desc');
        }else{
           
            $grid->model()->orderby('id','DESC');
        }
        $grid->name(trans('admin.form_menu.name'))->sortable();
        $grid->column('photo',trans('admin.form_menu.photo'))->display(function () { 
            if($this->photo!="")
            $img = '<img src="'.url('/').'/uploads/Users/'.$this->photo.'" width="75px" onerror="this.style.display=\'none\'"/>';
            else
            $img = "No Image Found";
            return $img;
        });
        
        $grid->email(trans('admin.form_menu.email'));
        $grid->phone(trans('admin.form_menu.phone'));
        $grid->language(trans('admin.form_menu.language'));
      //  $grid->dob('Dob');
         $grid->column('dob','Dob')->display(function () {
                return date("Y-m-d",strtotime($this->dob));
            }); 
        $grid->column('gender',trans('admin.form_menu.gender'))->display(function () {
                return ($this->gender==1)?'Male':'Female';
            }); 
        $states = [
            'on'  => ['value' => 1, 'text' => 'Active', 'color' => 'success'],
            'off' => ['value' => 0, 'text' => 'Inactive', 'color' => 'danger'],
        ];


        $grid->status(trans('admin.form_menu.status'))->switch($states);
        $grid->tools(function ($tools) {
            $tools->batch(function ($batch) {
                $batch->add(trans('admin.set_inactive'), new Inactive(0));
                $batch->add(trans('admin.set_active'), new Active(1));        
            });
        });

        $grid->actions(function ($actions) {
        $actions->append('<a href="'.url('/').'/admin/users/'.$actions->getKey().'/edit" class="editclick"><i class="fa fa-edit"></i></a>');
        $actions->disableEdit();
                
        });

        $grid->filter(function($filter){
                $filter->disableIdFilter();
                $filter->like('name', 'Name');
                $filter->like('email', 'Email');
                $filter->like('phone', 'Phone');
                $filter->like('language', 'Language');
                $filter->like('dob', 'DOB');
                $filter->equal('gender','Gender')->select(['0' => 'Female','1'=>'Male']);
                $filter->equal('status','status')->select(['0' => 'In Active','1'=>'Active']);
            });

        Admin::script('$( document ).ready(function() { $(".editclick").click(function() { location.reload(); }); $(".btn-success").click(function() { location.reload(); }); });');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Users::findOrFail($id));

        $show->id('Id');
        $show->name('Name');
        $show->lastname('Lastname');
        $show->photo('Photo');
        $show->email('Email');
        $show->password('Password');
        $show->phone('Phone');
        $show->language('Language');
        $show->dob('Dob');
        $show->gender('Gender');
        $show->id_cms_privileges('Id cms privileges');
        $show->facebook_id('Facebook id');
        $show->twitter_id('Twitter id');
        $show->googleplus_id('Googleplus id');
        $show->created_at('Created at');
        $show->updated_at('Updated at');
        $show->status('Status');
        $show->user_type('User type');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Users);

        $form->text('name', 'Name');
        $form->file('photo', 'Photo');
        $form->email('email', 'Email');
        $form->password('password', 'Password');
        $form->mobile('phone', 'Phone');
        $form->text('language', 'Language')->default('en');
        $form->date('dob', 'Dob')->default(date('Y-m-d'));
        $form->text('facebook_id', 'Facebook id');
        $form->text('twitter_id', 'Twitter id');
        $form->text('googleplus_id', 'Googleplus id');
        $form->number('gender', 'Gender');
        $form->switch('status');
        $form->text('user_type', 'User type');


        return $form;
    }

    public function saveusers(Request $request)
    {

        $username = $request->input('name');
        $phone = $request->input('phone');
        $phonecoutrycode = $request->input('phone_country_code');
        $status = $request->input('status');
        $pid = $request->input('pid');
        $deletecheck = $request->input('deletecheck');
        $password = $request->input('password');
        $picture_single ="";
        $imagecheck =0;
        $country_iso = explode('-', $phonecoutrycode);
        $country = Country::where('iso',$country_iso[1])->first()->name;
        if($request->has('photo'))
        {
            $image_single = $request->file('photo');
            $extension = $image_single->getClientOriginalExtension();
            $picture_single = date('YmdHis').".".$extension;
            $destinationPath = base_path() . '/public/uploads/Users/';
            $image_single->move($destinationPath, $picture_single);
            $imagecheck =1;
        }

        if($imagecheck=="0" && $deletecheck==0)
        $imagecheck=0;
        if($imagecheck=="0" && $deletecheck==1)
        $imagecheck=1;

        if($pid=="" || $pid==null || $pid==0)
        {
            $users = new Users;
            $users->created_at=  Carbon::now()->toDateTimeString();
            $users->password = bcrypt($password);
        }
        else
        {
            $users=Users::find($pid);
            if($password != $users->password)
            $users->password = bcrypt($password);
            $users->updated_at=  Carbon::now()->toDateTimeString();
        }

        $users->name=$username;

        if($imagecheck==1)
        $users->photo=$picture_single; 


        $users->email=$request->input('email');
        $users->phone=$phone;
        
        $users->phone_country_code=$phonecoutrycode;
        $users->language=$request->input('language');
        $users->country = $country;
        $users->dob=$request->input('dob');
        $users->gender=($request->has('gender')) ? $request->input('gender') : 0;
        if($request->has('facebook_id'))
        $users->facebook_id = $request->input('facebook_id');
        if($request->has('twitter_id'))
        $users->twitter_id = $request->input('twitter_id');
        if($request->has('googleplus_id'))
        $users->twitter_id = $request->input('googleplus_id');
        $users->status=$status;  
        $users->user_type="web";  
        $users->save();

        if($request->input('pid')!="")
        admin_toastr('User Updated successfully');
        else
        admin_toastr('User Created successfully');

        return redirect('admin/users');
        

    }

    public function userunique(Request $request)
    {
        $phoneid = $request->post('phone');
        $email = $request->post('email');
        $phonecode = $request->post('phone_country_code');
        $fbid = $request->post('facebook_id');
        $twid = $request->post('twitter_id');
        $gpid = $request->post('googleplus_id');
        $id = ($request->has('pid')) ? $request->post('pid') : "0";
        $errocode = 0;

        $duplicatecheck = Users::where('phone','=',$phoneid)->where('phone_country_code','=',$phonecode)->whereNOTIn('id',[$id])->first();
        if(is_object($duplicatecheck))
        $errocode = 1;

         if($errocode ==0 && $email!="")
        {
            $email = Users::where('email','=',$email)->whereNOTIn('id',[$id])->first();
            if(is_object($email))
            $errocode = 5;
        }
        if($errocode ==0 && $fbid!="")
        {
            $fbcheck = Users::where('facebook_id','=',$fbid)->whereNOTIn('id',[$id])->first();
            if(is_object($fbcheck))
            $errocode = 2;

        }
        if($errocode ==0 && $twid!="")
        {
            $twcheck = Users::where('twitter_id','=',$twid)->whereNOTIn('id',[$id])->first();
            if(is_object($twcheck))
            $errocode = 3;

        }
        if($errocode ==0 && $gpid!="")
        {
            $gpcheck = Users::where('googleplus_id','=',$gpid)->whereNOTIn('id',[$id])->first();
            if(is_object($gpcheck))
            $errocode = 4;
        } 
       
        echo $errocode;
        exit;
    }
}
