<?php

namespace App\Admin\Controllers;

use App\Models\Category;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Encore\Admin\Facades\Admin;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Contracts\Filesystem\Filesystem;
use Carbon\Carbon;
use DB;
use App\Admin\Extensions\Tools\Active;
use App\Admin\Extensions\Tools\Inactive;

class SubCategoryController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content,Request $request)
    {
        return $content
            ->header(trans('admin.form_menu.subcategory_management'))
            ->description('Listing')
            ->body($this->grid($request->all()));
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        $category = Category::with(['parentcategory'])->where('id',$id)->first();
        return $content
            ->header(trans('admin.form_menu.subcategory_management'))
            ->description(' ')
            ->body(view('subcategoryview',['category'=>$category]));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        $catname = DB::table('tbl_category')->where('status', 1)->where('parent_category_id', '=', 0)->pluck('category_name','id');
        $pid=$id;
        $image_array = array();
        $category=Category::find($id);
        if($category->category_image!="")
        $image_array[] = $category->category_image;
        return $content
        ->header(trans('admin.form_menu.subcategory_management'))
        ->description(' ')
        //->body($this->form()->edit($id))
        ->body(view('subcategory',['pid'=>$pid,'category'=>$category,'image_array'=>$image_array,'catname'=>$catname]));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        $catname = DB::table('tbl_category')->where('status', 1)->where('parent_category_id', '=', 0)->pluck('category_name','id');

        return $content
            ->header(trans('admin.form_menu.subcategory_management'))
            ->description(' ')
           // ->body($this->form());
            ->body(view('subcategory',['catname'=>$catname,'pid'=>'','image_array'=>array()]));
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid($request)
    {
        $grid = new Grid(new Category);
        $grid->disableExport();
        // $grid->model()->orderby('id','DESC');
           if(isset($request['_sort']['type']) && ($request['_sort']['type'] == "asc")){           
              $grid->model()->orderby($request['_sort']['column'],'asc')->where('parent_category_id','!=',0);
             
            }elseif(isset($request['_sort']['type']) && ($request['_sort']['type'] == "desc")){  
                     
              $grid->model()->orderby($request['_sort']['column'],'desc')->where('parent_category_id','!=',0);
            }else{
               
                $grid->model()->orderby('id','DESC')->where('parent_category_id','!=',0);
            }

         $grid->parent_category_id(trans('admin.form_menu.category_name'))->display(function () { 
            return ($this->parent_category_id!=0)? $this->parentcategory['category_name']:'-------';
        });

        $grid->category_name(trans('admin.form_menu.subcategory_name'))->sortable();
         
        // $grid->category_description('Category Description')->display(function ($category_description) 
        // {
        //     return str_limit( $category_description, 80, '...');
        // });  

       

        $grid->column(trans('admin.form_menu.subcategory_image'))->display(function () { 
            if($this->category_image!="")
            $img = '<img src="'.url('/').'/uploads/Category/'.$this->category_image.'" width="75px"/>';
            else
            $img = "No Image";
            return $img;
        });

        $states = [
            'on'  => ['value' => 1, 'text' => 'Active', 'color' => 'success'],
            'off' => ['value' => 0, 'text' => 'Inactive', 'color' => 'danger'],
        ];
        $grid->status(trans('admin.form_menu.status'))->switch($states);
        $grid->tools(function ($tools) {
            $tools->batch(function ($batch) {
                $batch->add(trans('admin.set_inactive'), new Inactive(0));
                $batch->add(trans('admin.set_active'), new Active(1));       
            });
        });
        $grid->filter(function($filter){
                $filter->disableIdFilter();
                $filter->like('category_name', trans('admin.form_menu.subcategory_name'));
                $filter->equal('status','status')->select(['0' => 'In Active','1'=>'Active']);
            });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Category::findOrFail($id));

        $show->category_name('Category name');
        $show->category_description('Category description');
        $show->category_image('Category image');
        $show->created_at('Created at');
        $show->updated_at('Updated at');
        $show->status('Status');
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Category);

        $form->text('category_name', 'Category name');
        $form->textarea('category_description', 'Category description');
        $form->text('category_image', 'Category image');
        $form->number('parent_category_id', 'Parent category id');
        $form->text('language', 'Language')->default('en');
        $form->number('status', 'Status')->default(1);
        $form->number('user_id', 'User id');

        return $form;
    }

    public function savecategory(Request $request)
    {
        $catname = $request->input('category_name');
        $catdesc = $request->input('category_description');
        $status = $request->input('status');
        $pid = $request->input('pid');
        $deletecheck = $request->input('deletecheck');
        $parentid = $request->input('parent_category_id');
        $picture_single ="";
        $imagecheck =0;

        if($request->has('category_image'))
        {
            $image_single = $request->file('category_image');
            $extension = $image_single->getClientOriginalExtension();
            $picture_single = date('YmdHis').".".$extension;
            $destinationPath = base_path() . '/public/uploads/Category/';
            $image_single->move($destinationPath, $picture_single);
            $imagecheck =1;
        }

        if($imagecheck=="0" && $deletecheck==0)
        $imagecheck=0;
        if($imagecheck=="0" && $deletecheck==1)
        $imagecheck=1;

        if($pid=="" || $pid==null)
        {
            $category = new Category;
            $category->created_at=  Carbon::now()->toDateTimeString();
        }
        else
        {
            $category=Category::find($pid);
            $category->updated_at=  Carbon::now()->toDateTimeString();
        }

        $category->category_name=$catname;
        $category->category_description=$catdesc;

        if($imagecheck==1)
        $category->category_image=$picture_single;

        $category->parent_category_id=$parentid;    
        $category->status=$status;    
        $category->save();

        if($request->input('pid')!="")
        admin_toastr('Category Updated successfully');
        else
        admin_toastr('Category Created successfully');

        return redirect('admin/subcategory');
    }


    public function categoryexist(Request $request)
    {
        $catname = $request->post('category_name');
        $id = ($request->has('pid')) ? $request->post('pid') : 0;
        $parentid = ($request->has('parent_category_id')) ? $request->post('parent_category_id') : 0;
        
        if(is_numeric($id))
            $catcheck=Category::where("category_name",'=',trim($catname))->where('parent_category_id',$parentid)->whereNOTIn('id',[$id])->first();
        else
            $catcheck=Category::where("category_name",'=',trim($catname))->where('parent_category_id',$parentid)->first();
                
        if($catcheck)
        {
            echo "failed";
            exit;
        }
        else
        {
            echo "success";
            exit;
        }
    }
    public function subcategorylist(Request $request)
    {
        $catid = $request->post('parent_category_id');
        $subcatlist=Category::where("parent_category_id",'=',$catid)->where('status',1)->get();
                
        if(count($subcatlist) > 0)
        {
            $returnresult = "<ul>";
            foreach($subcatlist as $sub_catlist)
            {
                $returnresult .="<li>".$sub_catlist->category_name."</li>";
            }
            $returnresult .= "</ul>";
            echo $returnresult;
            exit;
        }
        else
        {
            echo "No subcategories found";
            exit;
        }
    }

     public function subcategory(Request $request)
    {
        $catid = $request->post('parent_category_id');
        $subcatlist=Category::where("parent_category_id",'=',$catid)->where('status',1)->get();
        $selectedid = ($request->has('subchilid')) ? $request->post('subchilid') : 0;
        if(count($subcatlist) > 0)
        {
            $returnresult = '<select class="form-control status sub_cat_id" style="width: 100%;" name="sub_cat_id" id="sub_cat_id">';
            foreach($subcatlist as $sub_catlist)
            {
                if($selectedid == $sub_catlist->id)
                $returnresult .="<option value='".$sub_catlist->id."' selected >".$sub_catlist->category_name."</option>";
                else
                $returnresult .="<option value='".$sub_catlist->id."'>".$sub_catlist->category_name."</option>";

            }
            $returnresult .= "</select>";
            echo $returnresult;
            exit;
        }
        else
        {
            echo "No subcategories found";
            exit;
        }
    }
}
