<?php

namespace App\Admin\Controllers;

use App\Models\Report;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Http\Request;
use App\Exports\UsersExportData;
use App\Exports\CompanyExportData;
use Maatwebsite\Excel\Excel; 
use App\Models\Users;
use Encore\Admin\Facades\Admin;

class ReportController extends Controller
{
    use HasResourceActions;

    private $excel;

    public function __construct(Excel $excel)
    {
        $this->excel = $excel;
    }

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
         return $content
            ->header('Users Data Export')
            ->description('')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }
   


    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    { 
        return Admin::content(function (Content $content) {            
            $content->header('User Data Report');
            $content->description('');           
            $content->body(view('user_data_report'));                      
        });
    }


    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Report);
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Report::findOrFail($id));
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Report);
        return $form;
    }


    public function user_report(Content $content)
    {
        return Admin::content(function (Content $content) {            
            $content->header(trans('admin.form_menu.users__report'));
            $content->description('');           
            $content->body(view('user_data_report'));                        
        });
    }

    public function company_report(Content $content)
    {
         return Admin::content(function (Content $content) {            
            $content->header(trans('admin.form_menu.company__report'));
            $content->description('');           
            $content->body(view('company_report_form'));                        
        });  
    }

     public function ExportUserDataView(Request $request, Content $content)
      {          

        ob_end_clean();  
        return $this->excel->download(new UsersExportData($request->all()), 'users.csv');                     
      }
     public function ExportCompanyDataView(Request $request, Content $content)
      {          

        ob_end_clean();  
        return $this->excel->download(new CompanyExportData($request->all()), 'company.csv');                     
      }
         
      
           
      
      
}
