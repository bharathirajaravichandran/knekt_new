<?php

namespace App\Admin\Controllers;

use App\Models\Users;
use App\Models\Settings;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Encore\Admin\Facades\Admin;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Contracts\Filesystem\Filesystem;
use Carbon\Carbon;
use DB;

class SettingsController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header(trans('admin.form_menu.settings_management'))
            ->description('Listing')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        $setting = Settings::where('id',$id)->first();
        return $content
            ->header(trans('admin.form_menu.settings_management'))
            ->description(' ')
           // ->body($this->detail($id));
            ->body(view('settingsview',['setting'=>$setting])); 

    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
       
        $setting= Settings::find($id);        
        return $content
            ->header(trans('admin.form_menu.settings_management'))
            ->description(' ')
            //->body($this->form()->edit($id));
            ->body(view('settings',['sid'=>$id,'setting'=>$setting])); 
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header(trans('admin.form_menu.settings_management'))
            ->description('')
            //->body($this->form());
            ->body(view('settings',['sid'=>'0'])); 
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Settings);
        $grid->disableExport();
        $grid->disableCreateButton();        
        $grid->model()->orderby('id','DESC');
        $grid->google_apikey(trans('admin.form_menu.google_api_key'));
        $grid->actions(function ($actions) {
            $actions->disableDelete();
        $actions->append('<a href="'.url('/').'/admin/settings/'.$actions->getKey().'/edit" class="editclick"><i class="fa fa-edit"></i></a>');
        $actions->disableEdit();                
        });       
        Admin::script('$( document ).ready(function() { $(".editclick").click(function() { location.reload(); }); $(".btn-success").click(function() { location.reload(); }); });');
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Settings::findOrFail($id));

        $show->id('Id');
        $show->google_apikey('Google api key');
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Settings);
        $form->text('google_apikey', 'Google api key');
        return $form;
    }

    public function savesettings(Request $request)
    {  
        $sid = $request->input('sid');
        $settings_id = $request->input('google_apikey');
        if($request->input('sid') == 0)
        {
            $setting = new Settings;
            $setting->google_apikey= $settings_id;            
        }
        else
        {
            $setting=Settings::find($sid);            
            $setting->google_apikey = $settings_id;
        }       
        $setting->save();

        if($request->input('sid') != 0)
        admin_toastr('Setting Updated successfully');
        else
        admin_toastr('Setting Created successfully');

        return redirect('admin/settings');
        

    }

  
}
