<?php

namespace App\Admin\Controllers;

use App\Models\Cat_option;
use App\Models\Company;
use App\Models\Category;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Encore\Admin\Facades\Admin;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Contracts\Filesystem\Filesystem;
use DB;
use Auth;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

class AdminIvrDetailsController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header(trans('admin.form_menu.main_ivr'))
            ->description('')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        $ivr_value =Cat_option::find($id);
        $company = Company::find($ivr_value->company_id);
        $hour_time = explode(':', $ivr_value->time_delay);
        $time_delay = $hour_time[1].':'.$hour_time[2];
        return $content
            ->header(trans('admin.form_menu.main_ivr'))
            ->description('')
             ->body(view('ivrView',['company'=>$company,'ivr_value'=>$ivr_value,'time_delay'=>$time_delay]));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return Admin::content(function (Content $content) use ($id) {
            
            $content->header(trans('admin.form_menu.ivr_details'));
            $content->description('');
            $old_values = Cat_option::find($id);
            $times = $old_values->time_delay;
            $time_value=explode(':', $times);
            $companies = DB::table('tbl_service')->where('status', 1)->pluck('service_name','id');
            $languages=array('en'=>'English','ar'=>'Arabic');
            $content->body(view('ivrCreate',['companies'=>$companies,'languages'=>$languages,'old_values'=>$old_values,'time_value'=>$time_value,'pid'=>$id]));
                         //$content->body($this->form());
        });
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return Admin::content(function (Content $content) {
            
            $content->header(trans('admin.form_menu.ivr_details'));
            $content->description('');
            $companies = Company::where('status', 1)->pluck('service_name','id');
            $languages=array('en'=>'English','ar'=>'Arabic');
            $content->body(view('ivrCreate',['companies'=>$companies,'languages'=>$languages,'pid'=>'']));
                         //$content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Cat_option);
        // $grid->model();

        $grid->model()->where('parent_option_id',0)->orderBy('id', 'desc');

         $grid->tools(function ($tools) {
                $tools->append('<a href="'.url('/').'/admin/uploadivr" class="btn btn-sm btn-success" style="float: right;margin-right: 11px;">'.trans('admin.form_menu.import_ivr').'</a>');
            });

        // $grid->id('Id');
        $grid->company_name(trans('admin.form_menu.company_name'))->display(function () {
            $companyname=Company::find($this->company_id);
            if($companyname != ''){
               $value =$companyname->service_name;
            }else{
                $value ="";
            }
             return $value;
        });
        $grid->option_name(trans('admin.form_menu.language'))->sortable();
        $grid->option_value(trans('admin.form_menu.ivr_value'));
        // $grid->time_delay('Time Delay');
        $grid->column(trans('admin.form_menu.time_delay'))->display(function () {
             $hour_time = explode(':', $this->time_delay);
            $time_delay = $hour_time[1].':'.$hour_time[2];
            return $time_delay;
        });
        $grid->column(trans('admin.form_menu.sub_ivr'))->display(function () {
            $add_button = '
            <a href="add_subivr?company_id='.$this->company_id.'&id='.$this->id.'&lang='.$this->language.'" class="btn btn-primary btn-sm">'.trans('admin.form_menu.add').'</a>
            ';
            return $add_button;
        });
        $grid->column(trans('admin.form_menu.list'))->display(function () {
            $list = '
            <a href="list_ivr/'.$this->id.'?company_id='.$this->company_id.'&id='.$this->id.'&lang='.$this->language.'" class="btn btn-primary btn-sm">'.trans('admin.form_menu.list').'</a>
            ';
            return $list;
        });
         $grid->filter(function($filter){
            $filter->disableIdFilter();
            $filter->like('getCompanyName.service_name', trans('admin.form_menu.service_name'));
            $filter->equal('language')->radio([
                'en'    => 'English',
                'ar'    => 'Arabic',
            ]);        
            });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Cat_option::findOrFail($id));

        // $show->id('Id');
        $show->company_id('Company')->as(function ($company_id) {
                $company_name=Company::find($this->company_id)->service_name;
                return $company_name;
            });
        $show->option_name('Option name');
        $show->option_value('Option value');
        $show->phone('Phone');
        $show->alternatephone('Alternatephone');
        $show->time_delay('Time delay');
        // $show->parent_option_id('Parent option id');
        $show->panel()
        ->tools(function ($tools) {
            $tools->disableEdit();
            $tools->disableList();
            $tools->disableDelete();
        });
        // $show->language('Language');
        // $show->created_at('Created at');
        // $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Cat_option);
        $company[0]="Please Select Company";
               
        $companies = Company::where('status',1)->pluck('service_name','id');       
        foreach ($companies as $key => $value) {
                $company[$key] = $value;
        }

        $form->select('company_id', 'Company Name')->options($company)->attribute(['id'=>'company_id'])->rules('required');
         $form->select('language', 'IVR Language')->options(['en'=>'English','ar'=>'Arabic']);
         $form->html('<span id="ivrdetails"></span>');
        // $form->hidden('company_id', 'Company id');
        // $form->text('option_name', 'Option name');
        $form->text('option_value', 'IVR value');
        $form->text('phone', 'Phone');
        $form->text('alternatephone', 'Alternatephone');
        $form->time('time_delay', 'Time delay')->default(date('H:i:s'));
        $form->hidden('parent_option_id', 'Parent option id');
        $form->hidden('option_name', 'Parent option_name');
        // $form->hidden('token', 'token')->value({{ csrf_token() }});
        $form->saving(function (Form $form) {
            // dd($form->language);
            if($form->parent_option_id=='' || $form->parent_option_id==null){
                $form->parent_option_id=0;
            }
            if($form->language=='en'){
                $form->option_name='English';
            }
            if($form->language=='ar'){
                $form->option_name='Arabic';
            }
            // dd($form->option_name);
        });
        return $form;
    }
    public function get_company_phone(Request $request)
     {   
       
        $cat = DB::table('tbl_service')->where('id',$request->input('comp_id'))->select('phone')->first();  
        return response()->json($cat->phone);
                        
     }
      public function get_company_alterphone(Request $request)
     {   
        $cat = DB::table('tbl_service')->where('id',$request->input('comp_id'))->select('alternatephone')->first(); 
        return response()->json($cat->alternatephone);
                        
     } 
     public function get_ivr_details(Request $request)
     {  

        $company = Company::find($request->input('company_id'));
        $phone_country_code = explode('-', $company->phone_country_code);
        $phone_number = $phone_country_code[0].$company->phone;

        $cat = DB::table('tbl_catoptions')->where('company_id',$request->input('company_id'))->where('parent_option_id',0)->get(); 
        $subcat =array();
       
        foreach ($cat as $key => $value) {
            $subcat[] = $value->option_name.' = '.$value->option_value;
        }
        if($phone_number!=''){
            $phone_number=$phone_number;
        }else{
            $phone_number="No Phone Number";
        }
        if(count($subcat)!=0){
            $subcat = implode(',', $subcat);
        }else{
            $subcat ='No Data';
        }
         $datas=array('ivrdetail'=>$subcat,'phone'=>$phone_number);
        return response()->json($datas);
                        
     }
       // IVR already exists check
     public function get_ivr_value(Request $request)
     {   
        $data=0;
        $parent_id = $request->input('parent_id');
        $company_id =$request->input('comp_id');
        if($request->input('uri_segment') !='edit'){
           
                $ivr_option = DB::table('tbl_catoptions')->where('company_id',$request->input('comp_id'))->where('parent_option_id',0)->where('option_value',$request->input('option_value'))->get();
           
        }else{
            
                $ivr_option = DB::table('tbl_catoptions')->where('company_id',$request->input('comp_id'))->where('parent_option_id',0)->where('option_value',$request->input('option_value'))->where('id','!=',$request->input('old_id'))->get();
           
        }            
        if($ivr_option->count()!=0){
            $data =1;
        }else{
            $data =0;
        }
        
        return $data;   
     } 

      // IVR already exists check

     public function get_ivr_language(Request $request)
     {   
         // dd($request->all());
        $data=0;
        $parent_id = $request->input('parent_id');
        $company_id =$request->input('comp_id');
        $option_name =$request->input('option_name');
         if($request->input('uri_segment') !='edit'){
              $ivr_option = DB::table('tbl_catoptions')
              ->where('company_id',$request->input('comp_id'))
              ->where('parent_option_id',0)
              ->where('option_name',$option_name)
              ->get();
         }else{
             $ivr_option = DB::table('tbl_catoptions')
             ->where('company_id',$request->input('comp_id'))
             ->where('parent_option_id',0)
             ->where('option_name',$option_name)
             ->where('id','!=',$request->input('old_id'))
             ->get();
         }
        if($ivr_option->count()!=0){
            $data =1;
        }else{
            $data =0;
        }
        
        return $data;   
     }

     // Save IVR
     public function ivrsave(Request $request){

        // dd($request->all());
        $company = Company::find($request->input('company_id'));
         
           if($request->minutes !='' && $request->seconds !=''){
            if(strlen($request->minutes)==1){
                $minutes ='0'.$request->minutes;
            }else{
                $minutes =$request->minutes;
            }
            if(strlen($request->seconds)==1){
                 $seconds ='0'.$request->seconds;
            }else{
                 $seconds =$request->seconds;
            }

             $time_delay = '00:'.$minutes.':'.$seconds;
            }


        if($request->oid!='' || $request->oid!=null){
             $ivr_value=Cat_option::find($request->oid);
         }else{
             $ivr_value = new Cat_option;
         }
          

            $ivr_value->company_id = $request->input('company_id');
            $ivr_value->parent_option_id = 0;
            $ivr_value->language = strtolower(substr($request->input('option_name'), 0, 2));
            $ivr_value->phone = $company->phone;
            $ivr_value->alternatephone =  $company->alternatephone;
            $ivr_value->option_value = $request->input('option_value');
            $ivr_value->option_name =$request->input('option_name');
            $ivr_value->time_delay =$time_delay;
            $ivr_value->save();

       return redirect('admin/ivr_details');
     }

     public function ivr_upload_form(Request $request)
     {  
        $file = $request->file('ivr_upload') ;

        Excel::load($file->getRealPath(), function ($reader) {
            $j=0;
            $parent_id = 0;
            foreach ($reader->toArray() as $key => $row) {
                if(isset($row['category']) && $row['category']!="")
                { 
                  $checkcategoryexist =Category::where('category_name',"=",trim($row['category']))->first();
                  if(is_object($checkcategoryexist))
                  {
                    $category_id = $checkcategoryexist->id;
                  }
                  else
                  {
                    $category = new Category;
                    $category->category_name = trim($row['category']);
                    $category->category_description = trim($row['category']);
                    $category->parent_category_id = 0;
                    $category->language = "en";
                    $category->created_at = Carbon::now()->toDateTimeString();
                    $category->status = 1;
                    $category->save();
                    $category_id = $category->id;
                  }

                  if(isset($row['sub_category']) && $row['sub_category']!="")
                  {
                    $checksubcategoryexist =Category::where('category_name',"=",trim($row['sub_category']))->where('parent_category_id',$category_id)->first();
                    if(is_object($checksubcategoryexist))
                    {
                      $subcategory_id = $checksubcategoryexist->id;
                    }
                    else
                    {
                      $subcategory = new Category;
                      $subcategory->category_name = trim($row['sub_category']);
                      $subcategory->category_description = trim($row['sub_category']);
                      $subcategory->parent_category_id = $category_id;
                      $subcategory->language = "en";
                      $subcategory->created_at = Carbon::now()->toDateTimeString();
                      $subcategory->status = 1;
                      $subcategory->save();
                      $subcategory_id = $subcategory->id;
                    }


                    if(isset($row['company_name']) && $row['company_name']!="")
                    {
                      $checkcompanyexist =Company::where('service_name',"=",trim($row['company_name']))->where('parent_category_id',$category_id)->where('sub_cat_id',$subcategory_id)->first();
                      if(is_object($checkcompanyexist))
                      {
                        $companyid = $checkcompanyexist->id;
                        $cmpnyphonenumner = $checkcompanyexist->phone;
                      }
                      else
                      {
                        $phonenumber = explode(" ",trim($row['phone_number']));
                        $company = new Company;
                        $company->parent_category_id = $category_id;
                        $company->sub_cat_id = $subcategory_id;
                        $company->service_name = trim($row['company_name']);
                        $company->service_description = trim($row['company_name']);
                        $company->email = trim($row['email']);
                        $company->website = trim($row['web_address']);
                        $company->phone = (count($phonenumber)>=2) ? $phonenumber[1] :"0";
                        $company->phone_country_code = (count($phonenumber)>=2) ? $phonenumber[0] :"+965-kw";
                        $company->created_at = Carbon::now()->toDateTimeString();
                        $company->save();
                        $company_id = $company->id;
                        $cmpnyphonenumner = $company->phone;
                      }
                    }
                  }
                }

                if($row['level0']!="")
                { 
                    $parent_id=0;
                    $i=0;
                    $chk=1;
                    $resultset[][$row['level'.($i+1)]] = $row['level'.$i];
                }
                else
                {
                    $chk=1;
                    $parent_id=1;
                    if(isset($row['level'.$i]) || $row['level'.$i] == "")
                    {
                        if($row['level'.$i]=="" || is_numeric($row['level'.$i]))
                        {
                            for($j=0;$j<=$i;$j++)
                            {
                                if($row['level'.$j]!="" && !is_numeric($row['level'.$j]))
                                {
                                    if(isset($row['level'.($j+1)]))
                                    {
                                        if($row['level'.($j+1)]!="")
                                        {
                                            $i=$j;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    $chk=0;
                    /*if(isset($row['level'.($i+1)]) && $row['level'.($i+1)]!="" && $checked==2)
                    $resultset[][$row['level'.($i+1)]] = $row['level'.$i];*/
                }
                if($chk ==1 && (isset($company_id) && $company_id!=""))
                {
                    $catoptioncheck = Cat_option::where('company_id',$company_id)->where('option_name',"=",$row['level'.$i])->where('option_value',$row['level'.($i+1)])->first();
                    if(!is_object($catoptioncheck))
                    {
                      $option = new Cat_option;
                      $option->company_id = (isset($company_id) && $company_id!="") ? $company_id : 1; 
                      $option->option_name = $row['level'.$i]; 
                      $option->option_value = $row['level'.($i+1)]; 
                      $option->phone = $cmpnyphonenumner;
                      $option->alternatephone = "";
                      $option->time_delay = "00:00:10";
                      if($parent_id == 0)
                      $option->parent_option_id = 0;
                      else
                      $option->parent_option_id = $parent['level'.($i-1)];
                      $option->language = "en";
                      $option->created_at = Carbon::now()->toDateTimeString();
                      $option->user_id = 0;
                      $option->save();
                      $parent['level'.$i] = $option->id;
                      $i++;
                    }
                }
            }
        });

        admin_toastr('Excel Data imported successfully');

        return redirect('admin/ivr_details');
    }

    public function uploadivr(Content $content)
    {
        return $content
            ->header(trans('admin.form_menu.main_ivr'))
            ->description(' ')
           // ->body($this->form());
            ->body(view('ivrupload'));
    }
    public function list_ivr(Content $content,Request $request)
    {


        $id = $request->segment(3); 
        $company_id = Input::get('company_id', false);
        $preid = Input::get('id');
        $lang = Input::get('lang');
        $parent = Cat_option::find($id);
        $company = Company::find($parent->company_id)->service_name;
        $language = $parent->option_name;
        $categories = Cat_option::where('parent_option_id', '=', $id)->get();        
        $allCategories = Cat_option::pluck('option_name','id')->all();      
       /* return view('categoryTreeview',compact('categories','allCategories'));*/
        return $content
            ->header('IVR Structure')
            ->description(' ')
           // ->body($this->form());
            ->body(view('categoryTreeview',compact('categories','allCategories','language','company')));
    }
     public function maps(Content $content)
    {
          return $content
            ->header('Example Maps')
            ->description('Maps')
           // ->body($this->form());
            ->body(view('maps'));
                   
    }

}
