<?php

namespace App\Admin\Controllers;

use App\Models\Company;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

use Encore\Admin\Facades\Admin;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Contracts\Filesystem\Filesystem;
use Carbon\Carbon;
use DB;


class CompanyController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content,Request $request)
    {
        return $content
            ->header(trans('admin.form_menu.company_management')
        )
            ->description('Listing')
            ->body($this->grid($request->all()));
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
    	$company = Company::with(['category','subcategory'])->where('id',$id)->first();
        // dd($company);
        return $content
            ->header(trans('admin.form_menu.company_management'))
            ->description(' ')
           ->body(view('companyview',['company'=>$company]));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
    	$catname = DB::table('tbl_category')->where('status', 1)->where('parent_category_id', '=', 0)->pluck('category_name','id');
    	$pid=$id;
        $image_array = array();
        $company=Company::find($id);
        if($company->service_image!="")
        $image_array[] = $company->service_image;

        return $content
            ->header(trans('admin.form_menu.company_management'))
            ->description(' ')
          //  ->body($this->form()->edit($id));
            ->body(view('company',['pid'=>$pid,'company'=>$company,'image_array'=>$image_array,'catname'=>$catname]));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
    	$catname = DB::table('tbl_category')->where('status', 1)->where('parent_category_id', '=', 0)->pluck('category_name','id');
        return $content
            ->header(trans('admin.form_menu.company_management'))
            ->description(' ')
            //->body($this->form());
            ->body(view('company',['catname'=>$catname,'pid'=>'','image_array'=>array()]));
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid($request)
    {
        $grid = new Grid(new Company);
        $grid->disableExport();
        // $grid->model()->orderby('id','DESC');

        if(isset($request['_sort']['type']) && ($request['_sort']['type'] == "asc")){           
          $grid->model()->orderby($request['_sort']['column'],'asc');
         
        }elseif(isset($request['_sort']['type']) && ($request['_sort']['type'] == "desc")){  
                 
          $grid->model()->orderby($request['_sort']['column'],'desc');
        }else{
           
            $grid->model()->orderby('id','DESC');
        }


 		$grid->service_name(trans('admin.form_menu.company_name'))->sortable();

        $grid->parent_category_id(trans('admin.form_menu.category'))->display(function () {
            return ($this->parent_category_id!=0)? $this->category['category_name']:'No Category';
        })->sortable();

        $grid->sub_cat_id(trans('admin.form_menu.subcategory'))->display(function () {
            return ($this->sub_cat_id!=0)? $this->subcategory['category_name']:'No Subcategory';
        })->sortable();

        $grid->column(trans('admin.form_menu.company_image'))->display(function () { 
            if($this->service_image!="")
            $img = '<img src="'.url('/').'/uploads/Company/'.$this->service_image.'" width="75px"/>';
            else
            $img = "No Image";
            return $img;
        });

        $grid->phone(trans('admin.form_menu.phone'))->display(function () {
            return ($this->phone!=0 && $this->phone!="")? $this->phone:'--------';
        })->sortable();

        $states = [
            'on'  => ['value' => 1, 'text' => 'Active', 'color' => 'success'],
            'off' => ['value' => 0, 'text' => 'Inactive', 'color' => 'danger'],
        ];
        $grid->status(trans('admin.form_menu.status'))->switch($states);
        $grid->actions(function ($actions) {
        $actions->append('<a href="'.url('/').'/admin/company/'.$actions->getKey().'/edit" class="editclick"><i class="fa fa-edit"></i></a>');
        $actions->disableEdit();
                
        });
        $grid->filter(function($filter){
            $filter->disableIdFilter();
            $filter->like('service_name', 'Service Name');
            $filter->like('category.category_name', 'Category');
            $filter->like('subcategory.category_name', 'Subcategory');
            $filter->like('Phone', 'Phone');
            $filter->like('Sms', 'SMS');
            $filter->like('Email', 'Email');
            $filter->like('Whatsapp', 'Whatsapp');
            $filter->equal('status','status')->select(['0' => 'In Active','1'=>'Active']);
        });
         Admin::script('$( document ).ready(function() { $(".editclick").click(function() { location.reload(); }); $(".btn-success").click(function() { location.reload(); }); });');
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Company::findOrFail($id));

        $show->id('Id');
        $show->parent_category_id('Parent category id');
        $show->sub_cat_id('Sub cat id');
        $show->service_name('Service name');
        $show->service_description('Service description');
        $show->service_image('Service image');
        $show->notification('Notification');
        $show->phone('Phone');
        $show->alternatephone('Alternatephone');
        $show->sms('Sms');
        $show->email('Email');
        $show->whatsapp('Whatsapp');
        $show->web('Web');
        $show->website('Website');
        $show->linkedin('Linkedin');
        $show->language('Language');
        $show->user_id('User id');
        $show->created_at('Created at');
        $show->updated_at('Updated at');
        $show->status('Status');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
    	$latitude = "29.3117° N";
    	$longitude = "47.4818° E";
    	$label ="sample";
        $form = new Form(new Company);

        $form->number('parent_category_id', 'Parent category id');
        $form->number('sub_cat_id', 'Sub cat id');
        $form->text('service_name', 'Service name');
        $form->textarea('service_description', 'Service description');
        $form->text('service_image', 'Service image');
        $form->textarea('notification', 'Notification');
        $form->mobile('phone', 'Phone');
        $form->text('alternatephone', 'Alternatephone');
        $form->text('sms', 'Sms');
        $form->email('email', 'Email');
        $form->text('whatsapp', 'Whatsapp');
        $form->text('web', 'Web');
        $form->text('website', 'Website');
        $form->text('linkedin', 'Linkedin');
        $form->text('language', 'Language')->default('en');
        $form->number('user_id', 'User id');
         $form->switch('status');
     

        return $form;
    }

    public function savecompany(Request $request)
    {


    	$servicename = $request->input('service_name');
        $servicedesc = $request->input('service_description');
        $status = $request->input('status');
        $openingtime = $request->input('opening_time');
        $pid = $request->input('pid');
        $deletecheck = $request->input('deletecheck');
        $parentid = $request->input('parent_category_id');
        $subcatid = ($request->has('sub_cat_id')) ? $request->input('sub_cat_id') : 0;
        if($request->input('opening_time')==1){
            $working_time = $request->input('starting_time').' - '.$request->input('closing_time');
        }else{
            $working_time = '00:00:00 - 23:59:59';
        }
        
        $picture_single ="";
        $imagecheck =0;

        if($request->has('service_image'))
        {
            $image_single = $request->file('service_image');
            $extension = $image_single->getClientOriginalExtension();
            $picture_single = date('YmdHis').".".$extension;
            $destinationPath = base_path() . '/public/uploads/Company/';
            $image_single->move($destinationPath, $picture_single);
            $imagecheck =1;
        }

        if($imagecheck=="0" && $deletecheck==0)
        $imagecheck=0;
        if($imagecheck=="0" && $deletecheck==1)
        $imagecheck=1;

        if($pid=="" || $pid==null)
        {
            $company = new Company;
            $company->created_at=  Carbon::now()->toDateTimeString();
        }
        else
        {
            $company=Company::find($pid);
            $company->updated_at=  Carbon::now()->toDateTimeString();
        }

        $company->parent_category_id=$parentid;    
        $company->sub_cat_id=$subcatid;  
        $company->service_name=$servicename;
        $company->service_description=$servicedesc;
        if($imagecheck==1)
        $company->service_image=$picture_single;

        //$company->notification=$request->input('notification');
        $company->phone=$request->input('phone');
        $company->phone_country_code=$request->input('phone_country_code');
        if($request->has('alternatephone'))
        $company->alternatephone = $request->input('alternatephone');
    	if($request->has('alter_country_code'))
        $company->alter_country_code = $request->input('alter_country_code'); 
    	if($request->has('sms'))
        $company->sms = $request->input('sms');
    	if($request->has('sms_country_code'))
        $company->sms_country_code = $request->input('sms_country_code'); 
    	if($request->has('whatsapp'))
        $company->whatsapp = $request->input('whatsapp');
    	if($request->has('whatsapp_country_code'))
        $company->whatsapp_country_code = $request->input('whatsapp_country_code');

    	if($request->has('email'))
        $company->email = $request->input('email');
    	if($request->has('web'))
        $company->web = $request->input('web');
    	if($request->has('website'))
        $company->website = $request->input('website');
    	if($request->has('linkedin'))
        $company->linkedin = $request->input('linkedin');

    	$company->user_id = Admin::user()->id;
        $company->status = $status;    
        $company->opening_time = $openingtime;    
        $company->working_time = $working_time;    
        $company->save();

        if($request->input('pid')!="")
        admin_toastr('Company Updated successfully');
        else
        admin_toastr('Company Created successfully');

        return redirect('admin/company');

    }

    public function servicename(Request $request)
    {
    	$servicename = $request->post('service_name');
        $id = ($request->has('pid')) ? $request->post('pid') : 0;
        $parentid = ($request->has('parent_category_id')) ? $request->post('parent_category_id') : 0;
        $childid = ($request->has('sub_cat_id')) ? $request->post('sub_cat_id') : 0;
        
        if(is_numeric($id))
            $companycheck=Company::where("service_name",'=',trim($servicename))->where('parent_category_id',$parentid)->where('sub_cat_id',$childid)->whereNOTIn('id',[$id])->first();
        else
            $companycheck=Company::where("service_name",'=',trim($servicename))->where('parent_category_id',$parentid)->where('sub_cat_id',$childid)->first();
                
        if($companycheck)
        {
            echo "failed";
            exit;
        }
        else
        {
            echo "success";
            exit;
        }
    }
}
