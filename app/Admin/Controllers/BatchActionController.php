<?php

namespace App\Admin\Controllers;

use App\Models\Category;
use App\Models\Company;
use App\Models\Companybranch;
use App\Models\Users;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Http\Request;

class BatchActionController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Category);

        $grid->id('Id');
        $grid->category_name('Category name');
        $grid->category_description('Category description');
        $grid->category_image('Category image');
        $grid->parent_category_id('Parent category id');
        $grid->language('Language');
        $grid->created_at('Created at');
        $grid->updated_at('Updated at');
        $grid->status('Status');
        $grid->user_id('User id');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Category::findOrFail($id));

        $show->id('Id');
        $show->category_name('Category name');
        $show->category_description('Category description');
        $show->category_image('Category image');
        $show->parent_category_id('Parent category id');
        $show->language('Language');
        $show->created_at('Created at');
        $show->updated_at('Updated at');
        $show->status('Status');
        $show->user_id('User id');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Category);

        $form->text('category_name', 'Category name');
        $form->textarea('category_description', 'Category description');
        $form->text('category_image', 'Category image');
        $form->number('parent_category_id', 'Parent category id');
        $form->text('language', 'Language')->default('en');
        $form->number('status', 'Status')->default(1);
        $form->number('user_id', 'User id');

        return $form;
    }

     public function active(Request $request)
    {

        if($request->url_data == 'category'){
            foreach (Category::find($request->get('ids')) as $post) {
                $post->status = 1;
                $post->save();
             }
        }
        if($request->url_data == 'company'){
            foreach (Company::find($request->get('ids')) as $post) {
                $post->status = 1;
                $post->save();
             }
        }
        if($request->url_data == 'companybranch'){
            foreach (Companybranch::find($request->get('ids')) as $post) {
                $post->status = 1;
                $post->save();
             }
        }
        if($request->url_data == 'users'){
            foreach (Users::find($request->get('ids')) as $post) {
                $post->status = 1;
                $post->save();
             }
        }
        
         $responses = array(
          'status' => 'status',
          'message' =>'Updated successfully',                
          );
      return response()->json($responses);
    }

     public function inactive(Request $request)
    {
        if($request->url_data == 'category'){
            foreach (Category::find($request->get('ids')) as $post) {
                $post->status = 0;
                $post->save();
             }
        }
        if($request->url_data == 'company'){
            foreach (Company::find($request->get('ids')) as $post) {
                $post->status = 0;
                $post->save();
             }
        }
        if($request->url_data == 'companybranch'){
            foreach (Companybranch::find($request->get('ids')) as $post) {
                $post->status = 0;
                $post->save();
             }
        }
        if($request->url_data == 'users'){
            foreach (Users::find($request->get('ids')) as $post) {
                $post->status = 0;
                $post->save();
             }
        }

       
         $responses = array(
          'status' => 'status',
          'message' =>'Updated successfully',                
          );
      return response()->json($responses);
    }
}
