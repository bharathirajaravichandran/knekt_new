<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
   
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {    
    $router->get('/', 'HomeController@index');
    $router->post('savecategory', 'CategoryController@savecategory');
    $router->post('categoryexist', 'CategoryController@categoryexist');
    $router->post('subcategorylist', 'CategoryController@subcategorylist');
    $router->post('subcategory', 'CategoryController@subcategory');

    $router->post('savesubcategory', 'SubCategoryController@savecategory');
    $router->post('categoryexist', 'SubCategoryController@categoryexist');
    $router->post('subcategorylist', 'SubCategoryController@subcategorylist');
    $router->post('subcategory', 'SubCategoryController@subcategory');
    
    $router->post('savecompany', 'CompanyController@savecompany');
    $router->post('servicename', 'CompanyController@servicename');

    $router->post('saveusers', 'UsersController@saveusers');
    $router->post('userunique', 'UsersController@userunique');

    $router->post('savecompanybranch', 'CompanybranchController@savecompanybranch');
    $router->post('companybranchcheck', 'CompanybranchController@companybranchcheck');

    $router->post('ivr_upload_form', 'AdminIvrDetailsController@ivr_upload_form');
    $router->get('uploadivr', 'AdminIvrDetailsController@uploadivr');

    $router->get('/add_subivr', 'AdminSubIvrDetailsController@index');
    $router->get('/add_subivr/{id}/create', 'AdminSubIvrDetailsController@create');
    $router->get('/add_subivr/{id}/view', 'AdminSubIvrDetailsController@view');
    $router->get('/add_subivrs/{id}/create', 'AdminSubIvrDetailsController@create');
    $router->get('/add_subivr/{id}/edit', 'AdminSubIvrDetailsController@edit');
    $router->get('/add_subivr/detail/{id}', 'AdminSubIvrDetailsController@show');
    $router->post('/get_sub_ivr_details', 'AdminSubIvrDetailsController@get_sub_ivr_details');
    $router->post('/get_sub_ivr_value', 'AdminSubIvrDetailsController@get_ivr_value');
    $router->post('/get_sub_service_name', 'AdminSubIvrDetailsController@get_service_name');
    $router->post('/add_subivr/saveprd','AdminSubIvrDetailsController@subivrsave');

    $router->post('/get_company_phone','AdminIvrDetailsController@get_company_phone');
    $router->post('/get_company_alterphone','AdminIvrDetailsController@get_company_alterphone');
    $router->post('/get_sub_categories','AdminSubcategoryController@get_sub_categories');
    $router->post('/get_ivr_details','AdminIvrDetailsController@get_ivr_details');
    $router->post('/get_ivr_value','AdminIvrDetailsController@get_ivr_value');
    $router->post('/get_ivr_language','AdminIvrDetailsController@get_ivr_language');
    $router->post('/saveprds','AdminIvrDetailsController@ivrsave');


    $router->get('/list_ivr/{id}','AdminIvrDetailsController@list_ivr');
    
    $router->get('/user_report','ReportController@user_report');
    $router->post('/export_user_data_view','ReportController@ExportUserDataView'); 
   /* $router->post('/export_user_data_download','ReportController@ExportUserDataDownload');*/ 

    $router->get('/company_report','ReportController@company_report');
    $router->post('/export_company_data_view','ReportController@ExportCompanyDataView'); 
    /*$router->post('/export_company_data_download','ReportController@ExportCompanyDataDownload'); */
    
    $router->post('active', 'BatchActionController@active');
    $router->post('inactive', 'BatchActionController@inactive');

    $router->get('/maps', 'AdminIvrDetailsController@maps');
    
    $router->post('savesettings', 'SettingsController@savesettings');
    $router->resources([
        'category' => CategoryController::class,
    	'subcategory' => SubCategoryController::class,
    	'company'  => CompanyController::class, 
        'companybranch'  => CompanybranchController::class,
    	'ivr_details'  => AdminIvrDetailsController::class,
        'users'  => UsersController::class,
        'settings'  => SettingsController::class,
    ]);
});
