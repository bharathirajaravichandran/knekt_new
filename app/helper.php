<?php
use App\Models\Company;
use App\Models\Company_location;
use App\Models\Favourite;
use App\Models\Cat_option;
use App\Models\Category;


function file_upload($data,$type,$old_image){	
	$ProfileImagedate = md5(date('Y-m-d H:i:s'));
	$ProfileImageData = base64_decode($data);
	$profileImageFilename = $ProfileImagedate.rand().".png";
	$location = $type.'/'.$profileImageFilename;		
	 	if(file_exists(storage_path('app/uploads/3/'.$type.'/'.$old_image)))
     	{    		   
   		   @unlink(storage_path('app/uploads/3/'.$type.'/'.$old_image));
     	}	 
	$path = \Storage::disk('public')->put($location, $ProfileImageData);
	//\Storage::put($profileImageFilename, $ProfileImageData);
	return $profileImageFilename;
 }

 function createNewDirectory()
{
	$sCurrDate = date("Y-m"); //Current Date
	$sDirPath  = storage_path('app/uploads/3/'.$sCurrDate); //Specified Pathname
	$path      = 'uploads/3/'.$sCurrDate;		
	if(!file_exists($sDirPath))
   	{   		
	    	mkdir($sDirPath,0777,true);  
    }    
    return $path ;
}
function array_null_values_replace($result)
	{
          if(is_array($result))
          { 
            array_walk_recursive($result, function (&$item, $key)
             {  
               $item = null === $item ? '' : $item;
             });
            return $result;
          }                 
          else
           {  
                $r['Status'] = 'Failed';
                $r['message'] = "Failed";
                return response()->json($r);
           }
    }
function getCompanyColumn($id,$column_name)
  {   
    $find_dats = Company::where('id',$id)->get();
       if($find_dats->count() !=0){
    $data = Company::find($id);
    return $data->$column_name;
          }
          $data = '';
          return $data;
  }

function getCompanyData($id)
  {   
    $find_dats = Company::where('id',$id)->where('status',1)->orderBy('service_name','asc')->get();
       if($find_dats->count() !=0){
            $data = Company::find($id);
            return $data->toArray();
          }
          $data = '';
          return $data;
  }

  function getCompanyDetailData($comp_id,$id)
  {   
    $find_dats = Company::where('id',$comp_id)->where('status',1)->orderBy('service_name','asc')->get();
       if($find_dats->count() !=0){
            $compdata = Company::find($comp_id);
            $compdatas = Company_location::find($id);
            $data=$compdata;
            $data->latitude=$compdatas->latitude;
            $data->langitude=$compdatas->langitude;
            $data->address=$compdatas->address;
            return $data->toArray();
          }
          $data = '';
          return $data;
  }
function getCompanyBranchData($id,$column_name)
{   
  $find_data = Company_location::where('company_id',$id)->where('status',1)->get();
     if($find_data->count() !=0){
          $data = Company_location::where('company_id',$id)->first();
          return $data->$column_name;
        }
        $data = 0;
        return $data;
}
function getFavouriteStatus($company_id,$user_id,$valuelocation,$valueid,$apiname)
{   
      if($valuelocation == 'companypage')  
      {
         $Favourite = Favourite::where('company_id',$company_id)
                                ->where('user_id',$user_id)
                                ->where('valuelocation',$valuelocation)
                                ->where('valueid',$valueid)
                                ->where('apiname',$apiname)
                                ->get();
         return $Favourite->count();
      } 
      if($valuelocation == 'companysocialpage')   
      {
         $Favourite = Favourite::where('company_id',$company_id)
                                ->where('user_id',$user_id)
                                ->where('valuelocation',$valuelocation)
                                ->where('value',$valueid)
                                ->where('valueid',0)
                                ->where('apiname',$apiname)
                                ->get();                               
         return $Favourite->count();
      }
      if($valuelocation == 'companybranchpage')   
      {
         $Favourite = Favourite::where('company_id',$company_id)
                                ->where('user_id',$user_id)
                                ->where('valuelocation',$valuelocation)
                                ->where('valueid',$valueid)
                                ->where('apiname',$apiname)
                                ->get();
         return $Favourite->count();
      }
      if($valuelocation == 'companycallpage')   
      {  
         $Favourite = Favourite::where('company_id',$company_id)
                                ->where('user_id',$user_id)
                                ->where('valuelocation',$valuelocation)
                                ->where('valueid',$valueid)
                                ->where('apiname',$apiname)
                                ->get();
         return $Favourite->count();
      }
  
}
function getCategoryData($id)
{   
  $find_dats = Category::where('id',$id)->where('status',1)->orderBy('category_name','asc')->get();
     if($find_dats->count() !=0){
          $data = Category::find($id);
          return $data->toArray();
        }
        $data = 1;
        return $data;
}

function basepath(){
  $path ='http://knekt.dci.in/';
  return $path;
}

function getparentdata($id)
  {   
       $count = Cat_option::where('parent_option_id',$id)->get()->count();
       return $count;
  }


 ?>