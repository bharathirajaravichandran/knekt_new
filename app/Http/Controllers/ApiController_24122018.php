<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use DB;
use App\Models\Category;
use App\Models\Company;  
use App\Models\Users;
use App\Models\Company_location;
use App\Models\Country;
use App\Models\Cat_option;
use App\Models\Favourite;
use App\Models\Recentcategory;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\PDF;
use Maatwebsite\Excel\Facades\Excel;
use CRUDBooster;
use CB;
use Schema;
use DOMDocument;
use App\Models\Call_option;
// use Mail;

class ApiController extends Controller
{
      public function Signup(Request $request){  
           /*error_log(print_r($_REQUEST,true),3,'/var/www/html/error.log');*/ 
           if($request->filled('phone') && $request->filled('password') && $request->filled('country_code'))
             {   
                $country_code = str_replace('+', '', $request->country_code);               
                $iso = strtolower(Country::where('phonecode',$country_code)->first()->iso);
                $country =Country::where('phonecode',$country_code)->first()->name;
                $phone_country_code = $request->country_code.'-'.$iso;
                $user_mobile_check = Users::where('phone',$request->phone)->where('phone_country_code',$phone_country_code)->get();
                if($user_mobile_check->count() !=0){
                     $response['msg']    = config('resource.mobile_number_exists');
                     $response['status'] = config('resource.status_420');                   
                     return response()->json($response,config('resource.status_200')); }

                /*$check = createNewDirectory();*/               
                //profile upload start
           	    $user_data = new Users();
                $user_data->phone               =  $request->phone;
                $user_data->phone_country_code  =  $phone_country_code;
                $user_data->country             =   $country;
                $user_data->password            =  bcrypt($request->password);
                $user_data->photo               = 'uploads/default_image/avatar.png';
                $user_data->user_type           = 'app';             
                $user_data->id_cms_privileges   =  3 ;
                $user_data->status              =  1 ;
  			        $user_data->save();
               //profile upload end              
               $response['msg']    = config('resource.signup_success');
      			   $response['status'] = config('resource.status_200');
      			   $response['data']   = array_null_values_replace(Users::find($user_data->id)->toArray());
  			       return response()->json($response,config('resource.status_200'));
             }else
             {
                $response['msg'] = config('resource.parameter_missing');
  			        $response['status'] = config('resource.status_420');
  			        return response()->json($response,config('resource.status_200'));
             }
      }

      public function Signin(Request $request){   
        /*error_log(print_r($_REQUEST,true),3,'/var/www/html/error.log');*/
           if($request->filled('phone') && $request->filled('password') && $request->filled('country_code'))
             {    $phone = $request->phone ;
             	    $password = $request->password ;
                   $country_code = str_replace('+', '', $request->country_code);               
                   $iso = Country::where('phonecode',$country_code)->first()->iso;
                  $phone_country_code = $request->country_code.'-'.$iso;
                  $user_check = Users::where('phone',$request->phone)->where('phone_country_code',$phone_country_code)->where('id_cms_privileges',3)->get();
                   if($user_check->count() !=0){
                   	 if(password_verify($password,$user_check->first()->password)){
                   	 	//login success
                      $response['status']    = config('resource.status_200');
                      $response['msg']     = config('resource.login_success');
                      $response['data']   = array_null_values_replace($user_check->first()->toArray());
                      return response()->json($response,config('resource.status_200'));
                   	 }else{
                      //login failed, password wrong
                      $response['msg']    = config('resource.password_wrong');
                      $response['status'] = config('resource.status_420');
                      return response()->json($response,config('resource.status_200'));
                   	 }
                  }else{ 
                     	//user not found
                   	  $response['msg']   = config('resource.user_doesnt_exists'); 
          					  $response['status'] = config('resource.status_420');
          					  return response()->json($response,config('resource.status_200'));
                  } 
              }else{
                      $response['msg']   = config('resource.parameter_missing');
        			        $response['status'] = config('resource.status_420');
        			        return response()->json($response,config('resource.status_200'));
                }
      }


      public function checkUserExist(Request $request){   
        /*error_log(print_r($_REQUEST,true),3,'/var/www/html/error.log');*/
           if($request->filled('phone') && $request->filled('country_code'))
             {    
                   $country_code = str_replace('+', '', $request->country_code);               
                   $iso = Country::where('phonecode',$country_code)->first()->iso;
                  $phone_country_code = $request->country_code.'-'.$iso;
                  $phone = $request->phone ;               
                  $user_check = Users::where('phone',$request->phone)->where('phone_country_code',$phone_country_code)->where('id_cms_privileges',3)->get();
                   if($user_check->count() !=0){                   
                      //login success
                      $response['status']    = config('resource.status_200');
                      $response['msg']       = config('resource.user_exists');
                      $response['data']      = array_null_values_replace($user_check->first()->toArray());
                      return response()->json($response,config('resource.status_200'));                   
                   }else{ 
                      //user not found
                      $response['msg']   = config('resource.user_doesnt_exists'); 
                      $response['status'] = config('resource.status_420');
                      return response()->json($response,config('resource.status_200'));
                   }

             }else{
                $response['msg']   = config('resource.parameter_missing');
                $response['status'] = config('resource.status_420');
                return response()->json($response,config('resource.status_200'));
           }
      }

      public function Forgetpassword(Request $request){   
        if($request->filled('id') && $request->filled('password'))
             {    $password = $request->password ;           	    
                  $user_check = Users::where('id',$request->id)->get();               
                   if($user_check->count() !=0){
                    $user_check = Users::find($request->id);
                   	//password update
                   	$user_check->password = bcrypt($request->password); 
                   	$user_check->save();
                    $response['msg']    = config('resource.password_updated_successfully');
                    $response['status'] = config('resource.status_200');
                    $response['data']   = array_null_values_replace($user_check->toArray());
                    return response()->json($response,config('resource.status_200'));

                   }else{ 
                   	  //user not found
                   	  $response['msg']   = config('resource.record_not_found');
  					          $response['status'] = config('resource.status_420');
  					          return response()->json($response,config('resource.status_200'));
                   }

             }else{
                $response['msg']   = config('resource.parameter_missing');
        			  $response['status'] = config('resource.status_420');
        			  return response()->json($response,config('resource.status_200'));
           }
        }

      public function Profile(Request $request) { 
       if($request->filled('type'))
               {    
              if($request->type == 'get'){
                        $user_check = Users::where('id',$request->user_id)->get();
    	                 if($user_check->count() !=0){
    	                 	//get data
                          $phoneValues = '';
                          $user_check = Users::find($request->user_id);
                          $phoneValues =explode('-', $user_check->phone_country_code);
                          $user_check->country_code =$phoneValues[0];
                          $user_check->country_iso = strtoupper($phoneValues[1]);
    	                 	  $response['data']   = array_null_values_replace($user_check->toArray());
    						          $response['status'] = config('resource.status_200');
    						          return response()->json($response,config('resource.status_200'));
    	                 }else{ 
    	                 	  //user not found
    	                 	  $response['msg']   = config('resource.record_not_found');
    						          $response['status'] = config('resource.status_420');
    						          return response()->json($response,config('resource.status_200'));
    	                 }
              }elseif($request->type == 'update'){
                     //get data
                // dd($request->all());
              	     $user_check = Users::where('id',$request->user_id)->get();
  	                 if($user_check->count() !=0){        	                 	  
  	                 	  //update function start 
                        $user_check = Users::find($request->user_id);  
                         //mobile check start 
                         $country_code = str_replace('+', '', $request->country_code);               
                         $iso = Country::where('phonecode',$country_code)->first()->iso;
                         $phone_country_code = $request->country_code.'-'.$iso;
                         $mobile_check = Users::where('phone',$request->phone)->where('phone_country_code',$phone_country_code)->whereNotIn('id',[$request->user_id])->get(); 
                         if($mobile_check->count() !=0){
                            $response['msg']    = config('resource.mobile_number_exists');
                            $response['status'] = config('resource.status_420');                   
                            return response()->json($response,config('resource.status_200'));
                             }
                             if($request->email!=''){
                               $email_check = Users::where('email',$request->email)->get();
                              if($email_check->count() !=0){
                                $response['msg']    = config('resource.email_exists');
                                $response['status'] = config('resource.status_420');                   
                                return response()->json($response,config('resource.status_200'));
                                 }
                             }
                         

                             //mobile check end                            
                        $exclude_inputs = array('id_cms_privileges','photo','password','type','user_id');
                        foreach($request->all() as $key=>$value){
                              if(!in_array($key,$exclude_inputs))
                                  $user_check->$key = $value;
                           }
                         if($request->filled('password')){ 
                          $user_check->password = bcrypt($request->password);
                         }
                         if ($request->filled('photo')) {
                          $ProfileImagedate = date('YmdHis');
                          $profileImageFilename = $ProfileImagedate.".png";                        
                          $ProfileImageData = base64_decode($request->photo);
                         $path = \Storage::disk('user_image_uploads')->put($profileImageFilename,$ProfileImageData);                      
                         $user_check->photo =  $profileImageFilename; 
                        }
                         	                 	   
                   	    $user_check->save();
                        $response['msg']    = config('resource.updated_successfully');
                        $response['status'] = config('resource.status_200');
                        $response['link'] = basepath()."uploads/Users";
                        $response['data']   = array_null_values_replace($user_check->toArray());
                   	    return response()->json($response,config('resource.status_200'));
  	                 }else{ 
  	                 	  //user not found
  	                 	  $response['msg']    = config('resource.record_not_found');
  						          $response['status'] = config('resource.status_420');
  						          return response()->json($response,config('resource.status_200'));
  	                 }

              }else{
              	  //type parameter mis maching
                 $response['data']   = config('resource.type_missing');
                 $response['status'] = config('resource.status_420');
                 return response()->json($response,config('resource.status_200'));
              }
               }else{
                $response['data']   = config('resource.parameter_missing');
  			        $response['status'] = config('resource.status_420');
  			        return response()->json($response,config('resource.status_200'));
           }
      }

      public function GetCategories(Request $request){                  
            $category = Category::where('parent_category_id',0)->where('status',1)->orderBy('category_name','asc')->jsonPaginate();
            if($request->filled('search_key')){
               $category = Category::where('parent_category_id',0)->where('status',1)->orderBy('category_name','asc')->where('category_name','like','%'.$request->search_key.'%')->jsonPaginate();
            }
            if($category->count() !=0)
                 {   
                    //categories data  
                    $response['status'] = config('resource.status_200');           
                    $response['data']   = $category;  
                     $response['link'] = basepath()."uploads/Category";     
                    return response()->json($response,config('resource.status_200')); 
                 }else{
                    //no categories
                    $response['msg']   = config('resource.record_not_found');
                    $response['status'] = config('resource.status_420');
                    return response()->json($response,config('resource.status_200'));
               }
      }

      public function GetSubcategories(Request $request){   
        if($request->filled('category_id') && $request->filled('user_id')){
        //recent data set start
          if($request->filled('user_id')){
          $Recentcategory = Recentcategory::where('user_id',$request->user_id)
                                          ->where('category_id',$request->category_id)
                                          ->orderBy('updated_at','asc')
                                          ->get();
            if($Recentcategory->count() ==0){    //new create
              $data = new Recentcategory();
              $data->category_id = $request->category_id ;
              $data->user_id     = $request->user_id ;
              $data->created_at  = date('Y-m-d H:i:s');
              $data->save(); 
            }else{
              $Recentcategory->first()->updated_at = date('Y-m-d H:i:s');
              $Recentcategory->first()->save();
            }
          $count = Recentcategory::where('user_id',$request->user_id)->orderBy('updated_at','asc')->get();                     
            if($count->count() > 15){                           
              $deleteUs = Recentcategory::take($count->count())->skip(15)->orderBy('updated_at','DESC')->get();                
            foreach($deleteUs as $deleteMe){
             Recentcategory::where('id',$deleteMe->id)->delete();
            }
            }                      
          }                  
          //recent data set end

          //get subcategories
          if($request->type == 'subcategory'){             
            $subcategory = Company::where('parent_category_id',$request->category_id)
                                  ->where('sub_cat_id',$request->subcategory_id)                      
                                  ->orderBy('service_name','DESC')
                                  ->get();                                   
            if($request->filled('search_key')){
              $subcategory = Company::where('parent_category_id',$request->category_id)
                                    ->where('sub_cat_id',$request->subcategory_id)                  
                                    ->where('service_name','like','%'.$request->search_key.'%')
                                    ->orderBy('service_name','DESC')
                                    ->get();
            }
            if($subcategory->count() !=0){  
            //categories data 
              $response['status'] = config('resource.status_200');             
              $response['data']   = $subcategory ;    
              $response['link'] = basepath()."uploads/Category";             
              return response()->json($response,config('resource.status_200'));                    

            }else{                  
              //no categories
              $response['msg']    = config('resource.record_not_found');
              $response['status'] = config('resource.status_420');
              return response()->json($response,config('resource.status_200'));
            }
          //get subcategories
          }else{               
          $subcategory = Category::where('parent_category_id',$request->category_id)
                                  ->where('parent_category_id','!=',0)
                                  ->orderBy('created_at','DESC')
                                  ->get();
            if($request->filled('search_key')){
            $subcategory = Category::where('parent_category_id',$request->category_id)
                                    ->where('parent_category_id','!=',0)
                                    ->where('category_name','like','%'.$request->search_key.'%')
                                    ->orderBy('created_at','DESC')
                                    ->get();
            }
            if($subcategory->count() !=0){  
            //categories data 
              $response['status'] = config('resource.status_200');             
              $response['data']   = $subcategory ;  
              $response['link'] = basepath()."uploads/Category";               
              return response()->json($response,config('resource.status_200'));                    

            }else{
            //no categories
              $response['msg']    = config('resource.record_not_found');
              $response['status'] = config('resource.status_420');
              return response()->json($response,config('resource.status_200'));
            }
          }
        } else {
          $response['msg']   = config('resource.parameter_missing');
          $response['status'] = config('resource.status_420');
          return response()->json($response,config('resource.status_200'));
        }
      }

      public function GetCompanyData(Request $request)
      {   
        if($request->filled('company_id') && $request->filled('type'))
          {   
              if($request->type == 'get'){  
                     $find_company = Company::where('id',$request->company_id)->get();                   
                     if($find_company->count() ==0){
                        $response['msg']    = config('resource.record_not_found');
                        $response['status'] = config('resource.status_420');
                        return response()->json($response,config('resource.status_200'));
                     }
          $find_company->first()->longitude=getCompanyBranchData($find_company->first()->id,'langitude');      
          $find_company->first()->latitude=getCompanyBranchData($find_company->first()->id,'latitude');
          $find_company->first()->bookmark = getFavouriteStatus($find_company->first()->id,null,'companystatus');
          $favourite = Favourite::where('company_id',$request->company_id)->where('user_id',$request->user_id)->get();
          $companyTime = DB::table('tbl_company_time')->where('company_id',$request->company_id)->first();
            if($companyTime){
              $find_company->first()->call_time = $companyTime->call_time;
            }else{
              $find_company->first()->call_time ="";
            }
            if($favourite->count()!=0){
              $find_company->first()->favourite=1;
            }else{
              $find_company->first()->favourite=0;
            }

              $response['link'] = basepath()."uploads/Company";   
              $response['status'] = config('resource.status_200');  
              $response['data']   = $find_company->first()->toArray() ;    

                  return response()->json($response,config('resource.status_200'));
              }elseif($request->type == 'branch'){            
              $Company_location = Company_location::where('company_id',$request->company_id)->jsonPaginate();
              if($request->filled('search_key')){
              $Company_location = Company_location::where('company_id',$request->company_id)->where('category_name','like','%'.$request->search_key.'%')->jsonPaginate();  
              }
              if($Company_location->count() !=0)
                 {  
                    $company_name_add = array();
                    //Company_location data    
                    foreach ($Company_location as $key => $value){
                                   # code...
                      // dd($value);
                            $value->bookmark     = getFavouriteStatus($value->company_id,null,'companystatus');
                            $value->company_data = getCompanyDetailData($value->company_id,$value->id);     
                            $company_name_add[]  = $value ;
                            }  
                  $response['status'] = config('resource.status_200');                 
                  $response['data']   = $Company_location ; 
                  $response['link'] = basepath()."uploads/Company";               
                  return response()->json($response,config('resource.status_200'));
                 }else{
                    //no Company_location
                    $response['msg']    = config('resource.record_not_found');
                    $response['status'] = config('resource.status_420');
                    return response()->json($response,config('resource.status_200'));
               }
             }else{  
                    $response['msg']    = config('resource.type_missing');
                    $response['status'] = config('resource.status_420');
                    return response()->json($response,config('resource.status_200'));
                  }
          }else{
                $response['msg']    = config('resource.parameter_missing');
                $response['status'] = config('resource.status_420');
                return response()->json($response,config('resource.status_200'));
           }
      }

      /**
   * Undocumented function
   *
   * @param Request $request
   * @return void
   */
  public function getCountry(Request $request) {
    $country = Country::where('flag','!=','')->get();
    //dd($country);
    foreach($country as $countries)
    {
      $countries->flag = 'images/flags/'.$countries->flag;
      $coun[] = $countries;

    }
    $response['data'] = $coun;
    $response['status'] = config('resource.status_200');
    return response()->json($response,config('resource.status_200'));
      }

    /**
   * Undocumented function
   *
   * @param Request $request
   * @return void
   */
  public function GetCompanyKnekt(Request $request){
    if($request->filled('company_id'))
          {   
              if($request->type == 'get'){                  
               $find_company = Company::where('id',$request->company_id)->get();                   
               if($find_company->count() ==0){
                  $response['msg']    = config('resource.record_not_found');
                  $response['status'] = config('resource.status_420');
                  return response()->json($response,config('resource.status_200'));
               }   
                $datas =array();
                $newValue =array();
               // dd($find_company->first()->toArray());
               foreach ($find_company->first()->toArray() as $keys => $values) {
                 $attribute=array('phone','alternatephone','sms','email','whatsapp','web','linkedin','website');

                  foreach ($attribute as $key => $value) {                     
                    if($keys == $value){
                        $newattr = $keys.'_icon';                        
                        $newValue['status'] = ($values!='') ? 1 : 0;
                        $newValue['name'] = $keys;
                        $newValue['value'] = $values;
                        $newValue['icon']='/images/icon/'.$keys.'.png';
                        $datas[]=$newValue;
                    }
                  }
              // $find_company->first()->datas.=$datas; 
                 }    
                   // dd($datas);
              $find_company->first()->datas=$datas;
              $find_company->first()->bookmark           = getFavouriteStatus($request->company_id,null,'companystatus');         
              $find_company->first()->phone_bookmark     = 0 ;         
              $find_company->first()->sms_bookmark       = 0 ;         
              $find_company->first()->email_bookmark     = 0 ;         
              $find_company->first()->whatsapp_bookmark  = 0 ;         
              $find_company->first()->bookmark_web       = 0 ;         
              $find_company->first()->website_web        = 0 ;  
                
               $response['data']   = $find_company->first()->toArray(); 
               $response['status'] = config('resource.status_200');
              // $response['data']   = array_filter($find_company->first()->toArray());           
                     
                 
              return response()->json($response,config('resource.status_200'));
              }elseif($request->type == 'call'){
                     $company_data = Cat_option::where('company_id',$request->company_id)->get();
                  if($request->parent_option_id !=0){                                       
                    $company_data = Cat_option::where('company_id',$request->company_id)
                                            ->where('parent_option_id',$request->parent_option_id)
                                            ->jsonPaginate();
                     }  
                                     
                     if($company_data->count() ==0){                      
                        $response['msg']    = config('resource.record_not_found');
                        $response['status'] = config('resource.status_420');
                        return response()->json($response,config('resource.status_200'));
                     }  
                     $company_data = Cat_option::where('company_id',$request->company_id)
                         ->where('parent_option_id',0)->jsonPaginate();
                      if($request->parent_option_id !=0){                                            
                         $company_data = Cat_option::where('company_id',$request->company_id)
                                            ->where('parent_option_id',$request->parent_option_id)
                                            ->jsonPaginate();
                      }   
                  /* dd('sakjdfhk');*/
                  //save method start
                   if($request->company_id !='' && $request->parent_option_id !='' && $request->user_id !='')
                   {
                       if($request->parent_option_id ==0)
                              { 
                                Call_option::where('userid',$request->user_id)->where('companyid',$request->company_id)->delete();
                              }
                       
                        $companyData = Company::find($request->company_id);
                        $Call_option = Call_option::where('userid',$request->user_id)->where('companyid',$request->company_id)->get();
                        if($Call_option->count() == 0)
                        {   
                             $newdata = new Call_option();
                             $newdata->userid      = $request->user_id;
                             $newdata->companyid   = $request->company_id;
                             $newdata->callnumber  = $companyData->phone;
                             $newdata->optionslist = $request->parent_option_id ;
                             $newdata->save();
                        }else
                        {    

                             $newdata = Call_option::find($Call_option->first()->id);
                             $old_data   = array_unique(explode(',',$newdata->optionslist));                            
                             $new_data   = array($request->parent_option_id);
                             $final_data = array_merge($old_data,$new_data);                             
                             $newdata->userid      = $request->user_id;
                             $newdata->companyid   = $request->company_id;
                             $newdata->callnumber  = $companyData->phone;                                                       
                             $newdata->optionslist = implode(',',array_unique($final_data)) ;
                             $newdata->save();

                        }
                             

                   }
                //save method end



                          $bookmark_add = array();
                          //Company_location data    
                          foreach ($company_data as $key => $value)
                             {                                      
                                        $value->bookmark     = getFavouriteStatus($request->company_id,null,'companystatus');  
                                        $value->checksubitems = getparentdata($value->id);
                                         $value->icon='/images/icon/phone.png';
                                         if(isset($newdata))
                                         {
                                            $value->callnumber  = $newdata->callnumber ;
                                            /*$value->optionslist = implode('',array_shift(explode(',',$newdata->optionslist)));*/
                                            $value->optionslist = substr($newdata->optionslist,2);

                                         }
                                         $bookmark_add[]  = $value ;
                            }
                        
                     $response['status'] = config('resource.status_200');               
                     $response['data']   = $company_data ;           
                     return response()->json($response,config('resource.status_200'));
              }elseif($request->type == 'subcate'){                
                     $find_company = Cat_option::where('company_id',$request->company_id)
                                            ->where('parent_option_id',$request->parent_option_id)
                                            ->get();                   
                     if($find_company->count() ==0){
                        $response['msg']    = config('resource.record_not_found');
                        $response['status'] = config('resource.status_420');
                        return response()->json($response,config('resource.status_200'));
                     }  
                     $company_data = Cat_option::where('company_id',$request->company_id)
                         ->where('parent_option_id',$request->parent_option_id)->jsonPaginate();
                          $bookmark_add = array();
                          //Company_location data    
                          foreach ($company_data as $key => $value){                                      
                                        $value->bookmark     = getFavouriteStatus($request->company_id,null,'companystatus') ;                                               
                                        $bookmark_add[]  = $value ;
                                  } 
                     $response['status'] = config('resource.status_200');              
                     $response['data']   = $company_data ;         
                     $response['link'] = basepath()."uploads/Company";   
                     return response()->json($response,config('resource.status_200'));
              }else{
                 $response['msg']    = config('resource.parameter_missing');
                 $response['status'] = config('resource.status_420');
                 return response()->json($response,config('resource.status_200'));
             }
            }else{
                 $response['msg']    = config('resource.parameter_missing');
                 $response['status'] = config('resource.status_420');
                 return response()->json($response,config('resource.status_200'));
             }
          }

    public function AddFavourite(Request $request){
      if($request->filled('company_id') && $request->filled('user_id'))
            {  


               if($request->requesttype == 'companydata')
               {
                    if($request->actiontype == 'add')
                    {
                       $Favourite = Favourite::where('company_id',$request->company_id)
                                             ->where('user_id',$request->user_id)
                                             ->where('value',$request->value)
                                             ->where('valueid',$request->valueid)
                                             ->where('valuelocation',$request->valuelocation)   
                                             ->where('apiname',$request->apiname) 
                                             ->where('parent_ids',$request->parent_ids)               
                                             ->get();
                       if($Favourite->count() ==0)
                          {
                            $newFavourite = new Favourite();
                            $newFavourite->company_id = $request->company_id;
                            $newFavourite->user_id    = $request->user_id;
                            $newFavourite->value      = ($request->value !='') ? $request->value : 'Novalue' ;
                            $newFavourite->valueid    = ($request->valueid !='') ? $request->valueid : 'Novalue' ;
                            $newFavourite->valuelocation=($request->valuelocation !='') ? $request->valuelocation:'Novalue';
                            $newFavourite->parent_ids = ($request->parent_ids !='') ? $request->parent_ids : 'Novalue';
                            $newFavourite->apiname    = ($request->apiname !='') ? $request->apiname : 'Novalue';
                            $newFavourite->save();
                            $response['msg']    = config('resource.added_successfully');
                            $response['favourite']    = 1;
                            $response['status'] = config('resource.status_200');
                            return response()->json($response,config('resource.status_200'));
                           }else{
                            $Favourite->first()->delete();
                            $response['msg']    = config('resource.removed_successfully');
                            $response['favourite']    = 0;
                            $response['status'] = config('resource.status_200');
                            return response()->json($response,config('resource.status_200'));
                          }
                    }
                    $response['msg']    = 'Actiontype method is wrong';
                    $response['status'] = config('resource.status_420');
                    return response()->json($response,config('resource.status_200'));

               }elseif($request->requesttype == 'socialdata'){

                    if($request->actiontype == 'add')
                    {
                       $Favourite = Favourite::where('company_id',$request->company_id)
                                             ->where('user_id',$request->user_id)
                                             ->where('value',$request->value)
                                             ->where('valueid',$request->valueid)
                                             ->where('valuelocation',$request->valuelocation)   
                                             ->where('apiname',$request->apiname)                
                                             ->get();
                       if($Favourite->count() ==0)
                          {
                            $newFavourite = new Favourite();
                            $newFavourite->company_id = $request->company_id;
                            $newFavourite->user_id    = $request->user_id;
                            $newFavourite->value      = ($request->value !='') ? $request->value : 'Novalue' ;
                            $newFavourite->valueid    = ($request->valueid !='') ? $request->valueid : 'Novalue' ;
                            $newFavourite->valuelocation=($request->valuelocation !='') ? $request->valuelocation:'Novalue';
                            $newFavourite->parent_ids = ($request->parent_ids !='') ? $request->parent_ids : 0 ;
                            $newFavourite->apiname    = ($request->apiname !='') ? $request->apiname : 'Novalue';
                            $newFavourite->save();
                            $response['msg']    = config('resource.added_successfully');
                            $response['favourite']    = 1;
                            $response['status'] = config('resource.status_200');
                            return response()->json($response,config('resource.status_200'));
                           }else{
                            $Favourite->first()->delete();
                            $response['msg']    = config('resource.removed_successfully');
                            $response['favourite']    = 0;
                            $response['status'] = config('resource.status_200');
                            return response()->json($response,config('resource.status_200'));
                          }
                    }
                    $response['msg']    = 'Actiontype method is wrong';
                    $response['status'] = config('resource.status_420');
                    return response()->json($response,config('resource.status_200'));

               }elseif($request->requesttype == 'branchdata'){
                  if($request->actiontype == 'add')
                      {
                        $Favourite = Favourite::where('company_id',$request->company_id)
                                               ->where('user_id',$request->user_id)
                                               ->where('value',$request->value)
                                               ->where('valueid',$request->valueid)                
                                               ->where('valuelocation',$request->valuelocation)   
                                               ->where('apiname',$request->apiname)                
                                               ->get();
                         if($Favourite->count() ==0){
                              $newFavourite = new Favourite();
                              $newFavourite->company_id = $request->company_id;
                              $newFavourite->user_id    = $request->user_id;
                              $newFavourite->value      = ($request->value !='') ? $request->value : 'Novalue' ;
                              $newFavourite->valueid    = ($request->valueid !='') ? $request->valueid : 'Novalue' ;
                              $newFavourite->valuelocation=($request->valuelocation !='') ? $request->valuelocation:'Novalue';
                              $newFavourite->parent_ids = ($request->parent_ids !='') ? $request->parent_ids : 0 ;
                              $newFavourite->apiname    = ($request->apiname !='') ? $request->apiname : 'Novalue';
                              $newFavourite->save();
                              $response['msg']    = config('resource.added_successfully');
                              $response['favourite']    = 1;
                              $response['status'] = config('resource.status_200');
                              return response()->json($response,config('resource.status_200'));
                         }else{
                              $Favourite->first()->delete();
                              $response['msg']    = config('resource.removed_successfully');
                              $response['favourite']    = 0;
                              $response['status'] = config('resource.status_200');
                              return response()->json($response,config('resource.status_200'));
                         }
                     }
                    $response['msg']    = 'Actiontype method is wrong';
                    $response['status'] = config('resource.status_420');
                    return response()->json($response,config('resource.status_200'));
               }elseif($request->requesttype == 'calldata'){
                  if($request->actiontype == 'add')
                      {
                        $Call_option = Call_option::where('userid',$request->user_id)->where('companyid',$request->company_id)->get();
                        $Favourite = Favourite::where('company_id',$request->company_id)
                                               ->where('user_id',$request->user_id)
                                               ->where('value',$request->value)
                                               ->where('valueid',$request->valueid)                                  
                                               ->where('valuelocation',$request->valuelocation)   
                                               ->where('apiname',$request->apiname)                
                                               ->get();                         
                        if($Favourite->count() ==0){
                              $newFavourite = new Favourite();
                              $newFavourite->company_id = $request->company_id;
                              $newFavourite->user_id    = $request->user_id;
                              $newFavourite->value      = ($request->value !='') ? $request->value : 'Novalue' ;
                              $newFavourite->valueid    = ($request->valueid !='') ? $request->valueid : 'Novalue' ;
                              $newFavourite->valuelocation=($request->valuelocation !='') ? $request->valuelocation:'Novalue';
                              $newFavourite->parent_ids =  ($Call_option) ? $Call_option->first()->optionslist : $request->parent_ids;
                              $newFavourite->apiname    =  ($request->apiname !='') ? $request->apiname : 'Novalue';
                              $newFavourite->save();
                              $response['msg']    = config('resource.added_successfully');
                              $response['favourite']    = 1;
                              $response['status'] = config('resource.status_200');
                              return response()->json($response,config('resource.status_200'));
                         }else{
                              $Favourite->first()->delete();
                              $response['msg']    = config('resource.removed_successfully');
                              $response['favourite']    = 0;
                              $response['status'] = config('resource.status_200');
                              return response()->json($response,config('resource.status_200'));
                         }
                     }
                    $response['msg']    = 'Actiontype method is wrong';
                    $response['status'] = config('resource.status_420');
                }else{ 
               $response['msg']    = config('resource.parameter_missing');
               $response['status'] = config('resource.status_420');
               return response()->json($response,config('resource.status_200'));
             }
       }else{ 
               $response['msg']    = config('resource.parameter_missing');
               $response['status'] = config('resource.status_420');
               return response()->json($response,config('resource.status_200'));
            }

        }

   public function Getfavourites(Request $request){
      if($request->filled('user_id'))
            {     
                if($request->filled('company_id') && $request->filled('user_id'))  
                   $Favourite = Favourite::where('user_id',$request->user_id)->where('company_id',$request->company_id)->get();
                else         
                    $Favourite = Favourite::where('user_id',$request->user_id)->groupBy('company_id')->get();          
              
               if($Favourite->count() !=0){ 
                   $company_data =array();
                   foreach ($Favourite as $key => $value) {                                    
                                        $value->company_data = getCompanyData($value->company_id);
                                        $company_data = $value ;
                                     }                  
                  $response['data']    = $Favourite->toArray();
                   $response['link'] = basepath()."uploads/Company";  
                  $response['status']  = config('resource.status_200');
                return response()->json($response,config('resource.status_200'));                  
               }else{                
                  $response['msg']    = config('resource.no_data');
                  $response['status'] = config('resource.status_420');
                  return response()->json($response,config('resource.status_200'));
               }           
            }else{ 
                 $response['msg']    = config('resource.parameter_missing');
                 $response['status'] = config('resource.status_420');
                 return response()->json($response,config('resource.status_200'));
            }
        }
        
    public function GetRecentCategory(Request $request){
      if($request->filled('user_id'))
            {                
                $Recentcategory = Recentcategory::where('user_id',$request->user_id)
                                                ->orderBy('updated_at','DESC')->get();       
                $Recentcategorys =array();                              
               if($Recentcategory->count() !=0){ 

                   $category_data =array();
                   foreach ($Recentcategory as $key => $value) {    
                   $categories = getCategoryData($value->category_id); 

                     if($categories!=1){
                       $value->category_data = $categories;
                       $Recentcategorys[]=$value;
                     }else{
                      // $value->category_data='ff';

                      unset($Recentcategory[$key]); 

                     }                
                   }     
                
                    $response['data']    =  $Recentcategorys;
                     $response['link'] = basepath()."uploads/Category";  
                    $response['status']  = config('resource.status_200');
                    return response()->json($response,config('resource.status_200'));  
                              
               }else{                
                  $response['msg']    = config('resource.no_data');
                  $response['status'] = config('resource.status_420');
                  return response()->json($response,config('resource.status_200'));
               }           
            }else{ 
                 $response['msg']    = config('resource.parameter_missing');
                 $response['status'] = config('resource.status_420');
                 return response()->json($response,config('resource.status_200'));
            }
        }  
      public function checkSocialUserExist(Request $request){   
        /*error_log(print_r($_REQUEST,true),3,'/var/www/html/error.log');*/
           if($request->filled('email'))
             {    $email = $request->email ;               
                  $user_check = Users::where('email',$request->email)->where('id_cms_privileges',3)->get();
                   if($user_check->count() !=0){                   
                      //login success
                      $response['status']    = config('resource.status_200');
                      $response['msg']       = config('resource.user_exists');
                      $response['data']      = array_null_values_replace($user_check->first()->toArray());
                      return response()->json($response,config('resource.status_200'));                   
                   }else{ 
                      //user not found
                      $response['msg']   = config('resource.user_doesnt_exists'); 
                      $response['status'] = config('resource.status_420');
                      return response()->json($response,config('resource.status_200'));
                   }

             }else{
                $response['msg']   = config('resource.parameter_missing');
                $response['status'] = config('resource.status_420');
                return response()->json($response,config('resource.status_200'));
           }
      }

       public function SocialSignup(Request $request){  
         
            if($request->filled('email')){
               $user_checks = Users::where('email',$request->email)->where('id_cms_privileges',3)->get();    
               // dd($user_checks);          
                if($user_checks->count() !=0){
                  $user_check = $user_checks->first();

                   if($request->login_type == 'facebook'){
                     $user_check->facebook_id = $request->login_id;
                  }
                  if($request->login_type == 'twitter'){
                     $user_check->twitter_id = $request->login_id;
                  }
                  if($request->login_type == 'googleplus'){
                     $user_check->googleplus_id = $request->login_id;
                  }
                   $user_check->save();
                    // dd($user_check); 
                   $user_check->isRegistered =1;
                  $response['status']    = config('resource.status_200');
                  $response['msg']       = config('resource.update_success');
                  $response['data']      = array_null_values_replace($user_check->toArray());
                  return response()->json($response,config('resource.status_200')); 
                } else  {
                  $newUser = new Users;                  
                  $newUser->email = $request->email;                
                  if($request->login_type == 'facebook'){
                     $newUser->facebook_id = $request->login_id;
                  }
                  if($request->login_type == 'twitter'){
                     $newUser->twitter_id = $request->login_id;
                  }
                  if($request->login_type == 'googleplus'){
                     $newUser->googleplus_id = $request->login_id;
                  }                
                  $newUser->photo             = 'uploads/default_image/avatar.png';
                  $newUser->user_type         = 'app';             
                  $newUser->id_cms_privileges =  3 ;
                  $newUser->status            =  1 ;
                  $newUser->save();
                  $newUsers =Users::find($newUser->id);
                  $newUsers->isRegistered =0;
                  $response['msg']    = config('resource.signup_success');
                 $response['status'] = config('resource.status_200');
                 $response['data']   = array_null_values_replace($newUsers->toArray());
                 return response()->json($response,config('resource.status_200'));                  
               }
            }else {
            $response['msg'] = config('resource.parameter_missing');
            $response['status'] = config('resource.status_420');
            return response()->json($response,config('resource.status_200'));
          }
       }

        public function Forgotpassword(Request $request){  
           if($request->filled('phone') && $request->filled('country_code')){
                  $country_code = str_replace('+', '', $request->country_code);               
                  $iso = Country::where('phonecode',$country_code)->first()->iso;
                  $phone_country_code = $request->country_code.'-'.$iso;
               $userfinds = Users::where('phone',$request->phone)->where('id_cms_privileges',3)->get();
               
                if($userfinds->count() !=0){
                  $userfind = $userfinds->first();
                  
                  if($userfind->email !=null && $userfind->email !=''){
                  $datas['to'] = $userfind->email ; 
                  $datas['name'] = $userfind->name.'.'.$userfind->lastname ; 
                  // $datas['id'] = ; 
                  $datas['link'] = url('forgotpassword/'.encrypt($userfind->id)); 
                  $datas['subject'] = 'Reset Your Password' ;
                  Mail::send('email.forgotpassword',$datas, function($message) use ($datas){  
                            $message->to($datas['to']);         
                            $message->subject($datas['subject']);           
                        });
                       
                    $response['msg']       = config('resource.password_reset');         
                     $response['status']    = config('resource.status_200'); 
                    return response()->json($response,config('resource.status_200')); 
                  }else{
                      // dd($userfind->email);
                    $response['msg']       = config('resource.email_doesnt_exists'); 
                    $response['status']    = config('resource.status_200');         
                    return response()->json($response,config('resource.status_200')); 
                  }
                   
                }else{
                   
                  $response['msg']       = config('resource.user_doesnt_exists');  
                  $response['status']    = config('resource.status_200');         
                  return response()->json($response,config('resource.status_200')); 
                }
           }else{
             $response['msg'] = config('resource.parameter_missing');
              $response['status'] = config('resource.status_420');
              return response()->json($response,config('resource.status_200'));
           }
        }
        

        public function SearchCategories(Request $request){   
           if($request->filled('search_key')){ 
           $category=array();
              $company = Company::where('status',1)
              ->orderBy('service_name','asc')
              ->where('service_name','like','%'.$request->search_key.'%')
              ->get();   
               $categories = Category::where('status',1)->where('parent_category_id',0)->orderBy('category_name','asc')
               ->where('category_name','like','%'.$request->search_key.'%')
               // ->union($company)
               ->get();
               $subcategory = Category::where('status',1)->where('parent_category_id','!=',0)->orderBy('category_name','asc')
               ->where('category_name','like','%'.$request->search_key.'%')
               // ->union($company)
               ->get();
               $category['category'] = $categories;
               $category['subcategory'] = $subcategory;
               $category['company'] = $company;
               $category['category_image_url'] = basepath()."uploads/Category";   
               $category['subcategory_image_url'] = basepath()."uploads/Category";   
               $category['company_image_url'] = basepath()."uploads/Company";
               
           if(count($category['category']) !=0 || count($category['subcategory']) !=0 || count($category['company']) !=0 ){  
            //categories data 
              $response['status'] = config('resource.status_200');             
              $response['data']   = $category ;            
              return response()->json($response,config('resource.status_200'));                    

            }else{                  
              //no categories
              $response['msg']    = config('resource.record_not_found');
              $response['status'] = config('resource.status_420');
              return response()->json($response,config('resource.status_200'));
            }

           }else{
               $response['msg'] = config('resource.parameter_missing');
              $response['status'] = config('resource.status_420');
              return response()->json($response,config('resource.status_200'));
           }
        }

        public function SearchFavourites(Request $request){   
           if($request->filled('search_key') && $request->filled('user_id')){ 
            // dd($request->user_id);          
            // dd($Favourite);
               $Favourite = Favourite::where('user_id',$request->user_id)->orderBy('created_at','DESC')->where('value','like','%'.$request->search_key.'%')->get();
           if($Favourite->count() !=0){  
            //categories data 
             $company_data =array();
                   foreach ($Favourite as $key => $value) {                                    
                                        $value->company_data = getCompanyData($value->company_id);
                                        $company_data = $value ;
                                     } 
              $response['status'] = config('resource.status_200');             
              $response['data']   = $Favourite->toArray() ;            
              return response()->json($response,config('resource.status_200'));                    

            }else{                  
              //no categories
              $response['msg']    = config('resource.record_not_found');
              $response['status'] = config('resource.status_420');
              return response()->json($response,config('resource.status_200'));
            }

           }else{
               $response['msg'] = config('resource.parameter_missing');
              $response['status'] = config('resource.status_420');
              return response()->json($response,config('resource.status_200'));
           }
        }


}
