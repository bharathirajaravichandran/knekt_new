<?php

namespace App\Http\Middleware;

use Closure;
use Encore\Admin\Facades\Admin;

class LanguageCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Admin::user();
        if($user) 
           app()->setlocale($user->language);

        return $next($request);
    }
}
