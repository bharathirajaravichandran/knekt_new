<?php

return [
    'online'                => 'Online',
    'login'                 => 'Login',
    'logout'                => 'Logout',
    'setting'               => 'Setting',
    'name'                  => 'Name',
    'username'              => 'Username',
    'password'              => 'Password',
    'password_confirmation' => 'Password confirmation',
    'remember_me'           => 'Remember me',
    'user_setting'          => 'User setting',
    'avatar'                => 'Avatar',
    'list'                  => 'List',
    'new'                   => 'New',
    'create'                => 'Create',
    'delete'                => 'Delete',
    'remove'                => 'Remove',
    'edit'                  => 'Edit',
    'view'                  => 'View',
    'continue_editing'      => 'Continue editing',
    'continue_creating'     => 'Continue creating',
    'detail'                => 'Detail',
    'browse'                => 'Browse',
    'reset'                 => 'Reset',
    'export'                => 'Export',
    'batch_delete'          => 'Batch delete',
    'save'                  => 'Save',
    'refresh'               => 'Refresh',
    'order'                 => 'Order',
    'expand'                => 'Expand',
    'collapse'              => 'Collapse',
    'filter'                => 'Filter',
    'search'                => 'Search',
    'close'                 => 'Close',
    'show'                  => 'Show',
    'entries'               => 'entries',
    'captcha'               => 'Captcha',
    'action'                => 'Action',
    'title'                 => 'Title',
    'description'           => 'Description',
    'back'                  => 'Back',
    'back_to_list'          => 'Back to List',
    'submit'                => 'Submit',
    'menu'                  => 'Menu',
    'input'                 => 'Input',
    'succeeded'             => 'successfully',
    'failed'                => 'Failed',
    'delete_confirm'        => 'Are you sure you want to delete ?',
    'delete_succeeded'      => 'Deleted successfully !',
    'active_confirm'        => 'Are you sure you want to Activate these fields ?',
    'inactive_confirm'      => 'Are you sure you want to Inactivate these fields ?',
    'set_active'            => 'Set Active',
    'set_inactive'          => 'Set Inactive',
    'delete_failed'         => 'Delete failed !',
    'update_succeeded'      => 'Updated successfully !',
    'save_succeeded'        => 'Saved successfully !',
    'refresh_succeeded'     => 'Refreshed successfully !',
    'login_successful'      => 'Login successful',
    'choose'                => 'Choose',
    'choose_file'           => 'Select file',
    'choose_image'          => 'Select image',
    'more'                  => 'More',
    'deny'                  => 'Permission denied',
    'administrator'         => 'Administrator',
    'roles'                 => 'Roles',
    'permissions'           => 'Permissions',
    'slug'                  => 'Slug',
    'created_at'            => 'Created At',
    'updated_at'            => 'Updated At',
    'alert'                 => 'Alert',
    'parent_id'             => 'Parent',
    'icon'                  => 'Icon',
    'uri'                   => 'URI',
    'operation_log'         => 'Operation log',
    'parent_select_error'   => 'Parent select error',
    'language'              => 'Language',
    'member_since_admin'    => 'Member since admin',

    'pagination'            => [
        'range' => 'Showing :first to :last of :total entries',
    ],
    'role'                  => 'Role',
    'permission'            => 'Permission',
    'route'                 => 'Route',
    'confirm'               => 'Confirm',
    'cancel'                => 'Cancel',
    'http'                  => [
        'method' => 'HTTP method',
        'path'   => 'HTTP path',
    ],
    'all_methods_if_empty'  => 'All methods if empty',
    'all'                   => 'All',
    'current_page'          => 'Current page',
    'selected_rows'         => 'Selected rows',
    'upload'                => 'Upload',
    'new_folder'            => 'New folder',
    'time'                  => 'Time',
    'size'                  => 'Size',
    'listbox'               => [
        'text_total'         => 'Showing all {0}',
        'text_empty'         => 'Empty list',
        'filtered'           => '{0} / {1}',
        'filter_clear'       => 'Show all',
        'filter_placeholder' => 'Filter',
    ],
    'menu_titles' => [
        'admin'=>'Admin',
        'dashboard'=>'Dashboard',
        'users'=>'Users',
        'roles'=>'Roles',
        'permission'=>'Permission',
        'category'=>'Category',
        'company'=>'Company',
        'company_branch'=>'Company Branch',
        'ivr_details'=>'IVR Details',
        'menu'=> 'Menu',
        'operation_log'=> 'Operation log',
        'settings'=> 'Settings',
        'report'=> 'Report',
        'user_report'=> 'User Report',
        'company_report'=> 'Company Report',

    ],
    'form_menu'=>[
         'status'=>'Status',
         'submit'=>'Submit',
         'reset'=>'Reset',
         'list'=>'List',
         'add'=>'Add',
         'add_more'=>'Add More',
         'create'=>'Create',
         'edit'=>'Edit',
         'view'=>'View',
         'created_at'=>'Created at',
         'updated_at'=>'Updated at',

         'category'=>'Category',
         'subcategory'=>'Subcategory',
         'company_name'=>'Company Name',
         'company_description'=>'Company Description',
         'company_image'=>'Service Image',
         'email'=>'Email',
         'phone'=>'Phone',
         'alternate_phone'=>'Alternate Phone',
         'sms'=>'SMS',
         'whatsapp'=>'Whatsapp',
         'linkedin'=>'Linkedin',
         'website'=>'Website',
         'live_chart'=>'Live Chart',
         'notification'=>'Notification',
         'company_management'=>'Company Management',
         'opening_time'=>'Working Hours',
         'time'=>'Time',
         'open_time'=>'Open Time',
         'close_time'=>'Close Time',


         'parent_category'=>'Parent Category',
         'subcategory_list'=>'Subcategory List',
         'category_name'=>'Category Name',
         'category_description'=>'Category Description',
         'category_image'=>'Category Image',
         'category_management'=>'Category Management',
         'subcategory_name'=>'Sub Category Name',
         'subcategory_description'=>'Sub Category Description',
         'subcategory_image'=>'Sub Category Image',
         'subcategory_management'=>'Sub Category Management',

         'company'=>'Company',
         'address'=>'Address',
         'latitude'=>'Latitude',
         'longitude'=>'Longitude',
         'company_branch_details'=>'Company Branch Details',

         
         'language'=>'Language',
         'ivr_value'=>'IVR Value',
         'ivr_name'=>'IVR Name',
         'time_delay'=>'Time Delay',
         'sub_ivr'=>'Sub IVR',
         'import_ivr'=>'Import IVR',
         'ivr_details'       =>'IVR Details',
         'main_ivr'       =>'Main IVR',
         'sub_ivr_details'       =>'Sub IVR Details',
         'minutes'       =>'Minutes',
         'minutes'       =>'Minutes',
         'seconds'       =>'Seconds',
         'service_name'       =>'Service Name',
         'parent_option'=>'Parent Option',
         'add_sub_ivr'=>'Add Sub IVR',
         'sub_ivr_list'=>'Sub IVR List',
         'ivr_excel_upload'=>'IVR Excel Upload',
         'ivr_excel'=>'IVR Excel',

         'google_api_key'=>'Google API Key',
         'settings_management'=>'Settings Management',


         'name'=>'Name',
         'user_type'=>'User Type',
         'photo'=>'Photo',
         'password'=>'Password',
         'dob'=>'DOB',
         'gender'=>'Gender',
         'facebook_id'=>'Facebook id',
         'twitter_id'=>'Twitter id',
         'googleplus_id'=>'GooglePlus id',
         'users_management'=>'Users Management',

         'start_date'=>'Start Date',
         'end_date'=>'End Date',
         'types'=>'Types',
         'report'=> 'Report',
         'users__report'=>'Users Report',
         'company__report'=>'Companies Report',


    ],
   

];
