<section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="row">
            <div class="col-md-12">
               <div class="box box-info">
                  <div class="box-header with-border">
                     <h3 class="box-title">View</h3>
                     <div class="box-tools">
                        <div class="btn-group pull-right" style="margin-right: 5px">
                           <a href="/admin/users/{{$user->id}}/edit" class="btn btn-sm btn-primary" title="Edit">
                           <i class="fa fa-edit"></i><span class="hidden-xs"> Edit</span>
                           </a>
                        </div>
                        <div class="btn-group pull-right" style="margin-right: 5px">
                           <a href="/admin/users" class="btn btn-sm btn-default" title="List">
                           <i class="fa fa-list"></i><span class="hidden-xs"> List</span>
                           </a>
                        </div> 
                     </div>
                  </div>
                  <!-- /.box-header -->
                  <!-- form start -->
                  <div class="form-horizontal">
                     <div class="box-body" style="
                        border: 2px solid #00BFEC;
                        padding: 30px;
                        border-radius: 25px;
                        margin-top: 15px;
                        ">
                        <div class="fields-group">
                                                   
                                                   
                          
                         
                          
                     </div>
                     <!-- /.box-body -->
                  </div>
               </div>
            </div>
            <div class="col-md-12">
            </div>
         </div>
      </div>
   </div>
</section>