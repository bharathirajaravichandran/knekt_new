<?php 
$parent ='';
?>
<section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="row">
            <div class="col-md-12">
               <div class="box box-info">
                  <div class="box-header with-border">
                     <div class="box-tools">
                        <div class="btn-group pull-right" style="margin-right: 5px">
                           <a href="" class="btn btn-sm btn-primary" title="Edit">
                           <i class="fa fa-edit"></i><span class="hidden-xs"> Edit</span>
                           </a>
                        </div>
                        <div class="btn-group pull-right" style="margin-right: 5px">
                           <a href="" class="btn btn-sm btn-default" title="List">
                           <i class="fa fa-list"></i><span class="hidden-xs"> List</span>
                           </a>
                        </div> 
                     </div>
                  </div>
                  <!-- /.box-header -->
                  <!-- form start -->
                  <div class="form-horizontal">
                     <div class="box-body" style="
                        border: 2px solid #00BFEC;
                        padding: 30px;
                        border-radius: 25px;
                        margin-top: 15px;
                        ">
                        <div class="fields-group">  
                        
                        @if(count($data) !=0)
                              <!-- language -->                                               
                                 @if($cat != 0)
                                          
                                                     
                                                                
                                                     

                  <div class="col-lg-4 col-12 m-t-35">
                    <div class="card">
                   <table> <tr><th> Total Number of Companies = {{ count($data) }} </th></tr></table>
                    @foreach($final_array as $key=>$fin)  
                      <table>  
                        <tr>
                           <!-- <th> Number of Users = {{ count($fin) }} </th> -->
                           <tr><th>  Categories :  <?php
                            $parent = DB::table('tbl_category')->where('id',$key)->first();
                            echo @$parent->category_name;
                             ?> ({{ count($fin) }})  
                            </th> </tr>
                         </tr>
                       </table>
                    @endforeach

                            
                                                     <div class="card-body">
                                                     <table class="table display nowrap">
                                                          <thead>
                                                              <tr>
                                                                 
                                                                  <th>Company Name</th>
                                                                   <th>Parent Category</th>
                                                                  <th>Phone</th>
                                                                
                                                              </tr>
                                                          </thead>
                                                          <tbody>
                                      @foreach($final_array as $key=>$final)  
                                                   @foreach($final as $keys=>$finals)
                                                              <tr>
                                                               
                                                                 <td>{{ $finals->service_name }} </td>
                                                                  <td>
                                                                 <?php
                                                                  
                                                                  $parent = DB::table('tbl_category')->where('id',$finals->parent_category_id)->first();
                                                                  echo @$parent->category_name;
                                                                  ?>
                                                                  
                                                                </td> 
                                                                 <td>{{ $finals->phone }} </td> 
                                                                
                                                                
                                                              </tr>
                                                    @endforeach
                                       @endforeach

                                                          </tbody>
                                                      </table>
                                                  </div>
                           

                    </div>

                </div>

                                      
                                 @endif
                              <!-- language -->  


                              <!-- country -->  
                                 @if($coun != 0)
                                        country export
                                 @endif  
                              <!-- country -->  

                               <!-- gender -->  
                                 @if($gend != 0)
                                        <div class="col-lg-4 col-12 m-t-35">
                    <div class="card">
                   <table> <tr><th> Total Number of Companies = {{ count($data) }} </th></tr></table>
                    @foreach($final_array as $key=>$fin)  
                      <table>  
                        <tr>
                           <!-- <th> Number of Users = {{ count($fin) }} </th> -->
                           <tr><th>  Status : @if($key == 1)  Male ({{ count($fin) }})  @else Female ({{ count($fin) }}) @endif 
                            </th> </tr>
                         </tr>
                       </table>
                    @endforeach

                            
                                                     <div class="card-body">
                                                     <table class="table display nowrap">
                                                          <thead>
                                                              <tr>
                                                                  <th>Name</th>
                                                               
                                                                  <th>Phone</th>
                                                               
                                                              </tr>
                                                          </thead>
                                                          <tbody>
                                      @foreach($final_array as $key=>$final)  
                                                   @foreach($final as $keys=>$finals)
                                                              <tr>
                                                                 <td>{{ $finals->service_name }} </td>          
                                                                 <td>{{ $finals->phone }} </td> 
                                                                 <td>@if($finals->gender==1) Male @else Female @endif</td> 
                                                              </tr>
                                                    @endforeach
                                       @endforeach

                                                          </tbody>
                                                      </table>
                                                  </div>
                           

                    </div>

                </div>
                                 @endif                    
                               <!-- gender -->

                              <!-- state -->  
                                 @if($stat != 0)
                                        <div class="col-lg-4 col-12 m-t-35">
                    <div class="card">
                   <table> <tr><th> Total Number of Companies = {{ count($data) }} </th></tr></table>
                    @foreach($final_array as $key=>$fin)  
                      <table>  
                        <tr>
                           <!-- <th> Number of Users = {{ count($fin) }} </th> -->
                           <tr><th>  Status : @if($key == 1)  Active ({{ count($fin) }})  @else Inactive ({{ count($fin) }}) @endif 
                            </th> </tr>
                         </tr>
                       </table>
                    @endforeach

                            
                                                     <div class="card-body">
                                                     <table class="table display nowrap">
                                                          <thead>
                                                              <tr>
                                                                  <th>Name</th>
                                                                  <th>Phone</th>
                                                                  <th>Status</th>
                                                              </tr>
                                                          </thead>
                                                          <tbody>
                                      @foreach($final_array as $key=>$final)  
                                                   @foreach($final as $keys=>$finals)
                                                              <tr>
                                                                 <td>{{ $finals->service_name }} </td>
                                                                 <td>{{ $finals->phone }} </td> 
                                                                 <td>@if($finals->status==1) Active @else Inactive @endif</td> 
                                                              </tr>
                                                    @endforeach
                                       @endforeach

                                                          </tbody>
                                                      </table>
                                                  </div>
                           

                    </div>

                </div>

                                 @endif
                              <!-- state -->         
                        @else
                             <h5>No data here</h5>   
                                <div class="col-md-2">
                                    <div class="btn-group pull-right">
                                       <a href="{{ url('/')}}/admin/user_report"> <button type="button" class="btn btn-primary">Back</button> </a>
                                    </div>   
                                </div>
                        @endif       
                          
                         
                          
                     </div>
                     <!-- /.box-body -->
                  </div>
               </div>
            </div>
            <div class="col-md-12">
            </div>
         </div>
      </div>
   </div>
</section>