<section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="row">
            <div class="col-md-12">
               <div class="box box-info">
                  <div class="box-header with-border">
                     <h3 class="box-title">{{ trans('admin.form_menu.view') }}</h3>
                     <div class="box-tools">
                        <div class="btn-group pull-right" style="margin-right: 5px">
                           <a href="/admin/users/{{$user->id}}/edit" class="btn btn-sm btn-primary" title="Edit">
                           <i class="fa fa-edit"></i><span class="hidden-xs"> {{ trans('admin.form_menu.edit') }}</span>
                           </a>
                        </div>
                        <div class="btn-group pull-right" style="margin-right: 5px">
                           <a href="/admin/users" class="btn btn-sm btn-default" title="List">
                           <i class="fa fa-list"></i><span class="hidden-xs"> {{ trans('admin.form_menu.list') }}</span>
                           </a>
                        </div> 
                     </div>
                  </div>
                  <!-- /.box-header -->
                  <!-- form start -->
                  <div class="form-horizontal">
                     <div class="box-body" style="
                        border: 2px solid #00BFEC;
                        padding: 30px;
                        border-radius: 25px;
                        margin-top: 15px;
                        ">
                        <div class="fields-group">
                            <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.name') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                      {{$user->name}}
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.photo') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       <?php if($user->photo!="") { ?>
                                          <img src="{{ url('/') }}/uploads/Users/{{$user->photo}}" width="250px" height="250px">
                                      <?php } else {
                                          echo "No Image";
                                      }
                                       ?>
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div> 
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.email') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       {{$user->email}}
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.phone') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       {{$user->phone}}
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.gender') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <?php
                                       if($user->gender=="1")
                                       echo "Male";
                                       else
                                       echo "Female";
                                       ?>
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div> 
                            <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.dob') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       {{$user->dob}}
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.language') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                          <?php
                                       if($user->language=="en")
                                       echo "English";
                                       else if($user->language=="ar")
                                       echo "Arabic";
                                       else
                                       echo "Not Defined";
                                       ?>
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div> 
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.facebook_id') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       <?php
                                       if($user->facebook_id!="")
                                       echo $user->facebook_id;
                                       else
                                       echo "-----------------";
                                       ?>
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.twitter_id') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       <?php
                                       if($user->twitter_id!="")
                                       echo $user->twitter_id;
                                       else
                                       echo "-----------------";
                                       ?>
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div> 
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.googleplus_id') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       <?php
                                       if($user->googleplus_id!="")
                                       echo $user->googleplus_id;
                                       else
                                       echo "-----------------";
                                       ?>
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div> 
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.user_type') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        Web
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div> 
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.created_at') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       <?php echo date_format($user->created_at,"M d, Y H:m:s");?>
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.updated_at') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       <?php 
                                       if($user->updated_at!=null)
                                       echo date_format($user->updated_at,"M d, Y H:m:s");
                                       else
                                       echo "-------------";
                                       ?>
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.status') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                      {{($user->status == 1) ? "Active" : "InActive"}}
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- /.box-body -->
                  </div>
               </div>
            </div>
            <div class="col-md-12">
            </div>
         </div>
      </div>
   </div>
</section>