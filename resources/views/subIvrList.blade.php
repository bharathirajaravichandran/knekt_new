<section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="row">
            <div class="col-md-12">
               <div class="box box-info">
                  <div class="box-header with-border">
                     <h3 class="box-title">View</h3>
                     <div class="box-tools">                       
                        <div class="btn-group pull-right" style="margin-right: 5px">
                           <a href="/admin/add_subivr?company_id={{$_GET['company']}}&id={{$_GET['preid']}}&lang={{$_GET['lang']}}" class="btn btn-sm btn-default" title="List">
                           <i class="fa fa-list"></i><span class="hidden-xs"> List</span>
                           </a>
                        </div>
                     </div>
                  </div>
                  <!-- /.box-header -->
                  <!-- form start -->
                  <div class="form-horizontal">
                     <div class="box-body" style="
                        border: 2px solid #00BFEC;
                        padding: 30px;
                        border-radius: 25px;
                        margin-top: 15px;
                        ">
                        <div class="fields-group">
                           <div class="form-group ">
                              <label class="col-sm-3 control-label"></label>
                              <div class="col-sm-8">
                                 <div class="row">
                                    <p style="font-size: 20px;">
                                         {{$language}} : {{$language_value}}
                                    
                                    @foreach($data as $k => $v)
                                    
                                        => {{$v}} : {{$option_value[$k]}}
                                        @endforeach
                                    </p>
                                   
                                 </div>
                              </div>
                           </div>
                           </div>
                        </div>
                     </div>
                     <!-- /.box-body -->
                  </div>
               </div>
            </div>
            <div class="col-md-12">
            </div>
         </div>
      </div>
   </div>
</section>