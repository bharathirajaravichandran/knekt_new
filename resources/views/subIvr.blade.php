@php
    $preid = $_GET['preid'];
    if(Request::segment(4)=='create'){
        $cat = DB::table('tbl_catoptions')->where('parent_option_id',Request::segment(3))->get();  
        $parent_id =Request::segment(3);  
    }else{
        $parent = DB::table('tbl_catoptions')->where('id',Request::segment(3))->first();
        $cat = DB::table('tbl_catoptions')->where('parent_option_id',$parent->parent_option_id)->get(); 
        $parent_id =$parent->parent_option_id;
    }
    $url=Request::segment(2);
    $subcat =array();
    foreach ($cat as $key => $value) {
        $subcat[] = $value->option_name.' = '.$value->option_value;
    }
    if(count($subcat)!=0){
        $subcat = $subcat;
    }
    $subcats =implode(',', $subcat);
@endphp
<section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="box box-info">
            <div class="box-header with-border">
               <h3 class="box-title">{{ trans('admin.form_menu.sub_ivr') }}</h3>
                <div class="box-tools">
                  <div class="btn-group pull-right" style="margin-right: 5px">
                      <a href="/admin/add_subivr?company_id={{$_GET['company']}}&id={{$_GET['preid']}}&lang={{$_GET['lang']}}" class="btn btn-sm btn-default" title="List"><i class="fa fa-list"></i><span class="hidden-xs">&nbsp;{{ trans('admin.form_menu.list') }}</span></a>
                  </div>
                  </div>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{ url('/') }}/admin/add_subivr/saveprd" method="POST" accept-charset="UTF-8" class="form-horizontal sub_ivr_form" pjax-container id="sub_ivr_form">
               <div class="box-body">
                  <div class="fields-group">
                     <div class="form-group  ">
                        <label for="option_name" class="col-sm-2  control-label">{{ trans('admin.form_menu.sub_ivr') }}</label>
                        <div class="col-sm-8">
                           <span id="ivrdetails">{{$subcats}}</span>
                        </div>
                     </div>
                     <div class="form-group  ">
                        <label for="option_name" class="col-sm-2  control-label">{{ trans('admin.form_menu.service_name') }}</label>
                        <div class="col-sm-8 option_name_error">
                           <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                              <input type="text" id="option_name" name="option_name" value="{{(isset($old_values->option_name)) ? $old_values->option_name: '' }}" class="form-control option_name" placeholder="Input Service Name" />
                               <input type="hidden" name="optionnameError" id="optionnameError" value="">
                           </div>
                        </div>
                     </div>
                     <div class="form-group  ">
                        <label for="option_value" class="col-sm-2  control-label">{{ trans('admin.form_menu.ivr_value') }}</label>
                        <div class="col-sm-8 option_value_error">
                           <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                              <input type="text" id="option_value" name="option_value" value="{{(isset($old_values->option_value)) ? $old_values->option_value: '' }}" class="form-control option_value" placeholder="Input IVR Value" />
                               <input type="hidden" name="optionError" id="optionError" value="">
                           </div>
                        </div>
                     </div>
                     <input type="hidden" name="company_id" value="" class="company_id" />
                     <input type="hidden" name="parent_option_id" class="parent_option_id" value="{{$parent_id}}"/>
                     <input type="hidden" name="oid" value="{{$pid}}" class="oid"  />
                     <input type="hidden" name="url" value="{{$url}}" class="oid"  />
                     <input type="hidden" name="preid" value="{{$preid}}" class="oid"  />
                      <input type="hidden" id="submit_type" name="submit_type" value="" class=""/>
                     <div class="form-group  ">
                        <label for="time_delay" class="col-sm-2  control-label">{{ trans('admin.form_menu.time_delay') }}</label>
                        <div class="col-sm-8 time_delay_error">
                           <div>
                              <span id="minutes1">{{ trans('admin.form_menu.minutes') }} : </span>
                              <input type="number" name="minutes" id="minutes" style="width:50px" min="0" max="59" value="{{(isset($time_value[1])) ? $time_value[1]: 00 }}">
                              <span id="seconds1" style="padding-left: 10px"> {{ trans('admin.form_menu.seconds') }} : </span>
                              <input type="number" name="seconds" id="seconds" style="width:50px" min="0" max="59" value="{{(isset($time_value[2])) ? $time_value[2]: 00 }}">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- /.box-body -->
               <div class="box-footer">
                  <input type="hidden" name="_token" id="_token" class="_token" value="{{csrf_token()}}">
                  <div class="col-md-2">
                  </div>
                  <div class="col-md-8">
                      <div class="btn-group pull-right">
                        <button type="button" class="btn btn-primary" id="add" onclick="return validation($(this));">{{ trans('admin.form_menu.add') }}</button>
                     </div>
                      <div class="btn-group pull-right">
                        <button type="button" class="btn btn-primary" id="addmore" onclick="return validation($(this));">{{ trans('admin.form_menu.add_more') }}</button>
                     </div>
                     <div class="btn-group pull-left">
                        <button type="reset" class="btn btn-warning">{{ trans('admin.form_menu.reset') }}</button>
                     </div>
                  </div>
               </div>
               <!-- /.box-footer -->
            </form>
         </div>
      </div>
   </div>
</section>
<script type = "text/javascript" >
   function validation(submittype) {
       if(submittype.attr('id') == 'add'){
           $('#submit_type').val(submittype.attr('id'));
        }else{ 
           $('#submit_type').val(submittype.attr('id'));
        } 
       var option_name = $('.option_name').val();
       var option_value = $('.option_value').val();
       var minutes = $('#minutes').val();
       var seconds = $('#seconds').val();
   
       var err = '';
       if (option_name == '') {
           // $('#language').parent('.col-sm-8').parent('.form-group').addClass('has-error');
           $('.languageError1').remove();
           $('.option_name_error').prepend('<label class="control-label languageError1" for="inputError" style="color:red;"><i class="fa fa-times-circle-o"></i> The Language field is required.</label>');
           $('#language').focus();
           err = 'set';
       } else {
   
       }
       if (option_value == '') {
           // $('#option_value').parent('.col-sm-8').parent('.form-group').addClass('has-error');
           $('.option_valueError1').remove();
           $('.option_value_error').prepend('<label class="control-label option_valueError1" for="inputError" style="color:red;"><i class="fa fa-times-circle-o"></i> The IVR Value field is required.</label>');
           $('#option_value').focus();
           err = 'set';
   
       }
   
        if(minutes!=''){
      
       if(minutes>=60){
           var email_error ='<label class="control-label time_delay_error2" for="inputError" style="color:red;"><i class="fa fa-times-circle-o"></i> Less than One Hour Only.</label>';
            $('.time_delay_error2').remove(); 
            $('.time_delay_error3').remove(); 
            $('.time_delay_error').prepend(email_error);
           err='set';
       }
       if(minutes.length>=3){
            var email_error ='<label class="control-label time_delay_error2" for="inputError" style="color:red;"><i class="fa fa-times-circle-o"></i>Allow 2 digits Only.</label>';
            $('.time_delay_error2').remove(); 
            $('.time_delay_error3').remove(); 
            $('.time_delay_error').prepend(email_error);
           err='set';
       }
   }
    
   if(seconds!=''){
      
       if(seconds>=60){
           var email_error ='<label class="control-label time_delay_error3" for="inputError" style="color:red;"><i class="fa fa-times-circle-o"></i> Less than One Hour Only.</label>';
            $('.time_delay_error3').remove(); 
            $('.time_delay_error2').remove(); 
            $('.time_delay_error').prepend(email_error);
            err='set';
       }
       if(seconds.length>=3){
            var email_error ='<label class="control-label time_delay_error3" for="inputError" style="color:red;"><i class="fa fa-times-circle-o"></i> Allow 2 digits Only.</label>';
            $('.time_delay_error3').remove(); 
            $('.time_delay_error2').remove(); 
            $('.time_delay_error').prepend(email_error);
            err='set';
       }
   }
      
   
       if (err != '') {
           return false;
       } else {
           $('#sub_ivr_form').submit();
       }
   }
   $(document).ready(function() {
   //IVR Value already exists chevck START
    var _token = $('._token').val();
   $('#option_value').on('blur', function() {
       var fullurl = window.location.pathname.split('/');
       var parent_id = $('.parent_option_id').val();
       var uri_segment = fullurl[4];
       var old_id = fullurl[3];
       var option_value = $('#option_value').val();
       if (option_value != '') {
         var value = /^\d+$/;
           if (!value.test(option_value)) {
               var email_error = '<label class="control-label option_valueError2" for="inputError" style="color:red;"><i class="fa fa-times-circle-o"></i> The IVR Value field is Numeric only.</label>';
               $('.option_valueError1').remove();
               $('.option_valueError2').remove();
               $('.option_valueError3').remove();
               $('.option_value_error').prepend(email_error);
               $('#add').prop('disabled', true);
               $('#addmore').prop('disabled', true);
           }else{

              $.ajax({
                 type: 'POST',
                 url: '/admin/get_sub_ivr_value',
                 data: {
                     parent_id: parent_id,
                     option_value: option_value,
                     uri_segment: uri_segment,
                     old_id: old_id,
                     _token: _token,
                 },
                 success: function(result) {
     
                     if (result == 1) {
                         var email_error = '<label class="control-label option_valueError3" for="inputError" style="color:red;"><i class="fa fa-times-circle-o"></i>The IVR Value field is already taken.</label>';
                         $('.option_valueError1').remove();
                         $('.option_valueError2').remove();
                         $('.option_valueError3').remove();
                         $('.option_value_error').prepend(email_error);
                         $('#optionError').val('error');
                          $('#add').attr('disabled', 'disabled');
                          $('#addmore').attr('disabled', 'disabled');
                     } else {
                         $('.option_valueError1').remove();
                         $('.option_valueError2').remove();
                         $('.option_valueError3').remove();
                         $('#optionError').val('error');
                        if($('#optionnameError').val()!='error'){
                        $('#add').removeAttr("disabled");      
                        $('#addmore').removeAttr("disabled");      
                     }
                     }
                 }
             });
           }
           
       }
   
   });
   
   //IVR Value already exists check END
   //Service Name already exists chevck START
   
   $('#option_name').on('blur', function() {
       var fullurl = window.location.pathname.split('/');
       $('#small2').remove();
       $('#small3').remove();
       var parent_id = $('.parent_option_id').val();
       var uri_segment = fullurl[4];
       var old_id = fullurl[3];
       var option_name = $('#option_name').val();
       if (option_value != '') {
           $.ajax({
               type: 'POST',
               url: '/admin/get_sub_service_name',
               data: {
                   parent_id: parent_id,
                   option_name: option_name,
                   uri_segment: uri_segment,
                   old_id: old_id,
                   _token: _token,
               },
               success: function(result) {
   
                   if (result == 1) {
                       $('.languageError1').remove();
                       var email_error = '<label class="control-label languageError2" for="inputError" style="color:red;"><i class="fa fa-times-circle-o"></i> The Service Name is already taken.</label>';
                       $('.languageError2').remove();
                       $('.option_name_error').prepend(email_error);
                       $('#optionnameError').val('error');
                      $('#add').attr('disabled', 'disabled');
                      $('#addmore').attr('disabled', 'disabled');
   
                   } else {
                       $('.languageError2').remove();
                       $('#optionnameError').val('');
                        if($('#optionError').val()!='error'){
                       $('#add').removeAttr("disabled");      
                        $('#addmore').removeAttr("disabled");      
                      }
                   }
               }
           });
       }
   
   });
   
   //Service Name already exists check END
   }); 
</script>