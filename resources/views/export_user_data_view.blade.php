<section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="row">
            <div class="col-md-12">
               <div class="box box-info">
                  <div class="box-header with-border">
                     <h3 class="box-title">View</h3>
                     <div class="box-tools">
                        <div class="btn-group pull-right" style="margin-right: 5px">
                           <a href="" class="btn btn-sm btn-primary" title="Edit">
                           <i class="fa fa-edit"></i><span class="hidden-xs"> Edit</span>
                           </a>
                        </div>
                        <div class="btn-group pull-right" style="margin-right: 5px">
                           <a href="" class="btn btn-sm btn-default" title="List">
                           <i class="fa fa-list"></i><span class="hidden-xs"> List</span>
                           </a>
                        </div> 
                     </div>
                  </div>
                  <!-- /.box-header -->
                  <!-- form start -->
                  <div class="form-horizontal">
                     <div class="box-body" style="
                        border: 2px solid #00BFEC;
                        padding: 30px;
                        border-radius: 25px;
                        margin-top: 15px;
                        ">
                        <div class="fields-group">  
                        
                        @if(count($data) !=0)
                              <!-- language -->                                               
                                 @if($lang != 0)
                                          
                                                     
                                                                
                                                     

                  <div class="col-lg-4 col-12 m-t-35">
                    <div class="card">
                   <table> <tr><th> Total Number of Users = {{ count($data) }} </th></tr></table>
                    @foreach($final_array as $key=>$fin)  
                      <table>  
                        <tr>
                           <!-- <th> Number of Users = {{ count($fin) }} </th> -->
                           <tr><th>  Language: @if($key == 'ar')  Arabic ({{ count($fin) }})  @else ENGLISH ({{ count($fin) }}) @endif 
                            </th> </tr>
                         </tr>
                       </table>
                    @endforeach

                            
                                                     <div class="card-body">
                                                     <table class="table display nowrap">
                                                          <thead>
                                                              <tr>
                                                                  <th>Name</th>
                                                                  <th>Email</th>
                                                                  <th>Phone</th>
                                                                  <th>Language</th>
                                                              </tr>
                                                          </thead>
                                                          <tbody>
                                      @foreach($final_array as $key=>$final)  
                                                   @foreach($final as $keys=>$finals)
                                                              <tr>
                                                                 <td>{{ $finals->name }} </td>
                                                                 <td>{{ $finals->email }} </td> 
                                                                 <td>{{ $finals->phone }} </td> 
                                                                 <td>{{ $finals->language }} </td> 
                                                              </tr>
                                                    @endforeach
                                       @endforeach

                                                          </tbody>
                                                      </table>
                                                  </div>
                           

                    </div>

                </div>

                                      
                                 @endif
                              <!-- language -->  


                              <!-- country -->  
                                 @if($coun != 0)
                                        country export
                                 @endif  
                              <!-- country -->  

                               <!-- gender -->  
                                 @if($gend != 0)
                                        <div class="col-lg-4 col-12 m-t-35">
                    <div class="card">
                   <table> <tr><th> Total Number of Users = {{ count($data) }} </th></tr></table>
                    @foreach($final_array as $key=>$fin)  
                      <table>  
                        <tr>
                           <!-- <th> Number of Users = {{ count($fin) }} </th> -->
                           <tr><th>  Gender: @if($key == 1)  Male ({{ count($fin) }})  @else Female ({{ count($fin) }}) @endif 
                            </th> </tr>
                         </tr>
                       </table>
                    @endforeach

                            
                                                     <div class="card-body">
                                                     <table class="table display nowrap">
                                                          <thead>
                                                              <tr>
                                                                  <th>Name</th>
                                                                  <th>Email</th>
                                                                  <th>Phone</th>
                                                                  <th>Gender</th>
                                                              </tr>
                                                          </thead>
                                                          <tbody>
                                      @foreach($final_array as $key=>$final)  
                                                   @foreach($final as $keys=>$finals)
                                                              <tr>
                                                                 <td>{{ $finals->name }} </td>
                                                                 <td>{{ $finals->email }} </td> 
                                                                 <td>{{ $finals->phone }} </td> 
                                                                 <td>@if($finals->gender==1) Male @else Female @endif</td> 
                                                              </tr>
                                                    @endforeach
                                       @endforeach

                                                          </tbody>
                                                      </table>
                                                  </div>
                           

                    </div>

                </div>
                                 @endif                    
                               <!-- gender -->

                              <!-- state -->  
                                 @if($stat != 0)
                                        <div class="col-lg-4 col-12 m-t-35">
                    <div class="card">
                   <table> <tr><th> Total Number of Users = {{ count($data) }} </th></tr></table>
                    @foreach($final_array as $key=>$fin)  
                      <table>  
                        <tr>
                           <!-- <th> Number of Users = {{ count($fin) }} </th> -->
                           <tr><th>  Status: @if($key == 1)  Active ({{ count($fin) }})  @else Inactive ({{ count($fin) }}) @endif 
                            </th> </tr>
                         </tr>
                       </table>
                    @endforeach

                            
                                                     <div class="card-body">
                                                     <table class="table display nowrap">
                                                          <thead>
                                                              <tr>
                                                                  <th>Name</th>
                                                                  <th>Email</th>
                                                                  <th>Phone</th>
                                                                  <th>Status</th>
                                                              </tr>
                                                          </thead>
                                                          <tbody>
                                      @foreach($final_array as $key=>$final)  
                                                   @foreach($final as $keys=>$finals)
                                                              <tr>
                                                                 <td>{{ $finals->name }} </td>
                                                                 <td>{{ $finals->email }} </td> 
                                                                 <td>{{ $finals->phone }} </td> 
                                                                 <td>@if($finals->status==1) Active @else Inactive @endif</td> 
                                                              </tr>
                                                    @endforeach
                                       @endforeach

                                                          </tbody>
                                                      </table>
                                                  </div>
                           

                    </div>

                </div>

                                 @endif
                              <!-- state -->         
                        @else
                             <h5>No data here</h5>   
                                <div class="col-md-2">
                                    <div class="btn-group pull-right">
                                       <a href="{{ url('/')}}/admin/user_report"> <button type="button" class="btn btn-primary">Back</button> </a>
                                    </div>   
                                </div>
                        @endif       
                          
                         
                          
                     </div>
                     <!-- /.box-body -->
                  </div>
               </div>
            </div>
            <div class="col-md-12">
            </div>
         </div>
      </div>
   </div>
</section>