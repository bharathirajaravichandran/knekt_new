<section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="row">
            <div class="col-md-12">
               <div class="box box-info">
                  <div class="box-header with-border">
                     <h3 class="box-title">{{ trans('admin.form_menu.view') }}</h3>
                     <div class="box-tools">                       
                        <div class="btn-group pull-right" style="margin-right: 5px">
                           <a href="/admin/ivr_details" class="btn btn-sm btn-default" title="List">
                           <i class="fa fa-list"></i><span class="hidden-xs"> {{ trans('admin.form_menu.list') }}</span>
                           </a>
                        </div>
                     </div>
                  </div>
                  <!-- /.box-header -->
                  <!-- form start -->
                  <div class="form-horizontal">
                     <div class="box-body" style="
                        border: 2px solid #00BFEC;
                        padding: 30px;
                        border-radius: 25px;
                        margin-top: 15px;
                        ">
                        <div class="fields-group">
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.company') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       {{$company->service_name}}
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.language') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        {{$ivr_value->option_name}}
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div> 
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.ivr_value') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                         {{$ivr_value->option_value}}
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div> 
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.phone') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       {{$company->phone}}
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div> 
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.alternate_phone') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       <?php
                                       if($company->alternatephone!="")
                                       echo $company->alternatephone;
                                       else
                                       echo "-----------------";
                                       ?>
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.time_delay') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        {{$time_delay}}
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div> 

                           </div>
                        </div>
                     </div>
                     <!-- /.box-body -->
                  </div>
               </div>
            </div>
            <div class="col-md-12">
            </div>
         </div>
      </div>
   </div>
</section>