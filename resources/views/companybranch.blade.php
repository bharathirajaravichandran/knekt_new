
<link rel="stylesheet" href="{{url('/')}}/css/intlTelInput.css">
<link rel="stylesheet" href="{{url('/')}}/css/intlTelInput.min.css">
<script src="{{url('/')}}/js/intlTelInput.js" ></script>
<script src="{{url('/')}}/js/utils.js" ></script>
<script src="{{url('/')}}/js/intlTelInput.min.js" ></script>
<script src="{{url('/')}}/js/intlTelInput-jquery.js" ></script>
<script src="{{url('/')}}/js/intlTelInput-jquery.min.js" ></script>
<style type="text/css">
.kv-file-remove,.kv-file-upload, .fileinput-remove-button {display:none;}
.Err, .Err1 { display:none; color:red!important; }
.reqform {color:red !important;}
.intl-tel-input .country-list {z-index: 99999 !important;}
</style>
<?php
if($pid!="" || $pid!=0)
{
     $phonecode="kw";
    if($companybranch->phone_country_code!="")
    {
        $phonecountrycode = explode("-",$companybranch->phone_country_code);
        if(count($phonecountrycode)==2)
        $phonecode=$phonecountrycode[1];
        else
        $phonecode="kw";
    }
}
else
{
 $phonecode="kw";
}
?>

<style type="text/css">
.Err, .Err1 { display:none; color:red!important; }
</style>
<section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="box box-info">
            <div class="box-header with-border">
               <h3 class="box-title"><?php if($pid!="") echo "Edit"; else echo "Create";?></h3>
               <div class="box-tools">
                  <div class="btn-group pull-right" style="margin-right: 5px">
                     <a href="/admin/companybranch" class="btn btn-sm btn-default" title="List"><i class="fa fa-list"></i><span class="hidden-xs">&nbsp;{{ trans('admin.form_menu.list') }}</span></a>
                  </div>
               </div>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{ url('/')}}/admin/savecompanybranch" method="post" accept-charset="UTF-8" class="form-horizontal companybranchform" pjax-container="" id="companybranchform">
               <div class="box-body">
                  <div class="fields-group">
                     <div class="form-group  ">
                        <label for="company_id" class="col-sm-2  control-label">{{ trans('admin.form_menu.company') }}</label>
                        <div class="col-sm-8" id="company_id_inner">
                            <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Company field is required.</label> 
                            <select class="form-control status" style="width: 100%;" name="company_id" class="company_id" id="company_id">
                                <option value="">Please select Company</option>
                                     @foreach ($company as $k=>$v)
                                    <option value="{{ $k }}" {{ (isset($companybranch->company_id) && $companybranch->company_id==$k) ? 'selected': '' }}>{{ $v }}</option>
                                    @endforeach
                            </select>
                        </div>
                     </div>
                    <div class="form-group  ">
                        <label for="phone" class="col-sm-2  control-label">{{ trans('admin.form_menu.phone') }} <span class="reqform"></span></label>
                        <div class="col-sm-8" id="phone_inner">
                            <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Phone field is invalid.</label> 
                           <div class="input-group" id="phncde">
                              <span class="input-group-addon"><i class="fa fa-phone fa-fw"></i></span>
                              <input type="tel" id="phone" name="phone" value="{{(isset($companybranch->phone))?$companybranch->phone:''}}" class="form-control phone" placeholder="Input Phone" maxlength="15">
                           </div>
                        </div>
                     </div>
                     <div class="form-group  ">
                        <label for="address" class="col-sm-2  control-label">{{ trans('admin.form_menu.address') }}</label>
                        <div class="col-sm-8" id="address_inner">
                            <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i>The Address field is required</label>
                            <label class="control-label Err1" for="inputError"><i class="fa fa-times-circle-o"></i>The Address field is already Exist</label>
                           <textarea name="address" id="address" class="form-control address" rows="5" placeholder="Input Address">{{(isset($companybranch->address))?$companybranch->address:''}}</textarea>
                        </div>
                     </div>
                     <div class="form-group  ">
                        <label for="status" class="col-sm-2  control-label">{{ trans('admin.form_menu.status') }}</label>
                         <div class="col-sm-2" id="status_inner">
                            <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i>The Status field is required</label>
                            <select class="form-control status" style="width: 100%;" name="status" id="status">
                                <option value="">Please Select</option>
                                <option value="0" selected>In Active</option>
                                <option value="1" {{ (isset($companybranch->status) && $companybranch->status==1) ? 'selected': '' }}>Active</option>
                            </select>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- /.box-body -->
               <div class="box-footer">
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <input type="hidden" name="pid" value="{{ $pid }}" id="parent_id">
                  <input type="hidden" name="phone_country_code" id="phone_country_code" class="phone_country_code" value="">
                  <div class="col-md-2">
                  </div>
                  <div class="col-md-8">
                     <div class="btn-group pull-right">
                        <button type="button" class="btn btn-primary" onclick="return validation();">{{ trans('admin.form_menu.submit') }}</button>
                     </div>
                  </div>
               </div>
               <!-- /.box-footer -->
            </form>
         </div>
      </div>
   </div>
</section>
<script data-exec-on-popstate>
   $(function () {  
      $( document ).ready(function() { 
        $(".status").select2({"allowClear":false,"placeholder":"Please select"}); 
        $(".company_id").select2({"allowClear":true,"placeholder":"Please select"});
         $("#phone").val($.trim($("#phone").val().split(" ").join("")));
        });
    });

    function validation()
    {
        var phone_country_code = $('#phncde .selected-dial-code').text();
        var getcountrycode = $('#phncde .selected-dial-code').prev().attr('class').split(' ');
        $('#phone_country_code').val(phone_country_code+"-"+getcountrycode[1]);

        var companyname = $("#company_id").val();
        var address = $("#address").val();
        var phonenumber=$('#phone').val();
        var err='';
        $('.Err').hide();
        $('.Err1').hide();
        if((companyname=='' || companyname==null))
        {
            $('#company_id').addClass('has-error');
            $('#company_id_inner .Err').show();
            if(err=='')
            {
                $('#company_id').focus();
                err='set';
            }
        }   
        if((address=='' || address==null))
        {
            $('#address').addClass('has-error');
            $('#address_inner .Err').show();
            if(err=='')
            {
                $('#address').focus();
                err='set';
            }
        }
        if((phonenumber=='' || phonenumber==null || isNaN(phonenumber)))
        {
            $('#phone').addClass('has-error');
            $('#phone_inner .Err').show();
            if(err=='')
            {
                $('#phone').focus();
                err='set';
            }
        } 
        if(err!='')
        {
            $(':input[type="button"]').prop('disabled', false);
            return false;
        }
        else
        {
            checkunique();
        }   
    }

    function checkunique() {
        $.ajax({
          type : "POST",
          url : "{{ url('/') }}/admin/companybranchcheck",
          data : $('#companybranchform').serialize(),
          beforeSend : function() {
          },
          success : function(data) { 
            if(data == "failed")
            {
                $('#address').addClass('has-error');
                $('#address_inner .Err1').show();
                $('#address').focus();
                return false;
            }
            else
                $('#companybranchform').submit();   
            },
        });
    }
     var phonecountry_code="{{$phonecode}}";
    $('#phone').intlTelInput({
        initialCountry : phonecountry_code, 
        separateDialCode: true,
        utilsScript:'http://knekt.com/js/utils.js'
    });
</script>