<style type="text/css">
.kv-file-remove,.kv-file-upload, .fileinput-remove-button {display:none;}
.Err, .Err1 { display:none; color:red!important; }
.reqform {color:red !important;}
</style>
<section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="box box-info">
            <div class="box-header with-border">
               <h3 class="box-title"><?php if($pid!="") echo "Edit"; else echo "Create";?></h3>
               <div class="box-tools">
                  <div class="btn-group pull-right" style="margin-right: 5px">
                     <a href="{{ url('/')}}/admin/subcategory" class="btn btn-sm btn-default" title="List"><i class="fa fa-list"></i><span class="hidden-xs">&nbsp;{{ trans('admin.form_menu.list') }}</span></a>
                  </div>
               </div>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{ url('/') }}/admin/savesubcategory" method="post" accept-charset="UTF-8" class="form-horizontal categoryform" enctype=" multipart /form-data" pjax-container="" id="categoryform">
               <div class="box-body">
                  <div class="fields-group">
                    <?php
                    if($pid=="" || ($pid!="" && isset($category->parent_category_id) && $category->parent_category_id!=0))
                        { ?>
                     <div class="form-group  ">
                        <label for="status" class="col-sm-2  control-label">{{ trans('admin.form_menu.category_name') }}<span class="reqform">*</span></label>
                            <div class="col-sm-8" id="parent_category_inner">
                                 <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Category field is required.</label> 
                                <select class="form-control status" style="width: 100%;" name="parent_category_id" class="parent_category_id" id="parent_category_id" >    
                                    <option value=0>Please Select Category</option>
                                     @foreach ($catname as $k=>$v)
                                    <option value="{{ $k }}" {{ (isset($category->parent_category_id) && $category->parent_category_id==$k) ? 'selected': '' }}>{{ $v }}</option>
                                    @endforeach
                                </select>
                            </div>
                      </div>
                    <div class="form-group" style="display:none;">
                        <label for="status" class="col-sm-2  control-label">{{ trans('admin.form_menu.subcategory_list') }}</label>
                        <div class="col-sm-8" id="subcatlist">
                        </div>
                    </div>
                    <?php } ?>
                     <div class="form-group  ">
                        <label for="category_name" class="col-sm-2  control-label">{{ trans('admin.form_menu.subcategory_name') }} <span class="reqform">*</span></label>
                        <div class="col-sm-8" id="category_name_inner">
                             <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Sub Category Name field is required.</label> 
                             <label class="control-label Err1" for="inputError"><i class="fa fa-times-circle-o"></i> The Category Name is already exist.</label>
                           <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                              <input type="text" id="category_name" name="category_name" value="{{(isset($category->category_name))?$category->category_name:''}}" class="form-control category_name" placeholder="Input Category name">
                           </div>
                        </div>
                     </div>
                     <div class="form-group  ">
                        <label for="category_description" class="col-sm-2  control-label">{{ trans('admin.form_menu.subcategory_description') }}<span class="reqform">*</span></label>
                        <div class="col-sm-8" id="category_description_inner">
                             <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Sub Category Description field is required.</label>
                           <textarea name="category_description" id="category_description" class="form-control category_description" rows="5" placeholder="Input Category description">{{(isset($category->category_description))?$category->category_description:''}}</textarea>
                        </div>
                     </div>
                     <div class="form-group  ">
                        <label for="category_image" class="col-sm-2  control-label">{{ trans('admin.form_menu.subcategory_image') }}<span class="reqform">*</span></label>
                         <div class="col-sm-8 err_imageinner" id="category_image_inner">
                           <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> Please Upload Valid Image.</label>
                           @if($pid!=0)
                           <input type="file" class="category_image" id="category_image" name="category_image" value="{{$category->category_image}}"   />   
                           @else
                           <input type="file" class="category_image" id="category_image" name="category_image" /> 
                           @endif
                        </div>
                     </div>
                     <div class="form-group  ">
                        <label for="status" class="col-sm-2  control-label">{{ trans('admin.form_menu.status') }} <span class="reqform">*</span></label>
                            <div class="col-sm-2" id="status_inner">
                                <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i>The Status field is required</label>
                                <select class="form-control status" style="width: 100%;" name="status" id="status">
                                    <option value="">Please Select</option>
                                    <option value="0" selected>In Active</option>
                                    <option value="1" {{ (isset($category->status) && $category->status==1) ? 'selected': '' }}>Active</option>
                                </select>
                            </div>
                      </div>
                    </div>
                   <!-- /.box-body -->
                    <div class="box-footer">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="pid" value="{{ $pid }}" id="parent_id">
                        <input type="hidden" name="deletecheck" id="deletecheck" class="deletecheck" value="0">
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-8">
                            <div class="btn-group pull-right">
                                <button type="button" class="btn btn-primary" onclick="return validation();">{{ trans('admin.form_menu.submit') }}</button>
                            </div>
                             <!-- <div class="btn-group pull-left">
                                <button type="reset" class="btn btn-warning">Reset</button>
                             </div> -->
                        </div>
                    </div>
                 </div>
               <!-- /.box-footer -->
            </form>
         </div>
      </div>
   </div>
</section>

<script data-exec-on-popstate>
   $(function () {  
      $( document ).ready(function() { $(".Status").select2({"allowClear":false,"placeholder":"Status"}); });
       
        @if(count($image_array)>0)

        $("input.category_image").fileinput({
          "initialPreview":[@foreach($image_array as $k=>$v)"{{ url('/') }}/uploads/Category/{{$v}}",@endforeach],
          "initialPreviewConfig":[@foreach($image_array as $k=>$v) {
            "caption":"{{$v}}",
            "url":"{{ url('/') }}/admin/deleteimage",
            "key":"{{$k}}"  
          },
          @endforeach],
          "overwriteInitial":true,
          "initialPreviewAsData":true,
          "browseLabel":"Browse",
          "showRemove":false,
          "showUpload":false,
         // "uploadUrl":"#" , 
          "initialPreviewDelete":true, 
          "allowedFileExtensions": ['jpeg', 'jpg', 'png', 'gif'],
          "deleteExtraData":{"category_image[multiple_image]":"_file_del_","_file_del_":"","_token":"{{ csrf_token() }}","_method":"PUT"},
          "deleteUrl":"#"
        });

        @else

        $("input.category_image").fileinput({
          "overwriteInitial":true,
          "initialPreviewAsData":true,
          "browseLabel":"Browse",
          "showRemove":true,
          "showUpload":false,
         // "uploadUrl":"#" , 
          "initialPreviewDelete":true, 
          "allowedFileExtensions": ['jpeg', 'jpg', 'png', 'gif'], 
          "deleteExtraData":{"category_image":"_file_del_","_file_del_":"","_token":"{{ csrf_token() }}","_method":"PUT"},
          "deleteUrl":"#"
        });

        @endif   
      $(".status").select2({"allowClear":true,"placeholder":"Please select status"});
      $(".parent_category_id").select2({"allowClear":true,"placeholder":"Please select"});
    });

    $('body').on('click', '.fileinput-remove', function(e) {
        $('#deletecheck').val(1);
        $('#category_image').val("");
    });



  function validation()
  {

    var parentcat=$('#parent_category_id').val();
    var catname=$('#category_name').val().trim();
    var catimage=$('#category_image').val();
    var catdesc=$('#category_description').val();
    var status=$('#status').val();
    var mainid=$('#parent_id').val();
    var delcheckstatus=$('#deletecheck').val();

    var err='';
    $('.Err').hide();
    $('.Err1').hide();

    if((parentcat==0 || parentcat==null))
    {
        $('#parent_category_id').addClass('has-error');
        $('#parent_category_inner .Err').show();
        if(err=='')
        {
            $('#parent_category_id').focus();
            err='set';
        }
    }    
    if((catname=='' || catname==null))
    {
        $('#category_name').addClass('has-error');
        $('#category_name_inner .Err').show();
        if(err=='')
        {
            $('#category_name').focus();
            err='set';
        }
    }
    if((catdesc=='' || catdesc==null))
    {
        $('#category_description').addClass('has-error');
        $('#category_description_inner .Err').show();
        if(err=='')
        {
            $('#category_description').focus();
            err='set';
        }
    }
    if (mainid=="" && (catimage=="" || catimage==null))
    {
        $('#category_image').addClass('has-error');
        $('#category_image_inner .Err').show();
        if(err=='')
        {
            $('#category_image').focus();
            err='set';
        }
    }
    if (mainid!="" && (catimage=="" && delcheckstatus==1))
    {
        $('#category_image').addClass('has-error');
        $('#category_image_inner .Err').show();
        if(err=='')
        {
            $('#category_image').focus();
            err='set';
        }
    }
    if ( $("#category_image_inner .file-upload-indicator .glyphicon").hasClass("glyphicon-exclamation-sign") ) 
    {
        $('#category_image').addClass('has-error');
        $('#category_image_inner .Err').show();
        if(err=='')
        {
            $('#category_image').focus();
            err='set';
        }
    }
    if((status=='' || status==null))
    {
        $('#status').addClass('has-error');
        $('#status_inner .Err').show();
        if(err=='')
        {
            $('#status').focus();
            err='set';
        }
    }
    if(err!='')
    {
        $(':input[type="button"]').prop('disabled', false);
        return false;
    }
    else
    {
        checkunique();
    }
  }


function checkunique() {
    $.ajax({
      type : "POST",
      url : "{{ url('/') }}/admin/categoryexist",
      data : $('#categoryform').serialize(),
      beforeSend : function() {
      },
      success : function(data) { 
        if(data == "failed")
        {
            $('#category_name').addClass('has-error');
            $('#category_name_inner .Err1').show();
            $('#category_name').focus();
            return false;
        }
        else
            $('#categoryform').submit();   
        },
    });
   
   }
/*
   onchange="subcatlist(this.value);"
   select*/
   function subcatlist()
   {
     $maincatid = $("#parent_category_id").val();
     if($maincatid!=0)
     {
         $.ajax({
          type : "POST",
          url : "{{ url('/') }}/admin/subcategorylist",
          data : $('#categoryform').serialize(),
          beforeSend : function() {
          },
          success : function(data) { 
            $("#subcatlist").parent().show();
            $("#subcatlist").html("");
            $("#subcatlist").append(data);
            },
        });
     }
     else
     {
        $("#subcatlist").html("");
        $("#subcatlist").parent().hide();
     }
   
   }
</script>
@if ($pid != '')

<script type="text/javascript">
$(function () {
  subcatlist();
  });
</script>
 @endif
