<?php
use Carbon\Carbon;
?>
@php

    $active = DB::table('cms_users')->where('status',1)->where('id_cms_privileges','!=',1)->count();
    $user = DB::table('cms_users')->where('id_cms_privileges','!=',1)->count();

    $app_user_count = DB::table('cms_users')->where('status',1)->where('user_type','app')->count();
    
    $language_arabic = DB::table('tbl_language_time')->where('language_name','arabic')->first();
    $language_english = DB::table('tbl_language_time')->where('language_name','english')->first();

    $company_usage = DB::table('tbl_company_time')->get();
    $categories = DB::table('tbl_category')->where('parent_category_id',0)->count();
    $sub_categories = DB::table('tbl_service')->where('parent_category_id','!=',0)->count();

    $inactive = DB::table('cms_users')->where('status',0)->where('id_cms_privileges','!=',1)->count();
    $company = DB::table('tbl_service')->count();
    $active_com = DB::table('tbl_service')->where('status',1)->count();
    $inactive_com = DB::table('tbl_service')->where('status',0)->count();
    $data_label =  $data_value = [];
    //Bar char info
    $data = DB::table('tbl_reports')
                    ->join('tbl_service','tbl_reports.service_id','=','tbl_service.id')
                    ->select(DB::raw('count(*) as count, tbl_service.service_name'))
                    ->groupBy('tbl_reports.service_id')
                     ->get();
        foreach($data as $da) {
            $data_label[] = (string)$da->service_name;
            $data_value[] = $da->count;
            }

    $data_labe = implode(',',$data_label);
    $data_va = implode(',',$data_value);

    $now = Carbon::now();
    
    $dates = DB::table('cms_users')->where('id_cms_privileges','!=',1)->where('created_at','like', '%'.$now->year.'%')->get();

    
      foreach($dates as $key => $val){
        $month = Carbon::parse($val->created_at)->format('m');
        $result[$month][$val->gender][$key] = count($val);
      }

      for($i=1; $i<=12; $i++){
      
        if(strlen($i) == 1){
          $i='0'.$i;
        }else{
          $i=$i;
        }
        if(count(@$result[$i])!=0){
          @$result[$i][0]=count(@$result[$i][0]);
          @$result[$i][1]=count(@$result[$i][1]);
        }else{
          @$result[$i][0]=0;
          @$result[$i][1]=0;
        }
      }

      foreach($result as $key=>$value){

        if($key==1){
          $jan_male = $value[1];
          $jan_female =$value[0];
        }
        if($key==2){
          $feb_male = $value[1];
          $feb_female =$value[0];
        }
        if($key==3){
          $mar_male = $value[1];
          $mar_female =$value[0];
        }
        if($key==4){
          $apr_male = $value[1];
          $apr_female =$value[0];
        }
        if($key==5){
          $may_male = $value[1];
          $may_female =$value[0];
        }
        if($key==6){
          $june_male = $value[1];
          $june_female =$value[0];
        }
        if($key==7){
          $jul_male = $value[1];
          $jul_female =$value[0];
        }
        if($key==8){
          $aug_male = $value[1];
          $aug_female =$value[0];
        }
        if($key==9){
          $sep_male = $value[1];
          $sep_female =$value[0];
        }
        if($key==10){
          $oct_male = $value[1];
          $oct_female =$value[0];
        }
        if($key==11){
          $nov_male = $value[1];
          $nov_female =$value[0];
        }
        if($key==12){
          $dec_male = $value[1];
          $dec_female =$value[0];
        }
      }
      

@endphp




<script>

 jQuery(document).ready(function($) {

jQuery('.counter').counterUp({
	//console.log("okav");
  delay: 10,
  time: 2000
});

});

/**/

$(function () {

var ctx = document.getElementById("myChart");
var ctx1 = document.getElementById("myChart1");
var ctx2 = document.getElementById("myChart2");
var ctx3 = document.getElementById("myChart3");
var ctx4 = document.getElementById("myChart4");
var ctx5 = document.getElementById("myChart5");
var ctx6 = document.getElementById("myChart6");
var ctx7 = document.getElementById("myChart7");


var myChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: ["Active Users", "InActive Users"],
        datasets: [{
            label: 'Users',
            data: ['{{ $active }}', '{{ $inactive }}'],
            backgroundColor: [
                '#ff6384',
                '#36a2eb'
                
            ],
            borderColor: [
                '#ff6384',
                '#36a2eb',
                
            ],
           
        }]
    },
   
});

var myChart1 = new Chart(ctx1, {
    type: 'pie',
    data: {
        labels: ["Active Companies", "InActive Companies"],
        datasets: [{
            label: 'Companies',
            data: ['{{ $active_com }}', '{{ $inactive_com }}'],
            backgroundColor: [
                '#cc65fe',
                '#ffce56'
                
            ],
            borderColor: [
                '#cc65fe',
                '#ffce56',
                
            ],
           
        }]
    },
   
});


var myChart3 = new Chart(ctx3, {
    type: 'polarArea',
    data: {
        labels: ["Airtel", "Lg","Sony","Honda"],
        datasets: [{
            label: 'Companies',
            data: [30, 10,20,8],
            backgroundColor: [
                '#ffce56',
                '#00A65E',
                '#d9f442',
                '#f441ca'
                
            ],
            borderColor: [
                '#ffce56',
                '#00A65E',
                '#d9f442',
                '#f441ca'
                
            ],
           
        }]
    },
   
});










 var myChart4 = new Chart(ctx4, {   
    type: 'bar',
    data: {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July','Augest','September','Octber','November','December'],
            datasets: [{
                label: 'Male',
                backgroundColor: window.chartColors.red,
                data: ["{{ $jan_male}}","{{ $feb_male}}","{{ $mar_male}}","{{ $apr_male}}","{{ $may_male}}","{{ $june_male}}","{{ $jul_male}}","{{ $aug_male}}","{{ $sep_male}}","{{ $oct_male}}","{{ $nov_male}}","{{ $dec_male }}"]
            }, {
                label: 'Female',
                backgroundColor: window.chartColors.blue,
                 data: ["{{ $jan_female}}","{{ $feb_female}}","{{ $mar_female}}","{{ $apr_female}}","{{ $may_female}}","{{ $june_female}}","{{ $jul_female}}","{{ $aug_female}}","{{ $sep_female}}","{{ $oct_female}}","{{ $nov_female}}","{{ $dec_female }}"
]
            }]
    },
    options: {
        scales: {
            xAxes: [{
                stacked: true
            }],
            yAxes: [{
                stacked: true
            }]
        }
    }   
   
}); 

var myChart5 = new Chart(ctx5, {   
    type: 'doughnut',
    data: {
        labels: ["{{ $language_arabic->language_name }}" , "{{ $language_english->language_name }} "],
        datasets: [{
            label: 'Votes',
            data: [ '{{ $language_arabic->time }}','{{ $language_english->time }}'],
            backgroundColor: [
                '#0845a8',
                '#08a82d'
                
            ],
            borderColor: [
                '#0845a8',
                '#08a82d',
                
            ],
           
        }]
    },
   
});

var myChart6 = new Chart(ctx6, {   
    type: 'radar',
    data: {
        labels: ["{{$company_usage[0]->company_name}}", "{{$company_usage[1]->company_name}}" , "{{ $company_usage[2]->company_name }}","{{ $company_usage[3]->company_name }}","Airtel" ],
        datasets: [{
            label: 'Company',
            data: [ '{{$company_usage[0]->call_time}}' ,'{{$company_usage[1]->call_time}}','{{$company_usage[2]->call_time}}','{{$company_usage[3]->call_time}}',"50"],
            backgroundColor: [                
                '#e0a15e'                
                
            ],
            borderColor: [               
                '#e0a15e'               
                
            ],
           
        }]
    },
   
});


var myChart2 = new Chart(ctx2, {
    type: 'bar',
    axisYType: "secondary",
    data: {
        labels: [
           "App Users",           
           'Application Used times'
                ],
        datasets: [{
            label: 'Services Used',
            data: ['{{ $app_user_count }}',30],
            backgroundColor: [
                '#cc65fe',                
                '#3336FF'
                
            ],
            borderColor: [
                '#cc65fe',                
                '#3336FF'
                
            ],
           
        }]
    },
   
});

/**5.dec**/

var myChart7 = new Chart(ctx7, {   
    type: 'doughnut',
    data: {
        labels: ["Calls" , "Websites", "Location", "Instagram"],
        datasets: [{
            label: 'Votes',
            data: [ '25','50','70','90'],
            backgroundColor: [
                '#00C0EF',
                '#00A65A',
				'#DD4B39',
				'#F39C12'
                
            ],
            borderColor: [
                '#00C0EF',
                '#00A65A',
				'#DD4B39',
				'#F39C12'
            ],
           
        }]
    },
   
});

});
</script>


    <div id='modal-statistic' class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Modal title</h4>
          </div>
          <div class="modal-body">
            <p>One fine body&hellip;</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn-submit btn btn-primary" data-loading-text="Saving..." autocomplete="off">Save changes</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div id='statistic-area'>

<div id="statistic-area">


        <div class="statistic-row row">
            <div id="area1" class="col-sm-3 connectedSortable">             

            <div id="3dd9600313cd1f6715dd18c15b99ab87" class="border-box">
      <a href="admin/users">                                                   
    <div class="small-box bg-aqua   ">
         
        <div class="inner inner-box">
          <h3>{{$user}}</h3>
          <p>Users  </p>
        </div>
        <div class="icon">
          <i class="fa fa-user"></i>
        </div>
        <span class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></span>
    </div></a>

  
</div>
    </div>
            <div id="area2" class="col-sm-3 connectedSortable">
               
            <div id="6a30ce72a199906019e420241e46cac1" class="border-box">
      <a href="admin/category">                                                    
    <div class="small-box bg-green  ">
         
        <div class="inner inner-box">
          <h3>{{$categories}}</h3>
          <p>Categories </p>
        </div>
        <div class="icon">
          <i class="fa fa-dropbox  "></i>
        </div>
        <span class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></span>
    </div></a>

   
</div>
    </div>
            <div id="area3" class="col-sm-3 connectedSortable">

            <div id="2583690e9c0b6b9d1c95c757c26dcbc7" class="border-box">
      <a href="admin/subcategory">                                                     
    <div class="small-box bg-red    ">
         
        <div class="inner inner-box">
          <h3>{{ $sub_categories}}</h3>
          <p>Sub Categories </p>
        </div>
        <div class="icon">
          <i class="fa fa-cube"></i>
        </div>
        <span class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></span>
    </div></a>

  
</div>
    </div>
            <div id="area4" class="col-sm-3 connectedSortable">
                
            <div id="3b2085a3f24fc654bca8c325630988b4" class="border-box">
      <a href="admin/company">                                                     
    <div class="small-box bg-yellow ">
         
        <div class="inner inner-box">
          <h3>{{$company}}</h3>
          <p>Companies  </p>
        </div>
        <div class="icon">
          <i class="fa fa-building-o"></i>
        </div>
        <span class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></span>
    </div></a>

  
</div>
    </div>            
        </div>







             <div class="col-md-5" style="display: none;">
                <div class="box box-danger">
                         <div class="box-header with-border">
                                 <div class="box-body">
                                        <canvas id="myChart2" width="0" height="0" class="chartjs-render-monitor" style="display: block; width: 0px; height: 0px;"></canvas>
                                 </div> 
                         </div>
                </div>  

             </div>
           
        </div>


    
        



    </div>

        <div class="row chart_full">
			<div class="col-md-4">
				<div class="md_box box box-danger">
							 <div class="box-header with-border app_customer">
								  <span style="display: block;margin: 0 auto;text-align: center;font-size: 20px;font-weight: bold;"> Number of times application is used</span>
								 <!-- <h3 class="box-title">User Info</h3> -->
									<div class="box-body">
											 <h1><span class="counter">{{$user}}</span></h1>
											  
											  <i class="fa fa-users"></i>
									 </div> 
							</div>
					</div>     
			</div> 
		
		
				
            <div class="col-md-4">
               <div class="md_box box box-danger">
                         <div class="box-header with-border">
                              <span style="display: block;margin: 0 auto;text-align: center;font-size: 20px;font-weight: bold;"> Users </span>
                             <!-- <h3 class="box-title">User Info</h3> -->
                                <div class="box-body">
                                        <canvas id="myChart" width="300" height="200"></canvas>
                                 </div> 

                        </div>
                         <a href="admin/user_report" class="btn" style="float: right; padding-right: 10px;margin-right: 25px; background: #d2d6de;">Report</a>
                </div>
                
            </div>

            <div class="col-md-4">
               <div class="md_box box box-danger">

                         <div class="box-header with-border">   
                          <span style="display: block;margin: 0 auto;text-align: center;font-size: 20px;font-weight: bold;"> Companies </span>                        
                                <div class="box-body">
                                        <canvas id="myChart1" width="300" height="200"></canvas>
                                 </div> 
                        </div>
                         <a href="admin/company_report" class="btn" style="float: right; padding-right: 10px;margin-right: 25px; background: #d2d6de;">Report</a>
                </div>
                 
            </div>




            <div class="col-md-4">
               <div class="md_box box box-danger">
                         <div class="box-header with-border">
                             <!-- <h3 class="box-title">User <I></I>nfo</h3> -->
                                <span style="display: block;margin: 0 auto;text-align: center;font-size: 20px;font-weight: bold;">Number of Times a Company is Called </span>
                                <div class="box-body">
                                        <canvas id="myChart3" width="300" height="200"></canvas>
                                 </div> 
                        </div>
                </div>
                 
            </div>
			
			<div class="col-md-4">
               <div class="md_box box box-danger">
                         <div class="box-header with-border">                         
                               <span style="display: block;margin: 0 auto;text-align: center;font-size: 20px;font-weight: bold;"> Demographics of Users</span>
                                <div class="box-body">
                                        <canvas id="myChart4" width="300" height="200"></canvas>
                                 </div> 
                        </div>
                </div>
                
            </div>


            <div class="col-md-4">
               <div class="md_box box box-danger">
                         <div class="box-header with-border">
                             <!-- <h3 class="box-title">User <I></I>nfo</h3> -->
                                <span style="display: block;margin: 0 auto;text-align: center;font-size: 20px;font-weight: bold;">Statistics on language selected </span>
                                <div class="box-body">
                                        <canvas id="myChart5" width="300" height="200"></canvas>
                                 </div> 
                        </div>
                </div>
                 
            </div>

            <div class="col-md-4">
               <div class="md_box box box-danger">
                         <div class="box-header with-border">                         
                               <span style="display: block;margin: 0 auto;text-align: center;font-size: 20px;font-weight: bold;"> Time taken for the operator to answer the call by company</span>
                                <div class="box-body">
                                        <canvas id="myChart6" width="300" height="200"></canvas>
                                 </div> 
                        </div>
                </div>
                
            </div>

			 <div class="col-md-4">
               <div class="md_box box box-danger">
                         <div class="box-header with-border">
                             <!-- <h3 class="box-title">User <I></I>nfo</h3> -->
                                <span style="display: block;margin: 0 auto;text-align: center;font-size: 20px;font-weight: bold;">Statistics on usage</span>
                                <div class="box-body">
                                        <canvas id="myChart7" width="300" height="200"></canvas>
                                 </div> 
                        </div>
                </div>
                 
            </div>



             <div class="col-md-5" style="display: none;">
                <div class="box box-danger">
                         <div class="box-header with-border">
                                 <div class="box-body">
                                        <canvas id="myChart2" width="150" height="100"></canvas>
                                 </div> 
                         </div>
                </div>  

             </div>
           
        </div>


    
        



    </div>