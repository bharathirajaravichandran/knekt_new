<style type="text/css">
.kv-file-remove,.kv-file-upload, .fileinput-remove-button {display:none;}
.Err, .Err1 { display:none; color:red!important; }
.reqform {color:red !important;}
</style>

<section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="box box-info">
            <div class="box-header with-border">
               <!-- <h3 class="box-title">Create</h3> -->
               <div class="box-tools">
                 <!--  <div class="btn-group pull-right" style="margin-right: 5px">
                     <a href="{{ url('/')}}/admin" class="btn btn-sm btn-default" title="List"><i class="fa fa-list"></i><span class="hidden-xs">&nbsp;List</span></a>
                  </div> -->
               </div>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{ url('admin/export_company_data_view')}}" method="post" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" id="export_user_data_view">
               <div class="box-body">
                  <div class="fields-group">                 
                    
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label  class="col-sm-2  control-label">{{ trans('admin.form_menu.start_date') }}</label>
                        <div class="col-sm-3 start_date_error">
                            <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                              <input type="date" id="start_date" name="start_date" value="" class="form-control start_date" placeholder="Start Date" />
                           </div>
                        </div>
                        <label  class="col-sm-2  control-label">{{ trans('admin.form_menu.end_date') }}</label>
                        <div class="col-sm-3 end_date_error">
                            <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                              <input type="date" id="end_date" name="end_date" value="<?php echo date('Y-m-d'); ?>" class="form-control end_date" placeholder="End Date" />
                           </div>
                        </div>                        
                                        
                    </div>
                    

                     <div class="form-group ">
                       <label for="type" class="col-sm-2  control-label">{{ trans('admin.form_menu.types') }}</label>
                          <div class="col-sm-2">                            
                            <select class="form-control type" style="width: 100%;" name="type" id="type">         
                                <option value="category">Parent Categories</option>                              
                                <option value="status">Status</option>
                            </select>
                         </div>                       
                     </div>


                      <div class="col-md-12">
                        <div class="btn-group" style="text-align:center;display:block;">
                            <button type="submit" class="btn btn-primary" style="float:none;" onclick="return validation();">{{ trans('admin.form_menu.report') }}</button>
                        </div>   
                      </div>
               </div>

              

            
            </form>
         </div>
      </div>
   </div>
</section>
<script type="text/javascript">

   function validation()
   {
   var start_date=$('#start_date').val();
   var end_date=$('.end_date').val();
   // alert(start_date);

       var err='';
       if(start_date=='')
       {
   
           // $('#company_id').parent('.col-sm-8').parent('.form-group').addClass('has-error');
            $('.start_date_error1').remove();
           $('.start_date_error').prepend('<label class="control-label start_date_error1" for="inputError" style="color:red;"><i class="fa fa-times-circle-o"></i>The Start Date field is required.</label>');
                 
               $('#start_date').focus();
               err='set';
   
       }
   
       if(end_date=='')
       {
           // $('#language').parent('.col-sm-8').parent('.form-group').addClass('has-error');
           $('.end_date_error1').remove(); 
           $('.end_date_error').prepend('<label class="control-label end_date_error1" for="inputError" style="color:red;"><i class="fa fa-times-circle-o"></i> The End Date field is required.</label>');
            
               $('#end_date').focus();
               err='set';
       }else{
   
       }
       
       if(err!='') 
       {
           return false;
       }else{
           $('.end_date_error1').remove(); 
           $('.start_date_error1').remove();
           $('#company_details').submit();
       }
   }
   $( document ).ready(function() {
           $(".type").select2({"allowClear":false,"placeholder":"Product Preferences"});       
       });
</script>