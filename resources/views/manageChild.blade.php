<ul>
@foreach($childs as $child)
	<li>
	    {{ $child->option_name }} ({{ $child->option_value }})
	@if(count($child->childs))
            @include('manageChild',['childs' => $child->childs])
        @endif
	</li>
@endforeach
</ul>