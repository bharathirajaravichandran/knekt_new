<link rel="stylesheet" href="{{url('/')}}/css/intlTelInput.css">
<link rel="stylesheet" href="{{url('/')}}/css/intlTelInput.min.css">
<script src="{{url('/')}}/js/intlTelInput.js" ></script>
<script src="{{url('/')}}/js/utils.js" ></script>
<script src="{{url('/')}}/js/intlTelInput.min.js" ></script>
<script src="{{url('/')}}/js/intlTelInput-jquery.js" ></script>
<script src="{{url('/')}}/js/intlTelInput-jquery.min.js" ></script>
<style type="text/css">
.kv-file-remove,.kv-file-upload, .fileinput-remove-button {display:none;}
.Err, .Err1, .Err2{ display:none; color:red!important; }
.reqform {color:red !important;}
</style>
<?php
if($pid!="" && $pid!=0) 
{
    $phonecode="kw";
    if($user->phone_country_code!="")
    {
        $phonecountrycode = explode("-",$user->phone_country_code);
        if(count($phonecountrycode)==2)
        $phonecode=$phonecountrycode[1];
        else
        $phonecode="kw";
    }
}
else
$phonecode="kw";

?>
<section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="box box-info">
            <div class="box-header with-border">
               <h3 class="box-title">{{ trans('admin.form_menu.create') }}</h3>
               <div class="box-tools">
                  <div class="btn-group pull-right" style="margin-right: 5px">
                     <a href="{{ url('/')}}/admin/users" class="btn btn-sm btn-default" title="List"><i class="fa fa-list"></i><span class="hidden-xs">&nbsp;{{ trans('admin.form_menu.list') }}</span></a>
                  </div>
               </div>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{ url('/')}}/admin/saveusers" method="post" accept-charset="UTF-8" class="form-horizontal usersform" enctype="multipart/form-data" pjax-container id="usersform">
               <div class="box-body">
                  <div class="fields-group">
                     <div class="form-group  ">
                        <label for="name" class="col-sm-2  control-label">{{ trans('admin.form_menu.name') }} <span class="reqform">*</span></label>
                        <div class="col-sm-8" id="name_inner">
                            <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Name field is required.</label>
                           <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                              <input type="text" id="name" name="name" value="{{(isset($user->name))?$user->name:''}}" class="form-control name" placeholder="Input Name" maxlength="30" />
                           </div>
                        </div>
                     </div>
                     <div class="form-group  ">
                        <label for="photo" class="col-sm-2  control-label">{{ trans('admin.form_menu.photo') }}</label>
                        <div class="col-sm-8" id="photo_inner">
                            <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> Please Upload Valid Image.</label>
                            @if($pid!=0)
                           <input type="file" class="photo" id="photo" name="photo" value="{{$user->photo}}"   />   
                           @else
                           <input type="file" class="photo" id="photo" name="photo" /> 
                           @endif
                        </div>
                     </div>
                     <div class="form-group  ">
                        <label for="email" class="col-sm-2  control-label">{{ trans('admin.form_menu.email') }}</label>
                        <div class="col-sm-8" id="email_inner">
                            <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Email field is required.</label>
                            <label class="control-label Err1" for="inputError"><i class="fa fa-times-circle-o"></i> The Email field is Invalid.</label> 
                            <label class="control-label Err2" for="inputError"><i class="fa fa-times-circle-o"></i>Email or User already Registered</label>
                           <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                              <input type="email" id="email" name="email" value="{{(isset($user->email))?$user->email:''}}" class="form-control email" placeholder="Input Email" maxlength="30"/>
                           </div>
                        </div>
                     </div>
                     <div class="form-group  ">
                        <label for="password" class="col-sm-2  control-label">{{ trans('admin.form_menu.password') }} <span class="reqform">*</span></label>
                        <div class="col-sm-8" id="password_inner">
                            <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Password field is required.</label>
                             <label class="control-label Err1" for="inputError"><i class="fa fa-times-circle-o"></i> The Password field must have atleast 6 characters.</label>
                             <label class="control-label Err2" for="inputError"><i class="fa fa-times-circle-o"></i>Password Must Contain Caps,Character and Number.</label>
                           <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-eye-slash fa-fw"></i></span>
                              <input type="password" id="password" name="password" value="{{(isset($user->password))?$user->password:''}}" class="form-control password" placeholder="Input Password" />
                           </div>
                        </div>
                     </div>
                     <div class="form-group  ">
                        <label for="phone" class="col-sm-2  control-label">{{ trans('admin.form_menu.phone') }} <span class="reqform">*</span></label>
                        <div class="col-sm-8" id="phone_inner">
                             <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Phone field is Invalid.</label>
                              <label class="control-label Err1" for="inputError"><i class="fa fa-times-circle-o"></i> The Phone field is already Exist.</label>
                           <div class="input-group" id="phncde">
                              <span class="input-group-addon"><i class="fa fa-phone fa-fw"></i></span>
                              <input type="text" id="phone" name="phone" value="{{(isset($user->phone))?$user->phone:''}}" class="form-control phone" placeholder="Input Phone" maxlength="15" />
                           </div>
                        </div>
                     </div>
                     <div class="form-group  ">
                        <label for="dob" class="col-sm-2  control-label">{{ trans('admin.form_menu.dob') }} <span class="reqform">*</span></label>
                        <div class="col-sm-8" id="dob_inner">
                            <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The dob field is Invalid.</label>
                           <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>
                              <input style="width: 110px" type="text" id="dob" name="dob" value="{{(isset($user->dob))?$user->dob:'2000-01-01'}}" class="form-control dob" placeholder="Input Dob" />
                           </div>
                        </div>
                     </div>
                      <div class="form-group  ">
                        <label for="gender" class="col-sm-2  control-label">{{ trans('admin.form_menu.gender') }} <span class="reqform">*</span></label>
                        <div class="col-sm-8" id="gender_inner">
                            <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i>The Gender field is required</label>
                            <select class="form-control gender" style="width: 100%;" name="gender" id="gender">
                                <option value="">Please Select</option>
                                <option value="0" selected>Female</option>
                                <option value="1" {{ (isset($user->gender) && $user->gender==1) ? 'selected': '' }}>Male</option>
                            </select>
                        </div>
                     </div>
                      <div class="form-group  ">
                        <label for="language" class="col-sm-2  control-label">{{ trans('admin.form_menu.language') }} <span class="reqform">*</span></label>
                         <div class="col-sm-8" id="language_inner">
                            <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i>The Gender field is required</label>
                            <select class="form-control language" style="width: 100%;" name="language" id="language">
                                <option value="">Please Select</option>
                                <option value="en" selected>English</option>
                                <option value="ar" {{ (isset($user->language) && $user->language==1) ? 'selected': '' }}>Arabic</option>
                            </select>
                        </div>
                     </div>
                     <div class="form-group  ">
                        <label for="facebook_id" class="col-sm-2  control-label">{{ trans('admin.form_menu.facebook_id') }}</label>
                        <div class="col-sm-8" id="facebook_id_inner">
                            <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i>The Facebook Id field is already Exist</label>
                           <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                              <input type="text" id="facebook_id" name="facebook_id" value="{{(isset($user->facebook_id))?$user->facebook_id:''}}" class="form-control facebook_id" placeholder="Input Facebook id" maxlength="50"/>
                           </div>
                        </div>
                     </div>
                     <div class="form-group  ">
                        <label for="twitter_id" class="col-sm-2  control-label">{{ trans('admin.form_menu.twitter_id') }}</label>
                        <div class="col-sm-8" id="twitter_id_inner">
                            <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i>The Twitter Id field is already Exist</label>
                           <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                              <input type="text" id="twitter_id" name="twitter_id" value="{{(isset($user->twitter_id))?$user->twitter_id:''}}" class="form-control twitter_id" placeholder="Input Twitter id" maxlength="50" />
                           </div>
                        </div>
                     </div>
                     <div class="form-group  ">
                        <label for="googleplus_id" class="col-sm-2  control-label">{{ trans('admin.form_menu.googleplus_id') }}</label>
                        <div class="col-sm-8" id="googleplus_id_inner">
                             <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i>The Googleplus Id field is already Exist</label>
                           <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                              <input type="text" id="googleplus_id" name="googleplus_id" value="{{(isset($user->googleplus_id))?$user->googleplus_id:''}}" class="form-control googleplus_id" placeholder="Input Googleplus id" maxlength="50" />
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="status" class="col-sm-2  control-label">{{ trans('admin.form_menu.status') }} <span class="reqform">*</span></label>
                        <div class="col-sm-2" id="status_inner">
                            <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i>The Status field is required</label>
                            <select class="form-control status" style="width: 100%;" name="status" id="status">
                                <option value="">Please Select</option>
                                <option value="0" selected>In Active</option>
                                <option value="1" {{ (isset($user->status) && $user->status==1) ? 'selected': '' }}>Active</option>
                            </select>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- /.box-body -->
               <div class="box-footer">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="pid" value="{{ $pid }}" id="parent_id">
                    <input type="hidden" name="deletecheck" id="deletecheck" class="deletecheck" value="0">
                    <input type="hidden" name="phone_country_code" id="phone_country_code" class="phone_country_code" value="">
                  <div class="col-md-2">
                  </div>
                  <div class="col-md-8">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-primary" onclick="return validation();">{{ trans('admin.form_menu.submit') }}</button>
                    </div>
                     <!-- <div class="btn-group pull-left">
                        <button type="reset" class="btn btn-warning">Reset</button>
                     </div> -->
                </div>
               </div>
               <!-- /.box-footer -->
            </form>
         </div>
      </div>
   </div>
</section>
<script data-exec-on-popstate>
  $(document).ready(function(){
      var fullurl = window.location.pathname.split("/"); 
      var uri_segment = fullurl[4];
      if(uri_segment == 'edit'){
        var phone = $('#phone').val();
        var phone_trim = phone.replace(/\s/g, '');
        $('#phone').val(phone_trim);
      }
     
  });
   $(function () {
                   
         @if(count($image_array)>0)

        $("input.photo").fileinput({
          "initialPreview":[@foreach($image_array as $k=>$v)"{{ url('/') }}/uploads/Users/{{$v}}",@endforeach],
          "initialPreviewConfig":[@foreach($image_array as $k=>$v) {
            "caption":"{{$v}}",
            "url":"{{ url('/') }}/admin/deleteimage",
            "key":"{{$k}}"  
          },
          @endforeach],
          "overwriteInitial":true,
          "initialPreviewAsData":true,
          "browseLabel":"Browse",
          "showRemove":false,
          "showUpload":false,
         // "uploadUrl":"#" , 
          "initialPreviewDelete":true, 
          "allowedFileExtensions": ['jpeg', 'jpg', 'png', 'gif'],
          "deleteExtraData":{"photo[multiple_image]":"_file_del_","_file_del_":"","_token":"{{ csrf_token() }}","_method":"PUT"},
          "deleteUrl":"#"
        });

        @else

        $("input.photo").fileinput({
          "overwriteInitial":true,
          "initialPreviewAsData":true,
          "browseLabel":"Browse",
          "showRemove":true,
          "showUpload":false,
         // "uploadUrl":"#" , 
          "initialPreviewDelete":true, 
          "allowedFileExtensions": ['jpeg', 'jpg', 'png', 'gif'], 
          "deleteExtraData":{"photo":"_file_del_","_file_del_":"","_token":"{{ csrf_token() }}","_method":"PUT"},
          "deleteUrl":"#"
        });
        @endif   
            
       $('.phone').inputmask();
       $('.dob').parent().datetimepicker({"format":"YYYY-MM-DD","locale":"en","allowInputToggle":true});
                   
        $(".status").select2({"allowClear":true,"placeholder":"Please select status"});
        $(".gender").select2({"allowClear":true,"placeholder":"Please select gender"});
        $(".language").select2({"allowClear":true,"placeholder":"Please select language"});
    });

    $('body').on('click', '.fileinput-remove', function(e) {
        $('#deletecheck').val(1);
        $('#photo').val("");
    });

    var phonecountry_code="{{$phonecode}}";
    $('#phone').intlTelInput({
        initialCountry : phonecountry_code, 
        // hiddenInput: 'full_phone',
        separateDialCode: true,
        utilsScript:'http://knekt.dci.in/js/utils.js'
    });

    function validation()
    {

        var name=$('#name').val();
        var photo=$('#photo').val().trim();
        var email=$('#email').val();
        var pwd = $('#password').val();
        var pwd_check = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{10,12}$/;
        var phone=$('#phone').val();
        var dob=$('#dob').val();
        var language=$('#language').val();
        var gender=$('#gender').val();
        var fullurl = window.location.pathname.split("/"); 
        var uri_segment = fullurl[4];
/*      var fbid=$('#facebook_id').val();
        var twid=$('#twitter_id').val();
        var gpid=$('#googleplus_id').val();*/
        var status=$('#status').val();

        var mainid=$('#parent_id').val();
        var delcheckstatus=$('#deletecheck').val();
        var emailregex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        var phone_country_code = $('#phncde .selected-dial-code').text();
        var getcountrycode = $('#phncde .selected-dial-code').prev().attr('class').split(' ');
        $('#phone_country_code').val(phone_country_code+"-"+getcountrycode[1]);

        var err='';
        $('.Err').hide();
        $('.Err1').hide();
        $('.Err2').hide();

        if((name=='' || name==null))
        {
            $('#name').addClass('has-error');
            $('#name_inner .Err').show();
            if(err=='')
            {
                $('#name').focus();
                err='set';
            }
        } 
        if (mainid=="" && (photo=="" || photo==null))
        {
            $('#photo').addClass('has-error');
            $('#photo_inner .Err').show();
            if(err=='')
            {
                $('#photo').focus();
                err='set';
            }
        }
        if (mainid!="" && (photo=="" && delcheckstatus==1))
        {
            $('#photo').addClass('has-error');
            $('#photo_inner .Err').show();
            if(err=='')
            {
                $('#photo').focus();
                err='set';
            }
        }
        if ( $("#photo_inner .file-upload-indicator .glyphicon").hasClass("glyphicon-exclamation-sign") ) 
        {
            $('#photo').addClass('has-error');
            $('#photo_inner .Err').show();
            if(err=='')
            {
                $('#photo').focus();
                err='set';
            }
        }   
        if(email!="" && !emailregex.test(email))
        {
            $('#email').addClass('has-error');
            $('#email_inner .Err').show();
            if(err=='')
            {
                $('#email').focus();
                err='set';
            }
        } 
        if((phone=='' || phone==null || isNaN(phone)))
        {
            $('#phone').addClass('has-error');
            $('#phone_inner .Err').show();
            if(err=='')
            {
                $('#phone').focus();
                err='set';
            }
        } 
        if((pwd=='' || pwd==null))
        {
            $('#password').addClass('has-error');
            $('#password_inner .Err').show();
            if(err=='')
            {
                $('#password').focus();
                err='set';
            }
        }   
        if((pwd!='' && pwd.length <5))
        {
            $('#password').addClass('has-error');
            $('#password_inner .Err1').show();
            if(err=='')
            {
                $('#password').focus();
                err='set';
            }
        }
        if(uri_segment!='edit'){
            if((pwd!='' && !pwd_check.test(pwd)))
            {
                $('#password').addClass('has-error');
                $('#password_inner .Err2').show();
                if(err=='')
                {
                    $('#password').focus();
                    err='set';
                }
            }
        }
        
        if((language==''))
        {
            $('#language').addClass('has-error');
            $('#language_inner .Err').show();
            if(err=='')
            {
                $('#language').focus();
                err='set';
            }
        }  
        if((gender==''))
        {
            $('#gender').addClass('has-error');
            $('#gender_inner .Err').show();
            if(err=='')
            {
                $('#gender').focus();
                err='set';
            }
        }
        if((status=='' || status==null))
        {
            $('#status').addClass('has-error');
            $('#status_inner .Err').show();
            if(err=='')
            {
                $('#status').focus();
                err='set';
            }
        }
        if(err!='')
        {
            $(':input[type="button"]').prop('disabled', false);
            return false;
        }
        else
        {
            checkunique();
        }
    }


    function checkunique() {
        $.ajax({
          type : "POST",
          url : "{{ url('/') }}/admin/userunique",
          data : $('#usersform').serialize(),
          beforeSend : function() {
          },
          success : function(data) { 
            if(data == "1")
            {
                $('#phone').addClass('has-error');
                $('#phone_inner .Err1').show();
                $('#phone').focus();
                return false;
            }
            else if(data == "2")
            {
                $('#facebook_id').addClass('has-error');
                $('#facebook_id_inner .Err').show();
                $('#facebook_id').focus();
                return false;
            } 
            else if(data == "3")
            {
                $('#twitter_id').addClass('has-error');
                $('#twitter_id_inner .Err').show();
                $('#twitter_id').focus();
                return false;
            }
            else if(data == "4")
            {
                $('#googleplus_id').addClass('has-error');
                $('#googleplus_id_inner .Err').show();
                $('#googleplus_id').focus();
                return false;
            }
            else if(data == "5")
            {
                $('#email').addClass('has-error');
                $('#email_inner .Err2').show();
                $('#email').focus();
                return false;
            }
            else
                $('#usersform').submit(); 
        },
    });
   
   }
</script>