<link rel="stylesheet" href="{{url('/')}}/css/intlTelInput.css">
<link rel="stylesheet" href="{{url('/')}}/css/intlTelInput.min.css">
<script src="{{url('/')}}/js/intlTelInput.js" ></script>
<script src="{{url('/')}}/js/utils.js" ></script>
<script src="{{url('/')}}/js/intlTelInput.min.js" ></script>
<script src="{{url('/')}}/js/intlTelInput-jquery.js" ></script>
<script src="{{url('/')}}/js/intlTelInput-jquery.min.js" ></script>
<style type="text/css">
.kv-file-remove,.kv-file-upload, .fileinput-remove-button {display:none;}
.Err, .Err1 { display:none; color:red!important; }
.reqform {color:red !important;}
.intl-tel-input .country-list {z-index: 99999 !important;}
</style>
<?php
if($pid!="" || $pid!=0)
{
     $phonecode="kw";
     $alternatephonecode="kw";
     $smscode="kw";
     $whatsappcode="kw";
    if($company->phone_country_code!="")
    {
        $phonecountrycode = explode("-",$company->phone_country_code);
        if(count($phonecountrycode)==2)
        $phonecode=$phonecountrycode[1];
        else
        $phonecode="kw";
    }

    if($company->alter_country_code!="")
    {
        $altercmpycode = explode("-",$company->alter_country_code);
        if(count($altercmpycode)==2)
        $alternatephonecode=$altercmpycode[1];
        else
        $alternatephonecode="kw";
    }

     if($company->sms_country_code!="")
    {
        $smscountrycode = explode("-",$company->sms_country_code);
        if(count($smscountrycode)==2)
        $smscode=$smscountrycode[1];
        else
        $smscode="kw";
    }

     if($company->whatsapp_country_code!="")
    {
        $whatsappcountrycode = explode("-",$company->whatsapp_country_code);
        if(count($whatsappcountrycode)==2)
        $whatsappcode=$whatsappcountrycode[1];
        else
        $whatsappcode="kw";
    }
    if($company->working_time != ''){
      $working_time = explode('-', $company->working_time);

    }
}
else
{
 $phonecode="kw";
 $alternatephonecode="kw";
 $smscode="kw";
 $whatsappcode="kw";
}


?>
<section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="box box-info">
            <div class="box-header with-border">
               <h3 class="box-title"><?php if($pid!="") echo "Edit"; else echo "Create";?></h3>
               <div class="box-tools">
                  <div class="btn-group pull-right" style="margin-right: 5px">
                     <a href="{{ url('/') }}/admin/company" class="btn btn-sm btn-default" title="List"><i class="fa fa-list"></i><span class="hidden-xs">&nbsp;{{ trans('admin.form_menu.list') }}</span></a>
                  </div>
               </div>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{ url('/') }}/admin/savecompany" method="post" accept-charset="UTF-8" class="form-horizontal companyform" enctype=" multipart /form-data" pjax-container="" id="companyform">
               <div class="box-body">
                  <div class="fields-group">
                     <div class="form-group  ">
                        <label for="parent_category_id" class="col-sm-2  control-label">{{ trans('admin.form_menu.category') }}<span class="reqform">*</span></label>
                            <div class="col-sm-8" id="parent_category_id_inner">
                                 <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Category field is required.</label> 
                                <select class="form-control status" style="width: 100%;" name="parent_category_id" class="parent_category_id" id="parent_category_id" onchange="subcatlist(this.value);">
                                    <option value="">Please select Category</option>
                                     @foreach ($catname as $k=>$v)
                                    <option value="{{ $k }}" {{ (isset($company->parent_category_id) && $company->parent_category_id==$k) ? 'selected': '' }}>{{ $v }}</option>
                                    @endforeach
                                </select>
                            </div>
                      </div>
                     <div class="form-group  ">
                        <label for="sub_cat_id" class="col-sm-2  control-label">{{ trans('admin.form_menu.subcategory') }}</label>
                        <div class="col-sm-8" id="subcatlist">
                            <select class="form-control status" style="width: 100%;" name="sub_cat_id" class="sub_cat_id" id="sub_cat_id">
                            <option value="">Please select</option>
                            </select>
                        </div>
                     </div>
                     <div class="form-group  ">
                        <label for="service_name" class="col-sm-2  control-label">{{ trans('admin.form_menu.company_name') }}<span class="reqform">*</span></label>
                        <div class="col-sm-8" id="service_name_inner">
                            <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Company Name field is required.</label> 
                            <label class="control-label Err1" for="inputError"><i class="fa fa-times-circle-o"></i> This Company name is already exist.</label> 
                           <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                              <input type="text" id="service_name" name="service_name" value="{{(isset($company->service_name))?$company->service_name:''}}" class="form-control service_name" placeholder="Input Service name" maxlength="30">
                           </div>
                        </div>
                     </div>
                     <div class="form-group  ">
                        <label for="service_description" class="col-sm-2  control-label">{{ trans('admin.form_menu.company_description') }}<span class="reqform">*</span></label>
                        <div class="col-sm-8" id="service_description_inner">
                            <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Company Description field is required.</label> 
                           <textarea name="service_description" class="form-control service_description" id="service_description" rows="5" placeholder="Input Service description">{{(isset($company->service_description))?$company->service_description:''}}</textarea>
                        </div>
                     </div>
                     <div class="form-group  ">
                        <label for="service_image" class="col-sm-2  control-label">{{ trans('admin.form_menu.company_image') }}<span class="reqform">*</span></label>
                        <div class="col-sm-8" id="service_image_inner">
                            <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> Please Upload Valid Image.</label>
                            @if($pid!=0)
                           <input type="file" class="service_image" id="service_image" name="service_image" value="{{$company->service_image}}"   />   
                           @else
                           <input type="file" class="service_image" id="service_image" name="service_image" /> 
                           @endif
                        </div>
                     </div>
<!--                      <div class="form-group  ">
                        <label for="notification" class="col-sm-2  control-label">Notification</label>
                        <div class="col-sm-8">
                           <textarea name="notification" class="form-control notification" rows="5" placeholder="Input Notification">{{(isset($company->notification))?$company->notification:''}}</textarea>
                        </div>
                     </div> -->
                     <div class="form-group  ">
                        <label for="email" class="col-sm-2  control-label">{{ trans('admin.form_menu.email') }}</label>
                        <div class="col-sm-8" id="email_inner">
                             <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Email field is invalid.</label> 
                           <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                              <input type="email" id="email" name="email" value="{{(isset($company->email))?$company->email:''}}" class="form-control email" placeholder="Input Email" size="30">
                           </div>
                        </div>
                     </div>
                     <div class="form-group  ">
                        <label for="phone" class="col-sm-2  control-label">{{ trans('admin.form_menu.phone') }} <span class="reqform">*</span></label>
                        <div class="col-sm-8" id="phone_inner">
                            <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Phone field is invalid.</label> 
                           <div class="input-group" id="phncde">
                              <span class="input-group-addon"><i class="fa fa-phone fa-fw"></i></span>
                              <input type="tel" id="phone" name="phone" value="{{(isset($company->phone))?$company->phone:''}}" class="form-control phone" placeholder="Input Phone" maxlength="15">
                           </div>
                        </div>
                     </div>
                     <div class="form-group  ">
                        <label for="alternatephone" class="col-sm-2  control-label">{{ trans('admin.form_menu.alternate_phone') }}</label>
                        <div class="col-sm-8" id="alternatephone_inner">
                             <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Alternate Phone field is invalid.</label> 
                           <div class="input-group" id="altphncde">
                              <span class="input-group-addon"><i class="fa fa-phone fa-fw"></i></span>
                              <input type="tel" id="alternatephone" name="alternatephone" value="{{(isset($company->alternatephone))?$company->alternatephone:''}}" class="form-control alternatephone" placeholder="Input Alternatephone" maxlength="15">
                           </div>
                        </div>
                     </div>
                     <div class="form-group  ">
                        <label for="sms" class="col-sm-2  control-label">{{ trans('admin.form_menu.sms') }}</label>
                        <div class="col-sm-8" id="sms_inner">
                            <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The SMS field is invalid.</label> 
                           <div class="input-group" id="smscde">
                              <span class="input-group-addon"><i class="fa fa-phone fa-fw"></i></span>
                              <input type="tel" id="sms" name="sms" value="{{(isset($company->sms))?$company->sms:''}}" class="form-control sms" placeholder="Input Sms" maxlength="15">
                           </div>
                        </div>
                     </div>
                     <div class="form-group  ">
                        <label for="whatsapp" class="col-sm-2  control-label">{{ trans('admin.form_menu.whatsapp') }}</label>
                        <div class="col-sm-8" id="whatsapp_inner">
                            <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Whatsapp field is invalid.</label> 
                           <div class="input-group" id="whatsappcde">
                              <span class="input-group-addon"><i class="fa fa-phone fa-fw"></i></span>
                              <input type="tel" id="whatsapp" name="whatsapp" value="{{(isset($company->whatsapp))?$company->whatsapp:''}}" class="form-control whatsapp" placeholder="Input Whatsapp" maxlength="15">
                           </div>
                        </div>
                     </div>
                     <div class="form-group  ">
                        <label for="web" class="col-sm-2  control-label">{{ trans('admin.form_menu.live_chart') }}</label>
                        <div class="col-sm-8" id="web_inner">
                             <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Live Chat field is invalid.</label> 
                           <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                              <input type="text" id="web" name="web" value="{{(isset($company->web))?$company->web:''}}" class="form-control web" placeholder="Input Web">
                           </div>
                        </div>
                     </div>
                     <div class="form-group  ">
                        <label for="website" class="col-sm-2  control-label">{{ trans('admin.form_menu.website') }}</label>
                        <div class="col-sm-8" id="website_inner">
                             <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Website field is invalid.</label> 
                           <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                              <input type="text" id="website" name="website" value="{{(isset($company->website))?$company->website:''}}" class="form-control website" placeholder="Input Website">
                           </div>
                        </div>
                     </div>
                     <div class="form-group  ">
                        <label for="linkedin" class="col-sm-2  control-label">{{ trans('admin.form_menu.linkedin') }}</label>
                        <div class="col-sm-8" id="linkedin_inner">
                             <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Linkedin field is invalid.</label> 
                           <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                              <input type="text" id="linkedin" name="linkedin" value="{{(isset($company->linkedin))?$company->linkedin:''}}" class="form-control linkedin" placeholder="Input Linkedin">
                           </div>
                        </div>
                     </div>

                     <div class="form-group  ">
                        <label for="status" class="col-sm-2  control-label">{{ trans('admin.form_menu.opening_time') }} <span class="reqform"></span></label>
                        <div class="col-sm-2" id="status_inner">
                            <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i>The Status field is required</label>
                            <select class="form-control opening_time" style="width: 100%;" name="opening_time" id="opening_time">
                               
                                <option value="0" selected>Open All Time</option>
                                <option value="1" {{ (isset($company->opening_time) && $company->opening_time == 1) ? 'selected': '' }}>Select Timing</option>
                            </select>
                        </div>
                     </div>

                     <div class="form-group working_time" style="display: none; ">
                        <label for="time" class="col-sm-2  control-label">{{ trans('admin.form_menu.time') }} <span class="reqform"></span></label>
                        <div class="col-sm-6" id="open_time_inner">
                            <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i>The Status field is required</label>
                             <label for="status" class="col-sm-2  control-label">{{ trans('admin.form_menu.open_time') }}<span class="reqform"></span></label>
                             <div class='input-group datetimepicker3' id='datetimepicker3'>
                              <input type='text' class="form-control start_time" id="start_time" value="{{(isset($working_time[0]))?$working_time[0]:'00:00:00'}}" />
                              <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-time"></span>
                              </span>
                            </div>

                            <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i>The Status field is required</label>
                            <label for="status" class="col-sm-2  control-label">{{ trans('admin.form_menu.close_time') }}<span class="reqform"></span></label>
                            <div class='input-group datetimepicker2' id='datetimepicker2'>
                              <input type='text' class="form-control close_time" id="close_time" value="{{(isset($working_time[0]))?$working_time[1]:'23:59:59'}}" />
                              <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-time"></span>
                              </span>
                            </div>
                        </div>
                            
                        </div> 
                      


                     <div class="form-group  ">
                        <label for="status" class="col-sm-2  control-label">{{ trans('admin.form_menu.status') }} <span class="reqform">*</span></label>
                        <div class="col-sm-2" id="status_inner">
                            <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i>The Status field is required</label>
                            <select class="form-control status" style="width: 100%;" name="status" id="status">
                                <option value="">Please Select</option>
                                <option value="0" selected>In Active</option>
                                <option value="1" {{ (isset($company->status) && $company->status==1) ? 'selected': '' }}>Active</option>
                            </select>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- /.box-body -->
               <div class="box-footer">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="pid" value="{{ $pid }}" id="parent_id">
                <input type="hidden" name="deletecheck" id="deletecheck" class="deletecheck" value="0">
                <input type="hidden" name="subchilid" id="subchilid" class="subchilid" value="{{(isset($company->sub_cat_id))?$company->sub_cat_id:''}}">
                <input type="hidden" name="phone_country_code" id="phone_country_code" class="phone_country_code" value="">
                <input type="hidden" name="alter_country_code" id="alter_country_code" class="alter_country_code" value="">
                <input type="hidden" name="sms_country_code" id="sms_country_code" class="sms_country_code" value="">
                <input type="hidden" name="whatsapp_country_code" id="whatsapp_country_code" class="whatsapp_country_code" value="">
               
                <input type="hidden" name="starting_time" id="starting_time" class="starting_time" value="">
               
                <input type="hidden" name="closing_time" id="closing_time" class="closing_time" value="">
                  <div class="col-md-2">
                  </div>
                  <div class="col-md-8">
                     <div class="btn-group pull-right">
                        <button type="button" class="btn btn-primary" onclick="return validation();">{{ trans('admin.form_menu.submit') }}</button>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</section>
 <script type="text/javascript">
            $(function () {
                $('#datetimepicker3').datetimepicker({
                    format: 'HH:mm:ss'
                }); 
                $('#datetimepicker2').datetimepicker({
                    format: 'HH:mm:ss'
                });
            });
             var open_time = $('#opening_time').val();
            if(open_time==1){
                $('.working_time').css('display','block');
              }
            $('#opening_time').on('change',function(){
              var open_time = $('#opening_time').val();
              if(open_time==1){
                $('.working_time').css('display','block');
              }
              if(open_time==0){
                $('.working_time').css('display','none');
              }

            });
              var starting_time = $('#start_time').val();
             if(starting_time==''){
               $('#starting_time').val('00:00:00');
             }else{
               $('#starting_time').val(starting_time);
             }
            $('#start_time').on('blur focusout change',function(){
              var starting_time = $('#start_time').val();
              $('#starting_time').val(starting_time);
            });
             var closing_time = $('#close_time').val();
             if(closing_time==''){
               $('#closing_time').val('23:59:59');
             }else{
               $('#closing_time').val(closing_time);
             }
            $('#close_time').on('blur focusout change',function(){
              var closing_time = $('#close_time').val();
              $('#closing_time').val(closing_time);
            });
        </script>
<script data-exec-on-popstate>
   $(function () {  
      $( document ).ready(function() { 
        $(".Status").select2({"allowClear":false,"placeholder":"Status"});
        $("#phone").val($.trim($("#phone").val().split(" ").join("")));
        $("#alternatephone").val($.trim($("#alternatephone").val().split(" ").join("")));
        $("#sms").val($.trim($("#sms").val().split(" ").join("")));
        $("#whatsapp").val($.trim($("#whatsapp").val().split(" ").join(""))); 
      });
       
        @if(count($image_array)>0)

        $("input.service_image").fileinput({
          "initialPreview":[@foreach($image_array as $k=>$v)"{{ url('/') }}/uploads/Company/{{$v}}",@endforeach],
          "initialPreviewConfig":[@foreach($image_array as $k=>$v) {
            "caption":"{{$v}}",
            "url":"{{ url('/') }}/admin/deleteimage",
            "key":"{{$k}}"  
          },
          @endforeach],
          "overwriteInitial":true,
          "initialPreviewAsData":true,
          "browseLabel":"Browse",
          "showRemove":false,
          "showUpload":false,
         // "uploadUrl":"#" , 
          "initialPreviewDelete":true, 
          "allowedFileExtensions": ['jpeg', 'jpg', 'png', 'gif'],
          "deleteExtraData":{"service_image[multiple_image]":"_file_del_","_file_del_":"","_token":"{{ csrf_token() }}","_method":"PUT"},
          "deleteUrl":"#"
        });

        @else

        $("input.service_image").fileinput({
          "overwriteInitial":true,
          "initialPreviewAsData":true,
          "browseLabel":"Browse",
          "showRemove":true,
          "showUpload":false,
         // "uploadUrl":"#" , 
          "initialPreviewDelete":true, 
          "allowedFileExtensions": ['jpeg', 'jpg', 'png', 'gif'], 
          "deleteExtraData":{"service_image":"_file_del_","_file_del_":"","_token":"{{ csrf_token() }}","_method":"PUT"},
          "deleteUrl":"#"
        });

        @endif   
      $(".status").select2({"allowClear":true,"placeholder":"Please select status"});
      $(".parent_category_id").select2({"allowClear":true,"placeholder":"Please select"});
      $(".sub_cat_id").select2({"allowClear":true,"placeholder":"Please select"});
    });

    $('body').on('click', '.fileinput-remove', function(e) {
        $('#deletecheck').val(1);
        $('#service_image').val("");
    });

    function subcatlist()
   {
     $maincatid = $("#parent_category_id").val();
     if($maincatid!=0)
     {
         $.ajax({
          type : "POST",
          url : "{{ url('/') }}/admin/subcategory",
          data : $('#companyform').serialize(),
          beforeSend : function() {
          },
          success : function(data) { 
            $("#subcatlist").parent().show();
            $("#subcatlist").html("");
            $("#subcatlist").append(data);
            },
        });
     }
     else
     {
        $("#subcatlist").html("");
        $("#subcatlist").parent().hide();
     }
   
   }

    function validation()
    {
        var parentcat=$('#parent_category_id').val();
        var servicename=$('#service_name').val().trim();
        var serviceimage=$('#service_image').val();
        var servicedesc=$('#service_description').val();
        var phonenumber=$('#phone').val();
        var altnumber=$('#alternatephone').val();
        var smsnumber=$('#sms').val();
        var whatsappnumner=$('#whatsapp').val();
        var status=$('#status').val();
        var mainid=$('#parent_id').val();
        var delcheckstatus=$('#deletecheck').val();
        var web=$('#web').val();
        var email=$('#email').val();
        var website=$('#website').val();
        var linkedin=$('#linkedin').val();
        var delcheckstatus=$('#deletecheck').val();
        var urlregex = /(http(s)?:\\)?([\w-]+\.)+[\w-]+[.com|.in|.org]+(\[\?%&=]*)?/;
        var emailregex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        var phone_country_code = $('#phncde .selected-dial-code').text();
        var getcountrycode = $('#phncde .selected-dial-code').prev().attr('class').split(' ');
        $('#phone_country_code').val(phone_country_code+"-"+getcountrycode[1]);

        var alter_country_code = $('#altphncde .selected-dial-code').text();
        var getaltercode = $('#altphncde .selected-dial-code').prev().attr('class').split(' ');
        $('#alter_country_code').val(alter_country_code+"-"+getaltercode[1]);

        var sms_country_code = $('#smscde .selected-dial-code').text();
        var getsmscode = $('#smscde .selected-dial-code').prev().attr('class').split(' ');
        $('#sms_country_code').val(sms_country_code+"-"+getsmscode[1]);

        var whatsapp_country_code = $('#whatsappcde .selected-dial-code').text();
        var getwhatsappcode = $('#whatsappcde .selected-dial-code').prev().attr('class').split(' ');
        $('#whatsapp_country_code').val(whatsapp_country_code+"-"+getwhatsappcode[1]);


        var err='';
        $('.Err').hide();
        $('.Err1').hide();

        if((parentcat=='' || parentcat==null))
        {
            $('#parent_category_id').addClass('has-error');
            $('#parent_category_id_inner .Err').show();
            if(err=='')
            {
                $('#parent_category_id').focus();
                err='set';
            }
        }    
        if((servicename=='' || servicename==null))
        {
            $('#service_name').addClass('has-error');
            $('#service_name_inner .Err').show();
            if(err=='')
            {
                $('#service_name').focus();
                err='set';
            }
        }
        if((servicedesc=='' || servicedesc==null))
        {
            $('#service_description').addClass('has-error');
            $('#service_description_inner .Err').show();
            if(err=='')
            {
                $('#service_description').focus();
                err='set';
            }
        }
        if (mainid=="" && (serviceimage=="" || serviceimage==null))
        {
            $('#service_image').addClass('has-error');
            $('#service_image_inner .Err').show();
            if(err=='')
            {
                $('#service_image').focus();
                err='set';
            }
        }
        if (mainid!="" && (serviceimage=="" && delcheckstatus==1))
        {
            $('#service_image').addClass('has-error');
            $('#service_image_inner .Err').show();
            if(err=='')
            {
                $('#service_image').focus();
                err='set';
            }
        }
        if ( $("#service_image_inner .file-upload-indicator .glyphicon").hasClass("glyphicon-exclamation-sign") ) 
        {
            $('#service_image').addClass('has-error');
            $('#service_image_inner .Err').show();
            if(err=='')
            {
                $('#service_image').focus();
                err='set';
            }
        }

        if(email!="" && !emailregex.test(email))
        {
            $('#email').addClass('has-error');
            $('#email_inner .Err').show();
            if(err=='')
            {
                $('#email').focus();
                err='set';
            }
        } 
        if((phonenumber=='' || phonenumber==null || isNaN(phonenumber)))
        {
            $('#phone').addClass('has-error');
            $('#phone_inner .Err').show();
            if(err=='')
            {
                $('#phone').focus();
                err='set';
            }
        } 
        if((altnumber!='' && altnumber!=null && isNaN(altnumber)))
        {
            $('#alternatephone').addClass('has-error');
            $('#alternatephone_inner .Err').show();
            if(err=='')
            {
                $('#alternatephone').focus();
                err='set';
            }
        } 
        if((smsnumber!='' && smsnumber!=null && isNaN(smsnumber)))
        {
            $('#sms').addClass('has-error');
            $('#sms_inner .Err').show();
            if(err=='')
            {
                $('#sms').focus();
                err='set';
            }
        } 
        if((whatsappnumner!='' && whatsappnumner!=null && isNaN(whatsappnumner)))
        {
            $('#whatsapp').addClass('has-error');
            $('#whatsapp_inner .Err').show();
            if(err=='')
            {
                $('#whatsapp').focus();
                err='set';
            }
        }
        if(web!="" && !urlregex.test(web))
        {
            $('#web').addClass('has-error');
            $('#web_inner .Err').show();
            if(err=='')
            {
                $('#web').focus();
                err='set';
            }
        }
        if(website!="" && !urlregex.test(website))
        {
            $('#website').addClass('has-error');
            $('#website_inner .Err').show();
            if(err=='')
            {
                $('#website').focus();
                err='set';
            }
        }
        if(linkedin!="" && !urlregex.test(linkedin))
        {
            $('#linkedin').addClass('has-error');
            $('#linkedin_inner .Err').show();
            if(err=='')
            {
                $('#linkedin').focus();
                err='set';
            }
        }
        if((status=='' || status==null))
        {
            $('#status').addClass('has-error');
            $('#status_inner .Err').show();
            if(err=='')
            {
                $('#status').focus();
                err='set';
            }
        }

        if(err!='')
        {
            $(':input[type="button"]').prop('disabled', false);
            return false;
        }
        else
        {
            checkunique();
        }
    }

    function checkunique() {
        $.ajax({
          type : "POST",
          url : "{{ url('/') }}/admin/servicename",
          data : $('#companyform').serialize(),
          beforeSend : function() {
          },
          success : function(data) { 
            if(data == "failed")
            {
                $('#service_name').addClass('has-error');
                $('#service_name_inner .Err1').show();
                $('#service_name').focus();
                return false;
            }
            else
                $('#companyform').submit();   
            },
        });
    }

    var phonecountry_code="{{$phonecode}}";
    $('#phone').intlTelInput({
        initialCountry : phonecountry_code, 
        separateDialCode: true,
        utilsScript:'http://knekt.com/js/utils.js'
    });

    var alternatephone_code="{{$alternatephonecode}}";
    $('#alternatephone').intlTelInput({
        initialCountry : alternatephone_code, 
        hiddenInput: 'full_phone',
        separateDialCode: true,
        utilsScript:'http://knekt.com/js/utils.js'
    });

    var sms_code="{{$smscode}}";
    $('#sms').intlTelInput({
        initialCountry : sms_code, 
        hiddenInput: 'full_phone',
        separateDialCode: true,
        utilsScript:'http://knekt.com/js/utils.js'
    });

    var whatsapp="{{$whatsappcode}}";
    $('#whatsapp').intlTelInput({
        initialCountry : whatsapp, 
        hiddenInput: 'full_phone',
        separateDialCode: true,
        utilsScript:'http://knekt.com/js/utils.js'
    });

</script>
@if ($pid != '')
<script type="text/javascript">
$(function () {
  subcatlist();
  });
</script>
@endif