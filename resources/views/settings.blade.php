<link rel="stylesheet" href="{{url('/')}}/css/intlTelInput.css">
<link rel="stylesheet" href="{{url('/')}}/css/intlTelInput.min.css">
<script src="{{url('/')}}/js/intlTelInput.js" ></script>
<script src="{{url('/')}}/js/utils.js" ></script>
<script src="{{url('/')}}/js/intlTelInput.min.js" ></script>
<script src="{{url('/')}}/js/intlTelInput-jquery.js" ></script>
<script src="{{url('/')}}/js/intlTelInput-jquery.min.js" ></script>
<style type="text/css">
.kv-file-remove,.kv-file-upload, .fileinput-remove-button {display:none;}
.Err, .Err1 { display:none; color:red!important; }
.reqform {color:red !important;}
</style>


<section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="box box-info">
            <div class="box-header with-border">
               <h3 class="box-title">{{ trans('admin.form_menu.create') }}</h3>
               <div class="box-tools">
                  <div class="btn-group pull-right" style="margin-right: 5px">
                     <a href="{{ url('/')}}/admin/settings" class="btn btn-sm btn-default" title="List"><i class="fa fa-list"></i><span class="hidden-xs">&nbsp;{{ trans('admin.form_menu.list') }}</span></a>
                  </div>
               </div>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{ url('/')}}/admin/savesettings" method="post" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" pjax-container id="usersform">
               <div class="box-body">
               {{ csrf_field() }}
                  <div class="fields-group">                                         
                     <div class="form-group  ">
                        <label for="googleplus_id" class="col-sm-2  control-label">{{ trans('admin.form_menu.google_api_key') }}</label>
                        <div class="col-sm-8" id="google_apikey">                            
                           <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                              <input type="text" id="google_apikey" name="google_apikey" value="{{(isset($setting->google_apikey))?$setting->google_apikey:''}}" class="form-control google_apikey" placeholder="Input google api key" maxlength="50" />
                           </div>
                        </div>
                     </div>

                    
                  </div>
               </div>
               <!-- /.box-body -->
               <div class="box-footer">                   
                  <div class="col-md-2"></div>
                    <div class="col-md-8">
                       <div class="btn-group pull-right">
                          <button type="Submit" class="btn btn-primary">{{ trans('admin.form_menu.submit') }}</button>
                      </div>                    
                    </div>
               </div>
               <input type="hidden" name="sid" value="{{ $sid }}" id="setting_id">
               <!-- /.box-footer -->
            </form>
         </div>
      </div>
   </div>
</section>