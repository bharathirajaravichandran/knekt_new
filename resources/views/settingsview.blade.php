<section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="row">
            <div class="col-md-12">
               <div class="box box-info">
                  <div class="box-header with-border">
                     <h3 class="box-title">{{ trans('admin.form_menu.view') }}</h3>
                     <div class="box-tools">
                        <div class="btn-group pull-right" style="margin-right: 5px">
                        </div>
                        <div class="btn-group pull-right" style="margin-right: 5px">
                           <a href="/admin/settings" class="btn btn-sm btn-default" title="List">
                           <i class="fa fa-list"></i><span class="hidden-xs"> {{ trans('admin.form_menu.list') }}</span>
                           </a>
                        </div> 
                     </div>
                  </div>
                  <!-- /.box-header -->
                  <!-- form start -->
                  <div class="form-horizontal">
                     <div class="box-body" style="
                        border: 2px solid #00BFEC;
                        padding: 30px;
                        border-radius: 25px;
                        margin-top: 15px;
                        ">
                        <div class="fields-group">
                            
                           
                           
                           
                          
                           
                          
                           
                          
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.google_api_key') }} </label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       <?php
                                       if($setting->google_apikey!="")
                                       echo $setting->google_apikey;
                                       else
                                       echo "-----------------";
                                       ?>
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>                            
                          
                           
                          
                           
                        </div>
                     </div>
                     <!-- /.box-body -->
                  </div>
               </div>
            </div>
            <div class="col-md-12">
            </div>
         </div>
      </div>
   </div>
</section>