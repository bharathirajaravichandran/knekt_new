<section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="row">
            <div class="col-md-12">
               <div class="box box-info">
                  <div class="box-header with-border">
                     <h3 class="box-title">{{ trans('admin.form_menu.view') }}</h3>
                     <div class="box-tools">
                        <div class="btn-group pull-right" style="margin-right: 5px">
                           <a href="/admin/company/{{$company->id}}/edit" class="btn btn-sm btn-primary" title="Edit">
                           <i class="fa fa-edit"></i><span class="hidden-xs"> {{ trans('admin.form_menu.edit') }}</span>
                           </a>
                        </div>
                        <div class="btn-group pull-right" style="margin-right: 5px">
                           <a href="/admin/company" class="btn btn-sm btn-default" title="List">
                           <i class="fa fa-list"></i><span class="hidden-xs"> {{ trans('admin.form_menu.list') }}</span>
                           </a>
                        </div>
                     </div>
                  </div>
                  <!-- /.box-header -->
                  <!-- form start -->
                  <div class="form-horizontal">
                     <div class="box-body" style="
                        border: 2px solid #00BFEC;
                        padding: 30px;
                        border-radius: 25px;
                        margin-top: 15px;
                        ">
                        <div class="fields-group">
                            <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.category') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                      {{@$company->category->category_name}}
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.subcategory') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       <?php 
                                       if($company->sub_cat_id!="" && $company->sub_cat_id!=0)
                                          echo @$company->subcategory->category_name;
                                       else  
                                          echo "No Subcategory";                                      
                                       ?>
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.company_name') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       {{$company->service_name}}
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.company_description') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        {{$company->service_description}}
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div> 
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.company_image') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       <?php if($company->service_image!="") { ?>
                                          <img src="{{ url('/') }}/uploads/Company/{{$company->service_image}}" width="250px" height="250px">
                                      <?php } else {
                                          echo "No Image";
                                      }
                                       ?>
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div> 
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.notification') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <?php
                                       if($company->notification!="")
                                       echo $company->notification;
                                       else
                                       echo "-----------------";
                                       ?>
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div> 
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.email') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       <?php
                                       if($company->email!="")
                                       echo $company->email;
                                       else
                                       echo "-----------------";
                                       ?>
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.phone') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        {{$company->phone}}
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div> 
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.alternate_phone') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       <?php
                                       if($company->alternatephone!="")
                                       echo $company->alternatephone;
                                       else
                                       echo "-----------------";
                                       ?>
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div> 
                            <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.sms') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       <?php
                                       if($company->sms!="")
                                       echo $company->sms;
                                       else
                                       echo "-----------------";
                                       ?>
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                            <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.whatsapp') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       <?php
                                       if($company->whatsapp!="")
                                       echo $company->whatsapp;
                                       else
                                       echo "-----------------";
                                       ?>
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div> 
                            <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.live_chart') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       <?php
                                       if($company->web!="")
                                       echo $company->web;
                                       else
                                       echo "-----------------";
                                       ?>
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.website') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       <?php
                                       if($company->website!="")
                                       echo $company->website;
                                       else
                                       echo "-----------------";
                                       ?>
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                             <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.linkedin') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       <?php
                                       if($company->linkedin!="")
                                       echo $company->linkedin;
                                       else
                                       echo "-----------------";
                                       ?>
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.language') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        English
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div> 
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.opening_time') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       <?php
                                       if($company->opening_time != 1)
                                       echo 'Open All Time (00:00:00 - 23:59:59)';
                                       else
                                       echo "Open At (".$company->working_time.' )';
                                       ?>
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div> 
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.created_at') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       <?php echo date_format($company->created_at,"M d, Y H:m:s");?>
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.updated_at') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       <?php 
                                       if($company->updated_at!=null)
                                       echo date_format($company->updated_at,"M d, Y H:m:s");
                                       else
                                       echo "-------------";
                                       ?>
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.status') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                      {{($company->status == 1) ? "Active" : "InActive"}}
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- /.box-body -->
                  </div>
               </div>
            </div>
            <div class="col-md-12">
            </div>
         </div>
      </div>
   </div>
</section>