<section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="box box-info">
            <div class="box-header with-border">
               <h3 class="box-title">{{trans('admin.form_menu.ivr_details')}}</h3>
                <div class="box-tools">
                  <div class="btn-group pull-right" style="margin-right: 5px">
                     <a href="/admin/ivr_details" class="btn btn-sm btn-default" title="List"><i class="fa fa-list"></i><span class="hidden-xs">&nbsp;{{trans('admin.form_menu.list')}}</span></a>
                  </div>
                  </div>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{ url('/') }}/admin/saveprds" method="POST" accept-charset="UTF-8" class="form-horizontal ivr_form" id="ivr_form" pjax-container>
               
               <div class="box-body">
                  <div class="fields-group">
                     <div class="form-group  ">
                        <label for="company_id" class="col-sm-2  control-label">{{ trans('admin.form_menu.company_name') }}</label>
                        <div class="col-sm-8 company_id_error">
                           <input type="hidden" name="company_id"/>
                           <select class="form-control company_id" style="width: 100%;" name="company_id" id="company_id" data-value="" >
                              <option value="">Select Company</option>
                              @foreach ($companies as $k=>$v)
                              <option value="{{ $k }}" {{ (isset($old_values->company_id) && $old_values->company_id==$k)  ? 'selected': '' }}>{{ $v }}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label  class="col-sm-2  control-label"></label>
                        <div class="col-sm-8">
                           <span id="ivrdetails"></span>
                        </div>
                     </div>

                      <div class="form-group">
                        <label  class="col-sm-2  control-label">{{ trans('admin.form_menu.phone') }}</label>
                        <div class="col-sm-8">
                           <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                              <input type="text" id="phone" name="phone" value="" class="form-control phone" placeholder="Company Phone" readonly />
                           </div>
                        </div>
                     </div>

                     <div class="form-group">
                        <label for="language" class="col-sm-2  control-label">{{ trans('admin.form_menu.language') }}</label>
                        <div class="col-sm-8 option_name_error">
                           <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                              <input type="text" id="option_name" name="option_name" value="{{(isset($old_values->option_name)) ? $old_values->option_name: '' }}" class="form-control option_name" placeholder="Input IVR value" />
                              <input type="hidden" name="languageError" id="languageError" value="">
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="option_value" class="col-sm-2  control-label">{{ trans('admin.form_menu.ivr_value') }}</label>
                        <div class="col-sm-8 option_value_error">
                           <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                              <input type="text" id="option_value" name="option_value" value="{{(isset($old_values->option_value)) ? $old_values->option_value: '' }}" class="form-control option_value" placeholder="Input IVR value" />
                               <input type="hidden" name="optionError" id="optionError" value="">
                           </div>
                        </div>
                     </div>
                     <div class="form-group  ">
                        <label for="time_delay" class="col-sm-2  control-label">{{ trans('admin.form_menu.time_delay') }}</label>
                        <div class="col-sm-8 time_delay_error">
                           <div>
                              <span id="minutes1">{{ trans('admin.form_menu.minutes') }} : </span><input type="number" name="minutes" id="minutes" style="width:50px" min="00" max="59" value="{{(isset($time_value[1])) ? $time_value[1]: 00 }}">
                              <span id="seconds1" style="padding-left: 10px"> {{ trans('admin.form_menu.seconds') }} : </span><input type="number" name="seconds" id="seconds" style="width:50px" min="00" max="59" value="{{(isset($time_value[2])) ? $time_value[2]: 00 }}">
                           </div>
                        </div>
                     </div>
                     <input type="hidden" name="parent_option_id" value="" class="parent_option_id"  />
                     <input type="hidden" name="url_value" value="{{Request::segment(3)}}" class="url_value"  />
                     <input type="hidden" name="oid" value="{{$pid}}" class="oid"  />
                  </div>
               </div>
               <!-- /.box-body -->
               <div class="box-footer">
                  <input type="hidden" name="_token" class="_token" value="{{csrf_token()}}">
                  <div class="col-md-2">
                  </div>
                  <div class="col-md-8">
                     <div class="btn-group pull-right">
                        <button type="button" class="btn btn-primary" onclick="return validation();" >{{ trans('admin.form_menu.submit') }}</button>
                     </div>
                     <div class="btn-group pull-left">
                        <button type="reset" class="btn btn-warning">{{ trans('admin.form_menu.reset') }}</button>
                     </div>
                  </div>
               </div>
               <!-- /.box-footer -->
            </form>
         </div>
      </div>
   </div>
</section>
<script>
   function validation()
   {
   var company_id=$('#company_id').val();
   var option_name=$('.option_name').val();
   var option_value=$('.option_value').val();
   var minutes = $('#minutes').val();
   var seconds = $('#seconds').val();
      
       var err='';
       if(company_id=='')
       {
   
           // $('#company_id').parent('.col-sm-8').parent('.form-group').addClass('has-error');
           $('.company_id_error').prepend('<label class="control-label" for="inputError" style="color:red;"><i class="fa fa-times-circle-o"></i>The Company field is required.</label>');
         
               $('#company_id').focus();
               err='set';
   
       }
   
       if(option_name=='')
       {
           // $('#language').parent('.col-sm-8').parent('.form-group').addClass('has-error');
           $('.option_name_error').prepend('<label class="control-label languageError1" for="inputError" style="color:red;"><i class="fa fa-times-circle-o"></i> The IVR Name field is required.</label>');
               $('#language').focus();
               err='set';
       }else{
   
       }
       if(option_value=='')
       {
           // $('#option_value').parent('.col-sm-8').parent('.form-group').addClass('has-error');
           $('.option_value_error').prepend('<label class="control-label option_valueError1" for="inputError" style="color:red;"><i class="fa fa-times-circle-o"></i> The IVR Value field is required.</label>');
               $('#option_value').focus();
               err='set';
   
       }
   
       if(minutes!=''){
          
           if(minutes>=60){
               var email_error ='<label class="control-label time_delay_error2" for="inputError" style="color:red;"><i class="fa fa-times-circle-o"></i> Less than One Hour Only.</label>';
                $('.time_delay_error2').remove(); 
                $('.time_delay_error3').remove(); 
                $('.time_delay_error').prepend(email_error);
               err='set';
           }
           if(minutes.length>=3){
                var email_error ='<label class="control-label time_delay_error2" for="inputError" style="color:red;"><i class="fa fa-times-circle-o"></i>Allow 2 digits Only.</label>';
                $('.time_delay_error2').remove(); 
                $('.time_delay_error3').remove(); 
                $('.time_delay_error').prepend(email_error);
               err='set';
           }
       }
        
       if(seconds!=''){
          
           if(seconds>=60){
               var email_error ='<label class="control-label time_delay_error3" for="inputError" style="color:red;"><i class="fa fa-times-circle-o"></i> Less than One Hour Only.</label>';
                $('.time_delay_error3').remove(); 
                $('.time_delay_error2').remove(); 
                $('.time_delay_error').prepend(email_error);
                err='set';
           }
           if(seconds.length>=3){
                var email_error ='<label class="control-label time_delay_error3" for="inputError" style="color:red;"><i class="fa fa-times-circle-o"></i> Allow 2 digits Only.</label>';
                $('.time_delay_error3').remove(); 
                $('.time_delay_error2').remove(); 
                $('.time_delay_error').prepend(email_error);
                err='set';
           }
       }
       if(err!='') 
       {
           return false;
       }
       else
       {
           $('#ivr_form').submit();
       }
   }
   
        $( document ).ready(function() {
          
           $(".language").select2({"allowClear":false,"placeholder":"Product Preferences"});
           $("#company_id").select2({"allowClear":false,"placeholder":"Related Product"});
       
       });
   
               var url_sub = '/admin/get_ivr_details';
               var _token = $('._token').val();
               var fullurl = window.location.pathname.split('/'); 
                   $('#company_id').change(function() {
                    
                       var company_id = $(this).val();
                       $.ajax({
                           type:'POST',
                           url:  url_sub ,
                           data: $('#ivr_form').serialize(),
                           success: function(result) 
                           {

                               var value = result.ivrdetail;                          
                               $('#ivrdetails').text(value) ;
                               $('#phone').val(result.phone);
                               
                           
                           }
                       });               
   
                   });
   
                    //IVR Language Check Start
               $('.option_name').on('blur',function(){
                       var company_id = $('#company_id').val();
                       var option_name = $('.option_name').val();   
                       var uri_segment = fullurl[4];
                       var old_id = fullurl[3];                 
                        $.ajax({
                               type: 'POST',
                               url: '/admin/get_ivr_language',
                               data: {                               
                                   comp_id: company_id,
                                   option_name: option_name,
                                   _token: _token,     
                                    uri_segment: uri_segment,
                                   old_id: old_id,                          
                                   },
                               success: function( result ) {      
                                           
                                   if(result==1){
                                       $('.languageError1').remove();
                                       var email_error ='<label class="control-label languageError2" for="inputError" style="color:red;"><i class="fa fa-times-circle-o"></i> The Language is already taken.</label>';
                                        $('.languageError2').remove(); 
                                        $('.option_name_error').prepend(email_error);
                                        $('#languageError').val('error');
                                        $('button:contains(Submit)').attr('disabled', true); 
                                       
                                       
                                   }else{
                                        $('.languageError2').remove();  
                                         $('#languageError').val('');
                                          if($('#optionError').val()!='error'){
                                         $('button:contains(Submit)').attr('disabled', false); 
                                        }
                                   }
                               }
                           });
                   });
   
               //IVR Language Check End
   
                 //IVR Value already exists chevck START
   
                   $('#option_value').on('blur focusout keypress',function(){
                       var option_value = $('#option_value').val();
   
                       if(option_value!=''){
                           var value = /^\d+$/;
                           if(!value.test(option_value)){
                               var email_error ='<label class="control-label option_valueError2" for="inputError" style="color:red;"><i class="fa fa-times-circle-o"></i> The IVR Value field is Numeric only.</label>';
                                        $('.option_valueError1').remove(); 
                                        $('.option_valueError2').remove(); 
                                        $('.option_value_error').prepend(email_error);
                                       $('button:contains(Submit)').attr('disabled', true); 
                           }
                       }
                   });
                   $('#option_value').on('blur focusout',function(){   
                        $('#small2').remove();        
                       var parent_id = $('input[name=parent_value]').val();
                       var company_id = $('#company_id').val();                   
                       var uri_segment = fullurl[4];
                       var old_id = fullurl[3];
                       if(company_id!=''){
                           comp_id =company_id;
                       }else{
                           comp_id='';
                       }
                       var option_value = $('#option_value').val();
   
                       if(option_value!=''){
                           
                               $.ajax({
                               type: 'POST',
                               url: '/admin/get_ivr_value',
                               data: {
                                   parent_id: parent_id,
                                   comp_id: comp_id,
                                   option_value: option_value,
                                   uri_segment: uri_segment,
                                   old_id: old_id,
                                   _token: _token,               
                                   },
                               success: function( result ) {      
                                           
                                   if(result==1){                                   
                                       var email_error ='<label class="control-label option_valueError2" for="inputError" style="color:red;"><i class="fa fa-times-circle-o"></i>The IVR Value field is already taken.</label>';
                                        $('.option_valueError1').remove(); 
                                        $('.option_valueError2').remove(); 
                                        $('.option_value_error').prepend(email_error);
                                         $('#optionError').val('error');
                                          $('button:contains(Submit)').attr('disabled', true); 
                                   }else{
                                       $('.option_valueError2').remove(); 
                                        $('#optionError').val('');
                                        if($('#languageError').val()!='error'){
                                         $('button:contains(Submit)').attr('disabled', false); 
                                        }
                                   }
                               }
                           });
                           }
                   });
   
                   //IVR Value already exists check END
</script>