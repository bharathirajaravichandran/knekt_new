<section class="content">
   <script>
      $(function () {
          toastr.success('Login successful', null, []);
      });
   </script>
   <div class="row">
      <div class="col-md-12">
         <div class="row">
            <div class="col-md-12">
               <div class="box box-info">
                  <div class="box-header with-border">
                     <h3 class="box-title">{{ trans('admin.form_menu.view') }}</h3>
                     <div class="box-tools">
                        <div class="btn-group pull-right" style="margin-right: 5px">
                           <a href="/admin/companybranch/{{$companybranch->id}}/edit" class="btn btn-sm btn-primary" title="Edit">
                           <i class="fa fa-edit"></i><span class="hidden-xs"> {{ trans('admin.form_menu.edit') }}</span>
                           </a>
                        </div>
                        <div class="btn-group pull-right" style="margin-right: 5px">
                           <a href="/admin/companybranch" class="btn btn-sm btn-default" title="List">
                           <i class="fa fa-list"></i><span class="hidden-xs"> {{ trans('admin.form_menu.list') }}</span>
                           </a>
                        </div>
                     </div>
                  </div>
                  <!-- /.box-header -->
                  <!-- form start -->
                  <div class="form-horizontal">
                     <div class="box-body" style="
                        border: 2px solid #00BFEC;
                        padding: 30px;
                        border-radius: 25px;
                        margin-top: 15px;
                        ">
                        <div class="fields-group">
                            <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.company') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                      {{$companybranch->company->service_name}}
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.latitude') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       {{$companybranch->latitude}}
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.longitude') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       {{$companybranch->langitude}}
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.phone') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        {{$companybranch->phnoe}}
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.address') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        {{$companybranch->address}}
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.created_at') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       <?php echo date_format($companybranch->created_at,"M d, Y H:m:s");?>
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.updated_at') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       <?php 
                                       if($companybranch->updated_at!=null)
                                       echo date_format($companybranch->updated_at,"M d, Y H:m:s");
                                       else
                                       echo "-------------";
                                       ?>
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.status') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                      {{($companybranch->status == 1) ? "Active" : "InActive"}}
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- /.box-body -->
                  </div>
               </div>
            </div>
            <div class="col-md-12">
            </div>
         </div>
      </div>
   </div>
</section>