<style type="text/css">
.kv-file-remove,.kv-file-upload, .fileinput-remove-button {display:none;}
.Err, .Err1 { display:none; color:red!important; }
</style>
<section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="box box-info">
            <div class="box-header with-border">
               <h3 class="box-title">{{ trans('admin.form_menu.ivr_excel_upload') }}</h3>
               <div class="box-tools">
                  <div class="btn-group pull-right" style="margin-right: 5px">
                     <a href="{{ url('/')}}/admin/ivr_details" class="btn btn-sm btn-default" title="List"><i class="fa fa-list"></i><span class="hidden-xs">&nbsp;{{ trans('admin.form_menu.list') }}</span></a>
                  </div>
               </div>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{ url('/') }}/admin/ivr_upload_form" method="post" accept-charset="UTF-8" class="form-horizontal" enctype=" multipart /form-data" pjax-container="" id="uploadform">
               <div class="box-body">
                  <div class="fields-group">
                     <div class="form-group  ">
                        <label for="category_image" class="col-sm-2  control-label">{{ trans('admin.form_menu.ivr_excel') }}</label>
                         <div class="col-sm-8 err_imageinner" id="ivr_upload_inner">
                           <label class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> Please Upload Valid File.</label>
                           <input type="file" class="ivr_upload" id="ivr_upload" name="ivr_upload" /> 
                        </div>
                     </div>
                   <!-- /.box-body -->
                    <div class="box-footer">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="deletecheck" id="deletecheck" class="deletecheck" value="0">
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-8">
                            <div class="btn-group pull-right">
                                <button type="button" class="btn btn-primary" onclick="return validation();">{{ trans('admin.form_menu.submit') }}</button>
                            </div>
                        </div>
                    </div>
                 </div>
            </form>
         </div>
      </div>
   </div>
</section>

<script data-exec-on-popstate>
   $(function () {  
      $( document ).ready(function() {

        $("input.ivr_upload").fileinput({
          "overwriteInitial":true,
          "initialPreviewAsData":true,
          "browseLabel":"Browse",
          "showRemove":true,
          "showUpload":false,
          "uploadUrl":"#" , 
          "initialPreviewDelete":true, 
          "allowedFileExtensions": ['xls', 'xlsx'], 
          "deleteExtraData":{"category_image":"_file_del_","_file_del_":"","_token":"{{ csrf_token() }}","_method":"PUT"},
          "deleteUrl":"#"
        });
    });
    });

  function validation()
  {
    var catimage=$('#ivr_upload').val();
    var delcheckstatus=$('#deletecheck').val();

    var err='';
    $('.Err').hide();
    $('.Err1').hide();

    if ((catimage=="" || catimage==null))
    {
        $('#ivr_upload').addClass('has-error');
        $('#ivr_upload_inner .Err').show();
        if(err=='')
        {
            $('#ivr_upload').focus();
            err='set';
        }
    }
    if ((catimage=="" && delcheckstatus==1))
    {
        $('#ivr_upload').addClass('has-error');
        $('#ivr_upload_inner .Err').show();
        if(err=='')
        {
            $('#ivr_upload').focus();
            err='set';
        }
    }
    if ( $("#ivr_upload_inner .file-upload-indicator .glyphicon").hasClass("glyphicon-exclamation-sign") ) 
    {
        $('#ivr_upload').addClass('has-error');
        $('#ivr_upload_inner .Err').show();
        if(err=='')
        {
            $('#ivr_upload').focus();
            err='set';
        }
    }
    if(err!='')
    {
        $(':input[type="button"]').prop('disabled', false);
        return false;
    }
    else
      $("#uploadform").submit();
  }


</script>