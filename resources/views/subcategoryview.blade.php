<section class="content">
   <script>
      $(function () {
          toastr.success('Login successful', null, []);
      });
   </script>
   <div class="row">
      <div class="col-md-12">
         <div class="row">
            <div class="col-md-12">
               <div class="box box-info">
                  <div class="box-header with-border">
                     <h3 class="box-title">{{ trans('admin.form_menu.view') }}</h3>
                     <div class="box-tools">
                        <div class="btn-group pull-right" style="margin-right: 5px">
                           <a href="/admin/subcategory/{{$category->id}}/edit" class="btn btn-sm btn-primary" title="Edit">
                           <i class="fa fa-edit"></i><span class="hidden-xs"> {{ trans('admin.form_menu.edit') }}</span>
                           </a>
                        </div>
                        <div class="btn-group pull-right" style="margin-right: 5px">
                           <a href="/admin/subcategory" class="btn btn-sm btn-default" title="List">
                           <i class="fa fa-list"></i><span class="hidden-xs"> {{ trans('admin.form_menu.list') }}</span>
                           </a>
                        </div>
                     </div>
                  </div>
                  <!-- /.box-header -->
                  <!-- form start -->
                  <div class="form-horizontal">
                     <div class="box-body" style="
                        border: 2px solid #00BFEC;
                        padding: 30px;
                        border-radius: 25px;
                        margin-top: 15px;
                        ">
                        <div class="fields-group">
                            <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.category_name') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       <?php
                                       if($category->parent_category_id!=0)
                                          if($category->parentcategory->category_name!='')
                                             echo @$category->parentcategory->category_name;
                                          else
                                             echo "No Parent Category";
                                       else
                                       echo "No Parent Category";
                                       ?>
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.subcategory_name') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       {{$category->category_name}}
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                            <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.subcategory_description') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        {{$category->category_description}}
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div> 
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.language') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       English
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.subcategory_image') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                                                               <?php
                                       if($category->category_image!="") { ?>
                                          <img src="{{ url('/') }}/uploads/Category/{{$category->category_image}}" width="250px" height="250px">
                                      <?php } else {
                                       echo "No Image";
                                      }
                                       ?>
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div> 
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.created_at') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       <?php echo date_format($category->created_at,"M d, Y H:m:s");?>
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.updated_at') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                       <?php 
                                       if($category->updated_at!=null)
                                       echo date_format($category->updated_at,"M d, Y H:m:s");
                                       else
                                       echo "-------------";
                                       ?>
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                           <div class="form-group ">
                              <label class="col-sm-2 control-label">{{ trans('admin.form_menu.status') }}</label>
                              <div class="col-sm-8">
                                 <div class="box box-solid box-default no-margin box-show">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                      {{($category->status == 1) ? "Active" : "InActive"}}
                                    </div>
                                    <!-- /.box-body -->
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- /.box-body -->
                  </div>
               </div>
            </div>
            <div class="col-md-12">
            </div>
         </div>
      </div>
   </div>
</section>