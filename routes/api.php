<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('Signup','\App\Http\Controllers\ApiController@Signup');

Route::post('Signin','\App\Http\Controllers\ApiController@Signin');

Route::post('Forgotpassword','\App\Http\Controllers\ApiController@Forgotpassword');

Route::post('Profile','\App\Http\Controllers\ApiController@Profile');

Route::post('GetCategories','\App\Http\Controllers\ApiController@GetCategories');

Route::post('GetSubcategories','\App\Http\Controllers\ApiController@GetSubcategories');

Route::post('GetCompanyData','\App\Http\Controllers\ApiController@GetCompanyData');

Route::post('Addfavourite','\App\Http\Controllers\ApiController@Addfavourite');

Route::post('Getfavourites','\App\Http\Controllers\ApiController@Getfavourites');

Route::post('GetRecent','\App\Http\Controllers\ApiController@GetRecent');

Route::post('GetCountry','\App\Http\Controllers\ApiController@getCountry');

Route::post('GetCompanyKnekt','\App\Http\Controllers\ApiController@GetCompanyKnekt');

Route::post('AddFavourite','\App\Http\Controllers\ApiController@AddFavourite');

Route::post('GetRecentCategory','\App\Http\Controllers\ApiController@GetRecentCategory');

Route::post('checkUserExist','\App\Http\Controllers\ApiController@checkUserExist');

Route::post('checkSocialUserExist','\App\Http\Controllers\ApiController@checkSocialUserExist');

Route::post('SocialSignup','\App\Http\Controllers\ApiController@SocialSignup');

Route::post('SearchCategories','\App\Http\Controllers\ApiController@SearchCategories');

Route::post('SearchFavourites','\App\Http\Controllers\ApiController@SearchFavourites');


